HYDRA is a monitor for metric temporal logic (MTL).
We extend it to support metric dynamic logic (MDL)
and further optimize it to be interval-oblivious in the paper titled:

Multi-Head Monitoring of Metric Dynamic Logic

This archive is the supplementary material for the above paper.

HYDRA takes as input an MTL or MDL formula and a trace stored in a file
and outputs a trace of verdicts denoting the satisfaction (or violation)
of the formula at every position in the trace (for formulas involving
future operators, the trace of verdicts may be incomplete if no verdict
for a time-point can be inferred by the monitor from the given trace).

---

# Directory Structure:

`Dockerfile` - Dockerfile for this supplementary material
`paper.pdf` - paper on HYDRA(MDL)
`run_exp.sh` - script to run the experiments (see below for more details)
`run_tests.sh` - script to run the correctness tests (see below for more details)
`evaluation/` contains tools for testing correctness and performing evaluation
`examples/` contains example formulas and log from this README
`html/` contains the generated HTML page of the formalization in Isabelle
`src/` contains HYDRA's source code (in C++)
`third_party/` contains third-party monitoring tools
`thys/` contains the formalization of the paper in Isabelle
`vydra/` contains the formally verified core (`verified.ml`) and
unverified code to parse the formula and trace

---

# Formalization in Isabelle

The formal development can be browsed as a generated HTML page (see the html directory).
A better way to study the theory files, however, is to open them in Isabelle/jEdit.

The raw Isabelle sources are included in a separate directory called `thys`.

The formalization can been processed with Isabelle2020, which can be downloaded from

https://isabelle.in.tum.de/website-Isabelle2020/

and installed following the standard instructions from

https://isabelle.in.tum.de/website-Isabelle2020/installation.html

To build the theories, export the verified OCaml code,
and regenerate the HTML page, run
`isabelle build -o browser_info -c -e -v -D thys/`
in the root directory of this archive.

---

# Build and clean

We recommed using `docker` and the provided `Dockerfile`.
```
sudo docker build -t infsec/hydra .
sudo docker run -it infsec/hydra
```

Prerequisites:
```
sudo apt-get update
sudo apt-get install make gnuplot clang m4 libboost-dev libgmp3-dev python3-pip ocamlbuild ocaml-nox opam
pip3 install setuptools wheel
pip3 install antlr4-python3-runtime pyyaml
opam init --comp=4.05.0
opam switch 4.05.0
opam install ocamlfind safa menhir zarith
```

```
cd third_party/aerial
eval $(opam config env)
make
```

```
cd third_party/hydra-atva19
make hydra
```

```
cd third_party/monpoly
eval $(opam config env)
make
```

```
cd third_party/pcre2-10.34
./configure && make
```

```
cd third_party/reelay-codegen
python3 setup.py build
cp scripts/reelay reelay.py
```

Run `make hydra` in `src/` to build HYDRA.
Run `eval $(opam config env)` and `make` in `vydra/` to build VYDRA (the formally verified algorithm).

Run `make test` in `evaluation/` to build tools for
testing HYDRA against VYDRA. How to run the tests is described in
a separate section further below.

Run `make evaluation` in `evaluation/` to build tools for
performing the evaluation. How to run the evaluation is described in
a separate section further below.

---

# Usage

To run HYDRA type:

```bash

$ ./hydra formula log

```

where

formula = path to a text file with an MTL/MDL formula

log     = path to a text file with a log


MDL Syntax

```
{f} ::=   true
        | false
        | {ID}
        | NOT {f}
        | {f} AND {f}
        | {f} OR  {f}
        | PREV {i} {f}
        | NEXT {i} {f}
        | {f} SINCE {i} {f}
        | {f} UNTIL {i} {f}
        | ◁ {i} {r}
        | ▷ {i} {r}

{r} ::=   {f} ?
        | .
        | {r} + {r}
        | {r} {r}
        | {r} *

{i}  ::= [ {NUM} , {UP} ]
{UP} ::= {NUM} | INFINITY
```

where `{NUM}` is a nonnegative integer and `{ID}` is an identifier.
Non-terminals are enclosed in curly braces.

The semantics of temporal match operators is defined as follows:
```
i |= ◁ [a, b] {r} iff \exists j <= i. \tau_i \in \tau_j + [a, b] /\ (j, i) \in R(r)
i |= ▷ [a, b] {r} iff \exists j >= i. \tau_j \in \tau_i + [a, b] /\ (i, j) \in R(r)
```

Log Syntax

```
{L} :=   @{NUM} {ID}*
       | @{NUM} {ID}* \n {L}
```

where `{NUM}` is a nonnegative integer and `{ID}` is an identifier.
Numbers preceeded by `'@'` character are timestamps, which need to be
(non-strictly) monotonically increasing.

Example of a property expressible in both MTL and MDL:

```bash

$ cat examples/ex.mtl
p0 OR (p1 UNTIL[2,2] p2)

$ cat examples/ex.mdl
p0 OR (▷ [2,2] (p1? .)* p2?)

$ cat examples/ex.log
@0 p1 p2
@0 p0 p2
@1
@4 p0 p1 p2
@4 p1 p2
@5 p0 p1 p2

$ ./hydra ./examples/ex.mtl ./examples/ex.log
Monitoring (p0 OR (p1 UNTIL[2,2] p2))
0:0 false
0:1 true
1:0 false
4:0 true
4:1 false
5:0 true
Bye.

$ ./hydra ./examples/ex.mdl ./examples/ex.log
Monitoring (p0 OR (▷ [2,2] ((((p1?) .)*) (p2?))))
0:0 false
0:1 true
1:0 false
Bye.
```

---

# Tests

To test HYDRA against VYDRA (the formally verified algorithm) run

```bash
$ ./run_tests.sh
```

In total, 120 tests are executed (they take roughly a minute to finish).
The tests are conducted on pseudorandom formulas and traces.
The parameters are summarized here.
Formula's size: 1, 2, 3, 4, 8, 16
Formula's maximum interval bounds: 0, 1, 2, 4, 8
Pseudorandom generator's seed: 0, 1, 2, 3
Event rate: 4
Trace length: 40'000

---

# Evaluation

To reproduce the experiments from the paper, run

```bash
$ ./run_exp.sh config_mdl.py
```

We remark that the sources of R2U2 were sent to us
in a private communication and are thus not included
in the supplementary material. The authors of R2U2
plan to update the sources and we will include them
here as soon as they are publicly available.

The individual experiments are described in Section 5.
The prefixes of the experiments are as follows:
Experiment 1 -> `exp_scaling`
Experiment 2 -> `exp_size`
Experiment 3 -> `exp_mtl_exp`
Experiment 4 -> `exp_fixed_mtl`
Experiment 5 -> `exp_pcre`

After the script `run_exp.sh config_mdl.py` successfully finishes,
the raw data from the experiments are contained in `evaluation/stats/`,
and the plots are stored in `evaluation/figs/*.eps` files.
The number of runs with the same parameters (default: 3) and
a timeout per run (default: 90s) can be set in the configuration file
`config_mdl.py` under `exp_config`.

If the time or memory usage does not fit into the pre-specified
ranges in the plots, you can override them in the configuration file
`config_mdl.py` under `plot_config_exp[exp_name]["yrange"]["time"]`,
`plot_config_types[exp_name]["yrange"]["space"]`
and run `python3 proc.py config_mdl.py` in the `evaluation/` directory.
