module IArray : sig
  val of_list : 'a list -> 'a array
  val length' : 'a array -> Z.t
  val sub' : 'a array * Z.t -> 'a
end = struct

let of_list = Array.of_list;;

let length' xs = Z.of_int (Array.length xs);;

let sub' (xs, i) = Array.get xs (Z.to_int i);;

end

module VYDRA : sig
  type nat
  val integer_of_nat : nat -> Z.t
  type 'a set
  type 'a ets = Ets of 'a | Infty
  type 'a i
  type ('a, 'b) regex = Wild | Test of ('a, 'b) formula |
    Plus of ('a, 'b) regex * ('a, 'b) regex |
    Times of ('a, 'b) regex * ('a, 'b) regex | Star of ('a, 'b) regex
  and ('a, 'b) formula = Bool of bool | Atom of 'a | Neg of ('a, 'b) formula |
    Bin of (bool -> bool -> bool) * ('a, 'b) formula * ('a, 'b) formula |
    Prev of 'b i * ('a, 'b) formula | Next of 'b i * ('a, 'b) formula |
    Since of ('a, 'b) formula * 'b i * ('a, 'b) formula |
    Until of ('a, 'b) formula * 'b i * ('a, 'b) formula |
    MatchP of 'b i * ('a, 'b) regex | MatchF of 'b i * ('a, 'b) regex
  type ('a, 'b, 'c) vydra
  type ('a, 'b, 'c) vydra_rec
  val mk_event : string list -> string set
  val run_vydra :
    ('a -> ('a * (nat * string set)) option) ->
      nat ->
        (string, nat, 'a) vydra ->
          ((string, nat, 'a) vydra * (nat * bool)) option
  val sub_vydra :
    'a -> ('a -> ('a * (nat * string set)) option) ->
            nat -> (string, nat) formula -> (string, nat, 'a) vydra
  val nat_interval : nat -> nat ets -> nat i
  val nat_of_integer : Z.t -> nat
  val msize_fmla_vydra : (string, nat) formula -> nat
end = struct

type 'a countable = unit;;

type 'a finite = {countable_finite : 'a countable};;

type 'a enum =
  {finite_enum : 'a finite; enum : 'a list; enum_all : ('a -> bool) -> bool;
    enum_ex : ('a -> bool) -> bool};;
let enum _A = _A.enum;;
let enum_all _A = _A.enum_all;;
let enum_ex _A = _A.enum_ex;;

type 'a equal = {equal : 'a -> 'a -> bool};;
let equal _A = _A.equal;;

let rec eq _A a b = equal _A a b;;

let rec equal_funa _A _B f g = enum_all _A (fun x -> eq _B (f x) (g x));;

let rec equal_fun _A _B = ({equal = equal_funa _A _B} : ('a -> 'b) equal);;

type nat = Nat of Z.t;;

let rec integer_of_nat (Nat x) = x;;

let rec equal_nata m n = Z.equal (integer_of_nat m) (integer_of_nat n);;

let equal_nat = ({equal = equal_nata} : nat equal);;

let rec plus_nata m n = Nat (Z.add (integer_of_nat m) (integer_of_nat n));;

type 'a plus = {plus : 'a -> 'a -> 'a};;
let plus _A = _A.plus;;

let plus_nat = ({plus = plus_nata} : nat plus);;

let zero_nata : nat = Nat Z.zero;;

type 'a zero = {zero : 'a};;
let zero _A = _A.zero;;

let zero_nat = ({zero = zero_nata} : nat zero);;

type 'a ord = {less_eq : 'a -> 'a -> bool; less : 'a -> 'a -> bool};;
let less_eq _A = _A.less_eq;;
let less _A = _A.less;;

let rec max _A a b = (if less_eq _A a b then b else a);;

let rec less_eq_nat m n = Z.leq (integer_of_nat m) (integer_of_nat n);;

let rec less_nat m n = Z.lt (integer_of_nat m) (integer_of_nat n);;

let ord_nat = ({less_eq = less_eq_nat; less = less_nat} : nat ord);;

let rec sup_nata x = max ord_nat x;;

type 'a sup = {sup : 'a -> 'a -> 'a};;
let sup _A = _A.sup;;

let sup_nat = ({sup = sup_nata} : nat sup);;

type 'a preorder = {ord_preorder : 'a ord};;

type 'a order = {preorder_order : 'a preorder};;

let preorder_nat = ({ord_preorder = ord_nat} : nat preorder);;

let order_nat = ({preorder_order = preorder_nat} : nat order);;

type 'a semigroup_add = {plus_semigroup_add : 'a plus};;

type 'a monoid_add =
  {semigroup_add_monoid_add : 'a semigroup_add; zero_monoid_add : 'a zero};;

let semigroup_add_nat = ({plus_semigroup_add = plus_nat} : nat semigroup_add);;

let monoid_add_nat =
  ({semigroup_add_monoid_add = semigroup_add_nat; zero_monoid_add = zero_nat} :
    nat monoid_add);;

let rec embed_nat_nat n = n;;

type 'a semilattice_sup =
  {sup_semilattice_sup : 'a sup; order_semilattice_sup : 'a order};;

type 'a ab_semigroup_add = {semigroup_add_ab_semigroup_add : 'a semigroup_add};;

type 'a comm_monoid_add =
  {ab_semigroup_add_comm_monoid_add : 'a ab_semigroup_add;
    monoid_add_comm_monoid_add : 'a monoid_add};;

type 'a timestamp =
  {comm_monoid_add_timestamp : 'a comm_monoid_add;
    semilattice_sup_timestamp : 'a semilattice_sup; embed_nat : nat -> 'a};;
let embed_nat _A = _A.embed_nat;;

let semilattice_sup_nat =
  ({sup_semilattice_sup = sup_nat; order_semilattice_sup = order_nat} :
    nat semilattice_sup);;

let ab_semigroup_add_nat =
  ({semigroup_add_ab_semigroup_add = semigroup_add_nat} :
    nat ab_semigroup_add);;

let comm_monoid_add_nat =
  ({ab_semigroup_add_comm_monoid_add = ab_semigroup_add_nat;
     monoid_add_comm_monoid_add = monoid_add_nat}
    : nat comm_monoid_add);;

let timestamp_nat =
  ({comm_monoid_add_timestamp = comm_monoid_add_nat;
     semilattice_sup_timestamp = semilattice_sup_nat; embed_nat = embed_nat_nat}
    : nat timestamp);;

let rec list_all p x1 = match p, x1 with p, [] -> true
                   | p, x :: xs -> p x && list_all p xs;;

type 'a set = Set of 'a list | Coset of 'a list;;

let rec membera _A x0 y = match x0, y with [], y -> false
                     | x :: xs, y -> eq _A x y || membera _A xs y;;

let rec member _A
  x xa1 = match x, xa1 with x, Coset xs -> not (membera _A xs x)
    | x, Set xs -> membera _A xs x;;

let rec less_eq_set _A
  a b = match a, b with Coset [], Set [] -> false
    | a, Coset ys -> list_all (fun y -> not (member _A y a)) ys
    | Set xs, b -> list_all (fun x -> member _A x b) xs;;

let rec equal_seta _A a b = less_eq_set _A a b && less_eq_set _A b a;;

let rec equal_set _A = ({equal = equal_seta _A} : 'a set equal);;

let rec enum_all_bool p = p false && p true;;

let rec enum_ex_bool p = p false || p true;;

let enum_boola : bool list = [false; true];;

let countable_bool = (() : bool countable);;

let finite_bool = ({countable_finite = countable_bool} : bool finite);;

let enum_bool =
  ({finite_enum = finite_bool; enum = enum_boola; enum_all = enum_all_bool;
     enum_ex = enum_ex_bool}
    : bool enum);;

let rec equal_boola p pa = match p, pa with p, true -> p
                      | p, false -> not p
                      | true, p -> p
                      | false, p -> not p;;

let equal_bool = ({equal = equal_boola} : bool equal);;

let rec equal_prod _A _B (x1, x2) (y1, y2) = eq _A x1 y1 && eq _B x2 y2;;

type 'a ets = Ets of 'a | Infty;;

type 'a i = Abs_I of ('a * 'a ets);;

let rec rep_I (_A1, _A2) (Abs_I x) = x;;

let rec equal_etsa _A x0 x1 = match x0, x1 with Ets x1, Infty -> false
                        | Infty, Ets x1 -> false
                        | Ets x1, Ets y1 -> eq _A x1 y1
                        | Infty, Infty -> true;;

let rec equal_ets _A = ({equal = equal_etsa _A} : 'a ets equal);;

let rec equal_I (_A1, _A2, _A3)
  x xb =
    equal_prod _A2 (equal_ets _A2) (rep_I (_A1, _A3) x) (rep_I (_A1, _A3) xb);;

type ('a, 'b) regex = Wild | Test of ('a, 'b) formula |
  Plus of ('a, 'b) regex * ('a, 'b) regex |
  Times of ('a, 'b) regex * ('a, 'b) regex | Star of ('a, 'b) regex
and ('a, 'b) formula = Bool of bool | Atom of 'a | Neg of ('a, 'b) formula |
  Bin of (bool -> bool -> bool) * ('a, 'b) formula * ('a, 'b) formula |
  Prev of 'b i * ('a, 'b) formula | Next of 'b i * ('a, 'b) formula |
  Since of ('a, 'b) formula * 'b i * ('a, 'b) formula |
  Until of ('a, 'b) formula * 'b i * ('a, 'b) formula |
  MatchP of 'b i * ('a, 'b) regex | MatchF of 'b i * ('a, 'b) regex;;

let rec equal_formulaa _A (_B1, _B2)
  x0 x1 = match x0, x1 with MatchP (x91, x92), MatchF (x101, x102) -> false
    | MatchF (x101, x102), MatchP (x91, x92) -> false
    | Until (x81, x82, x83), MatchF (x101, x102) -> false
    | MatchF (x101, x102), Until (x81, x82, x83) -> false
    | Until (x81, x82, x83), MatchP (x91, x92) -> false
    | MatchP (x91, x92), Until (x81, x82, x83) -> false
    | Since (x71, x72, x73), MatchF (x101, x102) -> false
    | MatchF (x101, x102), Since (x71, x72, x73) -> false
    | Since (x71, x72, x73), MatchP (x91, x92) -> false
    | MatchP (x91, x92), Since (x71, x72, x73) -> false
    | Since (x71, x72, x73), Until (x81, x82, x83) -> false
    | Until (x81, x82, x83), Since (x71, x72, x73) -> false
    | Next (x61, x62), MatchF (x101, x102) -> false
    | MatchF (x101, x102), Next (x61, x62) -> false
    | Next (x61, x62), MatchP (x91, x92) -> false
    | MatchP (x91, x92), Next (x61, x62) -> false
    | Next (x61, x62), Until (x81, x82, x83) -> false
    | Until (x81, x82, x83), Next (x61, x62) -> false
    | Next (x61, x62), Since (x71, x72, x73) -> false
    | Since (x71, x72, x73), Next (x61, x62) -> false
    | Prev (x51, x52), MatchF (x101, x102) -> false
    | MatchF (x101, x102), Prev (x51, x52) -> false
    | Prev (x51, x52), MatchP (x91, x92) -> false
    | MatchP (x91, x92), Prev (x51, x52) -> false
    | Prev (x51, x52), Until (x81, x82, x83) -> false
    | Until (x81, x82, x83), Prev (x51, x52) -> false
    | Prev (x51, x52), Since (x71, x72, x73) -> false
    | Since (x71, x72, x73), Prev (x51, x52) -> false
    | Prev (x51, x52), Next (x61, x62) -> false
    | Next (x61, x62), Prev (x51, x52) -> false
    | Bin (x41, x42, x43), MatchF (x101, x102) -> false
    | MatchF (x101, x102), Bin (x41, x42, x43) -> false
    | Bin (x41, x42, x43), MatchP (x91, x92) -> false
    | MatchP (x91, x92), Bin (x41, x42, x43) -> false
    | Bin (x41, x42, x43), Until (x81, x82, x83) -> false
    | Until (x81, x82, x83), Bin (x41, x42, x43) -> false
    | Bin (x41, x42, x43), Since (x71, x72, x73) -> false
    | Since (x71, x72, x73), Bin (x41, x42, x43) -> false
    | Bin (x41, x42, x43), Next (x61, x62) -> false
    | Next (x61, x62), Bin (x41, x42, x43) -> false
    | Bin (x41, x42, x43), Prev (x51, x52) -> false
    | Prev (x51, x52), Bin (x41, x42, x43) -> false
    | Neg x3, MatchF (x101, x102) -> false
    | MatchF (x101, x102), Neg x3 -> false
    | Neg x3, MatchP (x91, x92) -> false
    | MatchP (x91, x92), Neg x3 -> false
    | Neg x3, Until (x81, x82, x83) -> false
    | Until (x81, x82, x83), Neg x3 -> false
    | Neg x3, Since (x71, x72, x73) -> false
    | Since (x71, x72, x73), Neg x3 -> false
    | Neg x3, Next (x61, x62) -> false
    | Next (x61, x62), Neg x3 -> false
    | Neg x3, Prev (x51, x52) -> false
    | Prev (x51, x52), Neg x3 -> false
    | Neg x3, Bin (x41, x42, x43) -> false
    | Bin (x41, x42, x43), Neg x3 -> false
    | Atom x2, MatchF (x101, x102) -> false
    | MatchF (x101, x102), Atom x2 -> false
    | Atom x2, MatchP (x91, x92) -> false
    | MatchP (x91, x92), Atom x2 -> false
    | Atom x2, Until (x81, x82, x83) -> false
    | Until (x81, x82, x83), Atom x2 -> false
    | Atom x2, Since (x71, x72, x73) -> false
    | Since (x71, x72, x73), Atom x2 -> false
    | Atom x2, Next (x61, x62) -> false
    | Next (x61, x62), Atom x2 -> false
    | Atom x2, Prev (x51, x52) -> false
    | Prev (x51, x52), Atom x2 -> false
    | Atom x2, Bin (x41, x42, x43) -> false
    | Bin (x41, x42, x43), Atom x2 -> false
    | Atom x2, Neg x3 -> false
    | Neg x3, Atom x2 -> false
    | Bool x1, MatchF (x101, x102) -> false
    | MatchF (x101, x102), Bool x1 -> false
    | Bool x1, MatchP (x91, x92) -> false
    | MatchP (x91, x92), Bool x1 -> false
    | Bool x1, Until (x81, x82, x83) -> false
    | Until (x81, x82, x83), Bool x1 -> false
    | Bool x1, Since (x71, x72, x73) -> false
    | Since (x71, x72, x73), Bool x1 -> false
    | Bool x1, Next (x61, x62) -> false
    | Next (x61, x62), Bool x1 -> false
    | Bool x1, Prev (x51, x52) -> false
    | Prev (x51, x52), Bool x1 -> false
    | Bool x1, Bin (x41, x42, x43) -> false
    | Bin (x41, x42, x43), Bool x1 -> false
    | Bool x1, Neg x3 -> false
    | Neg x3, Bool x1 -> false
    | Bool x1, Atom x2 -> false
    | Atom x2, Bool x1 -> false
    | MatchF (x101, x102), MatchF (y101, y102) ->
        equal_I
          (_B2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
            _B1, _B2.semilattice_sup_timestamp.order_semilattice_sup)
          x101 y101 &&
          equal_regex _A (_B1, _B2) x102 y102
    | MatchP (x91, x92), MatchP (y91, y92) ->
        equal_I
          (_B2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
            _B1, _B2.semilattice_sup_timestamp.order_semilattice_sup)
          x91 y91 &&
          equal_regex _A (_B1, _B2) x92 y92
    | Until (x81, x82, x83), Until (y81, y82, y83) ->
        equal_formulaa _A (_B1, _B2) x81 y81 &&
          (equal_I
             (_B2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
               _B1, _B2.semilattice_sup_timestamp.order_semilattice_sup)
             x82 y82 &&
            equal_formulaa _A (_B1, _B2) x83 y83)
    | Since (x71, x72, x73), Since (y71, y72, y73) ->
        equal_formulaa _A (_B1, _B2) x71 y71 &&
          (equal_I
             (_B2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
               _B1, _B2.semilattice_sup_timestamp.order_semilattice_sup)
             x72 y72 &&
            equal_formulaa _A (_B1, _B2) x73 y73)
    | Next (x61, x62), Next (y61, y62) ->
        equal_I
          (_B2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
            _B1, _B2.semilattice_sup_timestamp.order_semilattice_sup)
          x61 y61 &&
          equal_formulaa _A (_B1, _B2) x62 y62
    | Prev (x51, x52), Prev (y51, y52) ->
        equal_I
          (_B2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
            _B1, _B2.semilattice_sup_timestamp.order_semilattice_sup)
          x51 y51 &&
          equal_formulaa _A (_B1, _B2) x52 y52
    | Bin (x41, x42, x43), Bin (y41, y42, y43) ->
        equal_funa enum_bool (equal_fun enum_bool equal_bool) x41 y41 &&
          (equal_formulaa _A (_B1, _B2) x42 y42 &&
            equal_formulaa _A (_B1, _B2) x43 y43)
    | Neg x3, Neg y3 -> equal_formulaa _A (_B1, _B2) x3 y3
    | Atom x2, Atom y2 -> eq _A x2 y2
    | Bool x1, Bool y1 -> equal_boola x1 y1
and equal_regex _A (_B1, _B2)
  x0 x1 = match x0, x1 with Times (x41, x42), Star x5 -> false
    | Star x5, Times (x41, x42) -> false
    | Plus (x31, x32), Star x5 -> false
    | Star x5, Plus (x31, x32) -> false
    | Plus (x31, x32), Times (x41, x42) -> false
    | Times (x41, x42), Plus (x31, x32) -> false
    | Test x2, Star x5 -> false
    | Star x5, Test x2 -> false
    | Test x2, Times (x41, x42) -> false
    | Times (x41, x42), Test x2 -> false
    | Test x2, Plus (x31, x32) -> false
    | Plus (x31, x32), Test x2 -> false
    | Wild, Star x5 -> false
    | Star x5, Wild -> false
    | Wild, Times (x41, x42) -> false
    | Times (x41, x42), Wild -> false
    | Wild, Plus (x31, x32) -> false
    | Plus (x31, x32), Wild -> false
    | Wild, Test x2 -> false
    | Test x2, Wild -> false
    | Star x5, Star y5 -> equal_regex _A (_B1, _B2) x5 y5
    | Times (x41, x42), Times (y41, y42) ->
        equal_regex _A (_B1, _B2) x41 y41 && equal_regex _A (_B1, _B2) x42 y42
    | Plus (x31, x32), Plus (y31, y32) ->
        equal_regex _A (_B1, _B2) x31 y31 && equal_regex _A (_B1, _B2) x32 y32
    | Test x2, Test y2 -> equal_formulaa _A (_B1, _B2) x2 y2
    | Wild, Wild -> true;;

let rec equal_formula _A (_B1, _B2) =
  ({equal = equal_formulaa _A (_B1, _B2)} : ('a, 'b) formula equal);;

let equal_literal = ({equal = (fun a b -> ((a : string) = b))} : string equal);;

let ord_integer = ({less_eq = Z.leq; less = Z.lt} : Z.t ord);;

type num = One | Bit0 of num | Bit1 of num;;

type ('a, 'b, 'c, 'd, 'e) window_ext =
  Window_ext of
    nat * 'c * 'd * nat * 'c * 'd * ('a * ('a * ('b * nat) option)) list *
      ('a * 'b) list * 'e;;

type transition = Cond_eps of nat * nat | Wild_trans of nat |
  Split_trans of nat * nat;;

type ('a, 'b, 'c) vydra = VYDRA of (('a, 'b, 'c) vydra_rec * ('b * bool)) option
and ('a, 'b, 'c) vydra_rec = VYDRA_Bool of bool * 'c | VYDRA_Atom of 'a * 'c |
  VYDRA_Neg of ('a, 'b, 'c) vydra |
  VYDRA_Bin of (bool -> bool -> bool) * ('a, 'b, 'c) vydra * ('a, 'b, 'c) vydra
  | VYDRA_MatchP of
      'b i * transition array * nat *
        (nat set, 'b, (('c * 'b) option), (('a, 'b, 'c) vydra list), unit)
          window_ext
  | VYDRA_MatchF of
      'b i * transition array * nat *
        (nat set, 'b, (('c * 'b) option), (('a, 'b, 'c) vydra list), unit)
          window_ext;;

type ('a, 'b, 'c, 'd, 'e, 'f) args_ext =
  Args_ext of
    'b * ('b -> 'a -> 'b) * ('b -> 'a -> bool) * ('d -> ('d * 'c) option) *
      ('d -> 'c option) * ('e -> ('e * 'a) option) * ('e -> 'a option) * 'f;;

let one_nat : nat = Nat (Z.of_int 1);;

let rec suc n = plus_nata n one_nat;;

let rec list_ex p x1 = match p, x1 with p, [] -> false
                  | p, x :: xs -> p x || list_ex p xs;;

let rec bex (Set xs) p = list_ex p xs;;

let rec comp f g = (fun x -> f (g x));;

let rec fold f x1 s = match f, x1, s with f, x :: xs, s -> fold f xs (f x s)
               | f, [], s -> s;;

let rec baseF _B phi = Times (Test phi, Wild);;

let rec baseP _B phi = Times (Wild, Test phi);;

let rec map f x1 = match f, x1 with f, [] -> []
              | f, x21 :: x22 -> f x21 :: map f x22;;

let rec image f (Set xs) = Set (map f xs);;

let rec sub asa n = IArray.sub' (asa, integer_of_nat n);;

let rec foldl f a x2 = match f, a, x2 with f, a, [] -> a
                | f, a, x :: xs -> foldl f (f a x) xs;;

let rec map_of _A
  x0 k = match x0, k with
    (l, v) :: ps, k -> (if eq _A l k then Some v else map_of _A ps k)
    | [], k -> None;;

let rec t0 _B
  init run_event =
    (match run_event init with None -> None | Some (e, (t, _)) -> Some (e, t));;

let rec filter
  p x1 = match p, x1 with p, [] -> []
    | p, x :: xs -> (if p x then x :: filter p xs else filter p xs);;

let rec removeAll _A
  x xa1 = match x, xa1 with x, [] -> []
    | x, y :: xs ->
        (if eq _A x y then removeAll _A x xs else y :: removeAll _A x xs);;

let rec inserta _A x xs = (if membera _A xs x then xs else x :: xs);;

let rec insert _A
  x xa1 = match x, xa1 with x, Coset xs -> Coset (removeAll _A x xs)
    | x, Set xs -> Set (inserta _A x xs);;

let rec sup_set _A
  x0 a = match x0, a with
    Coset xs, a -> Coset (filter (fun x -> not (member _A x a)) xs)
    | Set xs, a -> fold (insert _A) xs a;;

let bot_set : 'a set = Set [];;

let rec sup_seta _A (Set xs) = fold (sup_set _A) xs bot_set;;

let rec step_eps_sucs
  transs len bs q =
    (if less_nat q len
      then (match sub transs q
             with Cond_eps (p, n) ->
               (if sub bs n then insert equal_nat p bot_set else bot_set)
             | Wild_trans _ -> bot_set
             | Split_trans (p, pa) ->
               insert equal_nat p (insert equal_nat pa bot_set))
      else bot_set);;

let rec step_eps_set
  transs len bs r = sup_seta equal_nat (image (step_eps_sucs transs len bs) r);;

let rec eps_closure_set
  transs len r bs =
    (let ra = sup_set equal_nat r (step_eps_set transs len bs r) in
      (if equal_seta equal_nat r ra then r
        else eps_closure_set transs len ra bs));;

let rec step_wild_sucs
  transs len q =
    (if less_nat q len
      then (match sub transs q with Cond_eps (_, _) -> bot_set
             | Wild_trans p -> insert equal_nat p bot_set
             | Split_trans (_, _) -> bot_set)
      else bot_set);;

let rec step_wild_set
  transs len r = sup_seta equal_nat (image (step_wild_sucs transs len) r);;

let rec delta
  transs len r bs = step_wild_set transs len (eps_closure_set transs len r bs);;

let rec minus_nat
  m n = Nat (max ord_integer Z.zero
              (Z.sub (integer_of_nat m) (integer_of_nat n)));;

let rec less_ets _A
  x0 uu = match x0, uu with
    Ets x, Ets y -> less _A.preorder_order.ord_preorder x y
    | Ets x, Infty -> true
    | Infty, uu -> false;;

let rec less_eq_ets (_A1, _A2) x y = less_ets _A2 x y || equal_etsa _A1 x y;;

let rec plus_ets _A
  x0 uu = match x0, uu with Ets x, Ets y -> Ets (plus _A x y)
    | Infty, uu -> Infty
    | Ets v, Infty -> Infty;;

let rec w_read_t
  (Args_ext
    (w_init, w_step, w_accept, w_run_t, w_read_t, w_run_sub, w_read_sub, more))
    = w_read_t;;

let rec w_ti
  (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) = w_ti;;

let rec w_j
  (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) = w_j;;

let rec w_i
  (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) = w_i;;

let rec the (Some x2) = x2;;

let rec fst (x1, x2) = x1;;

let rec left (_A1, _A2) x = fst (rep_I (_A1, _A2) x);;

let rec matchP_loop_cond _A
  args i t =
    (fun w ->
      less_nat (w_i w) (w_j w) &&
        less_eq
          _A.semilattice_sup_timestamp.order_semilattice_sup.preorder_order.ord_preorder
          (plus _A.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.semigroup_add_monoid_add.plus_semigroup_add
            (the (w_read_t args (w_ti w)))
            (left (_A.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
                    _A.semilattice_sup_timestamp.order_semilattice_sup)
              i))
          t);;

let rec w_read_sub
  (Args_ext
    (w_init, w_step, w_accept, w_run_t, w_read_t, w_run_sub, w_read_sub, more))
    = w_read_sub;;

let rec whilea b c s = (if b s then whilea b c (c s) else s);;

let rec w_accept
  (Args_ext
    (w_init, w_step, w_accept, w_run_t, w_read_t, w_run_sub, w_read_sub, more))
    = w_accept;;

let rec w_tj
  (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) = w_tj;;

let rec w_sj
  (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) = w_sj;;

let rec w_e
  (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) = w_e;;

let rec w_ti_update
  w_tia (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) =
    Window_ext (w_i, w_tia w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more);;

let rec w_si_update
  w_sia (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) =
    Window_ext (w_i, w_ti, w_sia w_si, w_j, w_tj, w_sj, w_s, w_e, more);;

let rec w_s_update
  w_sa (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) =
    Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_sa w_s, w_e, more);;

let rec w_i_update
  w_ia (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) =
    Window_ext (w_ia w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more);;

let rec w_e_update
  w_ea (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) =
    Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_ea w_e, more);;

let rec w_run_sub
  (Args_ext
    (w_init, w_step, w_accept, w_run_t, w_read_t, w_run_sub, w_read_sub, more))
    = w_run_sub;;

let rec w_run_t
  (Args_ext
    (w_init, w_step, w_accept, w_run_t, w_read_t, w_run_sub, w_read_sub, more))
    = w_run_t;;

let rec w_si
  (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) = w_si;;

let rec update _A
  k v x2 = match k, v, x2 with k, v, [] -> [(k, v)]
    | k, v, p :: ps ->
        (if eq _A (fst p) k then (k, v) :: ps else p :: update _A k v ps);;

let rec mmap_update _A = update _A;;

let rec mmap_lookup _A = map_of _A;;

let rec w_step
  (Args_ext
    (w_init, w_step, w_accept, w_run_t, w_read_t, w_run_sub, w_read_sub, more))
    = w_step;;

let rec w_init
  (Args_ext
    (w_init, w_step, w_accept, w_run_t, w_read_t, w_run_sub, w_read_sub, more))
    = w_init;;

let rec w_s
  (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) = w_s;;

let rec mmap_keys kvs = Set (map fst kvs);;

let rec loop_cond _A _D
  j = (fun (i, (_, (_, (q, (s, _))))) ->
        less _A i j && not (member _D q (mmap_keys s)));;

let rec mmap_combine _A
  k v c x3 = match k, v, c, x3 with k, v, c, [] -> [(k, v)]
    | k, v, c, p :: ps ->
        (let (ka, va) = p in
          (if eq _A k ka then (k, c va v) :: ps
            else p :: mmap_combine _A k v c ps));;

let rec mmap_fold _A
  m f c =
    foldl (fun r p -> (let (k, v) = f p in mmap_combine _A k v c r)) [] m;;

let rec drop_cur
  i = (fun (q, tstp) ->
        (q, (match tstp with None -> tstp
              | Some (_, tp) -> (if equal_nata tp i then None else tstp))));;

let rec adv_d _A
  step i b s =
    mmap_fold _A s (fun (x, v) -> (step x b, drop_cur i v)) (fun x _ -> x);;

let rec loop_body _A
  step accept run_t run_sub =
    (fun (i, (ti, (si, (q, (s, tstp))))) ->
      (let Some (tia, t) = run_t ti in
       let Some (sia, b) = run_sub si in
        (suc i,
          (tia, (sia, (step q b,
                        (adv_d _A step i b s,
                          (if accept q b then Some (t, i) else tstp))))))));;

let rec adv_start _B _C
  args w =
    (let init = w_init args in
     let step = w_step args in
     let accept = w_accept args in
     let run_t = w_run_t args in
     let run_sub = w_run_sub args in
     let i = w_i w in
     let ti = w_ti w in
     let si = w_si w in
     let j = w_j w in
     let _ = w_tj w in
     let _ = w_sj w in
     let s = w_s w in
     let e = w_e w in
     let Some (tia, t) = run_t ti in
     let Some (sia, bs) = run_sub si in
     let sa = adv_d _B step i bs s in
     let ea = mmap_update _B (fst (the (mmap_lookup _B s init))) t e in
     let (_, (_, (_, (q_cur, (s_cur, tstp_cur))))) =
       whilea (loop_cond ord_nat _B j) (loop_body _B step accept run_t run_sub)
         (suc i, (tia, (sia, (init, (sa, None)))))
       in
     let sb =
       mmap_update _B init
         (match mmap_lookup _B s_cur q_cur with None -> (q_cur, tstp_cur)
           | Some (q, tstp) ->
             (match tstp with None -> (q, tstp_cur) | Some (_, _) -> (q, tstp)))
         sa
       in
      w_e_update (fun _ -> ea)
        (w_s_update (fun _ -> sb)
          (w_si_update (fun _ -> sia)
            (w_ti_update (fun _ -> tia) (w_i_update (fun _ -> suc i) w)))));;

let rec ex_key _A
  p m = bex (mmap_keys m) (fun k -> p k (the (mmap_lookup _A m k)));;

let rec w_tj_update
  w_tja (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) =
    Window_ext (w_i, w_ti, w_si, w_j, w_tja w_tj, w_sj, w_s, w_e, more);;

let rec w_sj_update
  w_sja (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) =
    Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sja w_sj, w_s, w_e, more);;

let rec w_j_update
  w_ja (Window_ext (w_i, w_ti, w_si, w_j, w_tj, w_sj, w_s, w_e, more)) =
    Window_ext (w_i, w_ti, w_si, w_ja w_j, w_tj, w_sj, w_s, w_e, more);;

let rec mmap_map f m = map (fun (k, v) -> (k, f k v)) m;;

let rec fold_sup _A _B
  m f = mmap_fold _A m (fun (x, a) -> (f x, a))
          (sup _B.semilattice_sup_timestamp.sup_semilattice_sup);;

let rec adv_end _B _C
  args w =
    (let _ = w_init args in
     let step = w_step args in
     let accept = w_accept args in
     let run_t = w_run_t args in
     let run_sub = w_run_sub args in
     let _ = w_i w in
     let _ = w_ti w in
     let _ = w_si w in
     let j = w_j w in
     let tj = w_tj w in
     let sj = w_sj w in
     let s = w_s w in
     let e = w_e w in
     let Some (tja, t) = run_t tj in
     let Some (sja, bs) = run_sub sj in
     let sa =
       mmap_map
         (fun _ (q, tstp) ->
           (step q bs, (if accept q bs then Some (t, j) else tstp)))
         s
       in
     let ea = fold_sup _B _C e (fun q -> step q bs) in
      w_e_update (fun _ -> ea)
        (w_s_update (fun _ -> sa)
          (w_sj_update (fun _ -> sja)
            (w_tj_update (fun _ -> tja) (w_j_update (fun _ -> suc j) w)))));;

let rec snd (x1, x2) = x2;;

let rec right (_A1, _A2) x = snd (rep_I (_A1, _A2) x);;

let rec eval_matchP (_A1, _A2)
  args i w =
    (match w_read_t args (w_tj w) with None -> None
      | Some t ->
        (match w_read_sub args (w_sj w) with None -> None
          | Some b ->
            (let wa =
               whilea (matchP_loop_cond _A2 args i t)
                 (adv_start (equal_set equal_nat) _A2 args) w
               in
             let beta =
               ex_key (equal_set equal_nat)
                 (fun q ta ->
                   less_eq_ets
                     (_A1, _A2.semilattice_sup_timestamp.order_semilattice_sup)
                     (Ets t)
                     (plus_ets
                       _A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.semigroup_add_monoid_add.plus_semigroup_add
                       (Ets ta)
                       (right
                         (_A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
                           _A2.semilattice_sup_timestamp.order_semilattice_sup)
                         i)) &&
                     w_accept args q b)
                 (w_e wa) ||
                 eq _A1
                   (left (_A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
                           _A2.semilattice_sup_timestamp.order_semilattice_sup)
                     i)
                   (zero _A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add) &&
                   w_accept args (insert equal_nat zero_nata bot_set) b
               in
              Some ((t, beta), adv_end (equal_set equal_nat) _A2 args wa))));;

let rec is_none = function Some x -> false
                  | None -> true;;

let rec matchF_loop_cond (_A1, _A2)
  args i t =
    (fun w ->
      (match w_read_t args (w_tj w) with None -> false
        | Some ta ->
          less_eq_ets (_A1, _A2.semilattice_sup_timestamp.order_semilattice_sup)
            (Ets ta)
            (plus_ets
              _A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.semigroup_add_monoid_add.plus_semigroup_add
              (Ets t)
              (right
                (_A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
                  _A2.semilattice_sup_timestamp.order_semilattice_sup)
                i)) &&
            not (is_none (w_read_sub args (w_sj w)))));;

let rec eval_matchF (_A1, _A2)
  args i w =
    (match w_read_t args (w_ti w) with None -> None
      | Some t ->
        (let wa =
           whilea (matchF_loop_cond (_A1, _A2) args i t)
             (adv_end (equal_set equal_nat) _A2 args) w
           in
          (match w_read_t args (w_tj wa) with None -> None
            | Some ta ->
              (if less_eq_ets
                    (_A1, _A2.semilattice_sup_timestamp.order_semilattice_sup)
                    (Ets ta)
                    (plus_ets
                      _A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.semigroup_add_monoid_add.plus_semigroup_add
                      (Ets t)
                      (right
                        (_A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
                          _A2.semilattice_sup_timestamp.order_semilattice_sup)
                        i))
                then None
                else (let beta =
                        (match
                          snd (the (mmap_lookup (equal_set equal_nat) (w_s wa)
                                     (insert equal_nat zero_nata bot_set)))
                          with None -> false
                          | Some tstp ->
                            less_eq
                              _A2.semilattice_sup_timestamp.order_semilattice_sup.preorder_order.ord_preorder
                              (plus _A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.semigroup_add_monoid_add.plus_semigroup_add
                                t (left (_A2.comm_monoid_add_timestamp.monoid_add_comm_monoid_add.zero_monoid_add,
  _A2.semilattice_sup_timestamp.order_semilattice_sup)
                                    i))
                              (fst tstp))
                        in
                       Some ((t, beta),
                              adv_start (equal_set equal_nat) _A2 args
                                wa))))));;

let rec read_subs
  read =
    (fun vs ->
      (let vsa = map read vs in
        (if list_ex is_none vsa then None
          else Some (IArray.of_list (map (comp snd the) vsa)))));;

let rec init_args
  (init, (step, accept)) (run_t, read_t) (run_sub, read_sub) =
    Args_ext (init, step, accept, run_t, read_t, run_sub, read_sub, ());;

let rec run_subs
  run = (fun vs ->
          (let vsa = map run vs in
            (if list_ex is_none vsa then None
              else Some (map (comp fst the) vsa,
                          IArray.of_list
                            (map (comp (comp snd snd) the) vsa)))));;

let rec read_t _B = function None -> None
                    | Some (e, t) -> Some t;;

let rec run_t _B
  run_event x1 = match run_event, x1 with run_event, None -> None
    | run_event, Some (e, t) ->
        (match run_event e with None -> Some (None, t)
          | Some (ea, (ta, _)) -> Some (Some (ea, ta), t));;

let rec read _B = function VYDRA None -> None
                  | VYDRA (Some (v, x)) -> Some x;;

let rec accept
  transs len r bs = member equal_nat len (eps_closure_set transs len r bs);;

let rec run (_B1, _B2) _C
  run_event n x2 = match run_event, n, x2 with run_event, n, VYDRA None -> None
    | run_event, n, VYDRA (Some (v, x)) ->
        Some (VYDRA (run_rec (_B1, _B2) _C run_event n v), x)
and run_rec (_B1, _B2) _C
  run_event n x2 = match run_event, n, x2 with
    run_event, n, VYDRA_Bool (b, e) ->
      (match run_event e with None -> None
        | Some (ea, (t, _)) -> Some (VYDRA_Bool (b, ea), (t, b)))
    | run_event, n, VYDRA_Atom (a, e) ->
        (match run_event e with None -> None
          | Some (ea, (t, x)) -> Some (VYDRA_Atom (a, ea), (t, member _C a x)))
    | run_event, n, VYDRA_Neg v ->
        (if equal_nata n zero_nata then None
          else (match run (_B1, _B2) _C run_event (minus_nat n one_nat) v
                 with None -> None
                 | Some (va, (t, b)) -> Some (VYDRA_Neg va, (t, not b))))
    | run_event, n, VYDRA_Bin (v, va, vb) ->
        (if equal_nata n zero_nata then None
          else (match run (_B1, _B2) _C run_event (minus_nat n one_nat) va
                 with None -> None
                 | Some (vl, (t, bl)) ->
                   (match run (_B1, _B2) _C run_event (minus_nat n one_nat) vb
                     with None -> None
                     | Some (vr, (_, br)) ->
                       Some (VYDRA_Bin (v, vl, vr), (t, v bl br)))))
    | run_event, n, VYDRA_MatchP (v, va, vb, vc) ->
        (if equal_nata n zero_nata then None
          else (match
                 eval_matchP (_B1, _B2)
                   (init_args
                     (insert equal_nat zero_nata bot_set,
                       (delta va vb, accept va vb))
                     (run_t _B2 run_event, read_t _B2)
                     (run_subs
                        (run (_B1, _B2) _C run_event (minus_nat n one_nat)),
                       read_subs (read _B2)))
                   v vc
                 with None -> None
                 | Some ((t, b), w) ->
                   Some (VYDRA_MatchP (v, va, vb, w), (t, b))))
    | run_event, n, VYDRA_MatchF (v, va, vb, vc) ->
        (if equal_nata n zero_nata then None
          else (match
                 eval_matchF (_B1, _B2)
                   (init_args
                     (insert equal_nat zero_nata bot_set,
                       (delta va vb, accept va vb))
                     (run_t _B2 run_event, read_t _B2)
                     (run_subs
                        (run (_B1, _B2) _C run_event (minus_nat n one_nat)),
                       read_subs (read _B2)))
                   v vc
                 with None -> None
                 | Some ((t, b), w) ->
                   Some (VYDRA_MatchF (v, va, vb, w), (t, b))));;

let rec collect_subfmlas _A (_B1, _B2)
  x0 phis = match x0, phis with Wild, phis -> phis
    | Test phi, phis ->
        (if membera (equal_formula _A (_B1, _B2)) phis phi then phis
          else phis @ [phi])
    | Plus (r, s), phis ->
        collect_subfmlas _A (_B1, _B2) s (collect_subfmlas _A (_B1, _B2) r phis)
    | Times (r, s), phis ->
        collect_subfmlas _A (_B1, _B2) s (collect_subfmlas _A (_B1, _B2) r phis)
    | Star r, phis -> collect_subfmlas _A (_B1, _B2) r phis;;

let rec gen_length n x1 = match n, x1 with n, x :: xs -> gen_length (suc n) xs
                     | n, [] -> n;;

let rec size_list x = gen_length zero_nata x;;

let rec state_cnt _B
  = function Wild -> one_nat
    | Test uu -> one_nat
    | Plus (r, s) ->
        plus_nata (plus_nata one_nat (state_cnt _B r)) (state_cnt _B s)
    | Times (r, s) -> plus_nata (state_cnt _B r) (state_cnt _B s)
    | Star r -> plus_nata one_nat (state_cnt _B r);;

let rec pos _A
  a x1 = match a, x1 with a, [] -> None
    | a, x :: xs ->
        (if eq _A a x then Some zero_nata
          else (match pos _A a xs with None -> None | Some n -> Some (suc n)));;

let rec build_nfa_impl _A (_B1, _B2)
  x0 x1 = match x0, x1 with Wild, (q0, (qf, phis)) -> [Wild_trans qf]
    | Test phi, (q0, (qf, phis)) ->
        (match pos (equal_formula _A (_B1, _B2)) phi phis
          with None -> [Cond_eps (qf, size_list phis)]
          | Some n -> [Cond_eps (qf, n)])
    | Plus (r, s), (q0, (qf, phis)) ->
        (let ts_r =
           build_nfa_impl _A (_B1, _B2) r (plus_nata q0 one_nat, (qf, phis)) in
         let ts_s =
           build_nfa_impl _A (_B1, _B2) s
             (plus_nata (plus_nata q0 one_nat) (state_cnt _B2 r),
               (qf, collect_subfmlas _A (_B1, _B2) r phis))
           in
          Split_trans
            (plus_nata q0 one_nat,
              plus_nata (plus_nata q0 one_nat) (state_cnt _B2 r)) ::
            ts_r @ ts_s)
    | Times (r, s), (q0, (qf, phis)) ->
        (let ts_r =
           build_nfa_impl _A (_B1, _B2) r
             (q0, (plus_nata q0 (state_cnt _B2 r), phis))
           in
         let a =
           build_nfa_impl _A (_B1, _B2) s
             (plus_nata q0 (state_cnt _B2 r),
               (qf, collect_subfmlas _A (_B1, _B2) r phis))
           in
          ts_r @ a)
    | Star r, (q0, (qf, phis)) ->
        (let a =
           build_nfa_impl _A (_B1, _B2) r (plus_nata q0 one_nat, (q0, phis)) in
          Split_trans (plus_nata q0 one_nat, qf) :: a);;

let rec init_window
  args t0 sub =
    Window_ext
      (zero_nata, t0, sub, zero_nata, t0, sub,
        [(w_init args, (w_init args, None))], [], ());;

let rec msize_fmla _B
  = function Bool b -> zero_nata
    | Atom a -> zero_nata
    | Neg phi -> suc (msize_fmla _B phi)
    | Bin (f, phi, psi) ->
        suc (plus_nata (msize_fmla _B phi) (msize_fmla _B psi))
    | Prev (i, phi) -> suc (msize_fmla _B phi)
    | Next (i, phi) -> suc (msize_fmla _B phi)
    | Since (phi, i, psi) ->
        suc (max ord_nat (msize_fmla _B phi) (msize_fmla _B psi))
    | Until (phi, i, psi) ->
        suc (max ord_nat (msize_fmla _B phi) (msize_fmla _B psi))
    | MatchP (i, r) -> suc (msize_regex _B r)
    | MatchF (i, r) -> suc (msize_regex _B r)
and msize_regex _B
  = function Wild -> zero_nata
    | Test phi -> msize_fmla _B phi
    | Plus (r, s) -> max ord_nat (msize_regex _B r) (msize_regex _B s)
    | Times (r, s) -> max ord_nat (msize_regex _B r) (msize_regex _B s)
    | Star r -> msize_regex _B r;;

let rec possiblyP _B phi i r = MatchP (i, Times (Test phi, r));;

let rec possiblyF _B r i phi = MatchF (i, Times (r, Test phi));;

let rec suba (_B1, _B2) _C
  init run_event n phi =
    VYDRA (run_rec (_B1, _B2) _C run_event n
            (sub_rec (_B1, _B2) _C init run_event n phi))
and sub_rec (_B1, _B2) _C
  init run_event n x3 = match init, run_event, n, x3 with
    init, run_event, n, Bool b -> VYDRA_Bool (b, init)
    | init, run_event, n, Atom a -> VYDRA_Atom (a, init)
    | init, run_event, n, Prev (i, phi) ->
        sub_rec (_B1, _B2) _C init run_event n (possiblyP _B2 phi i Wild)
    | init, run_event, n, Next (i, phi) ->
        sub_rec (_B1, _B2) _C init run_event n (possiblyF _B2 Wild i phi)
    | init, run_event, n, Since (phi, i, psi) ->
        sub_rec (_B1, _B2) _C init run_event n
          (possiblyP _B2 psi i (Star (baseP _B2 phi)))
    | init, run_event, n, Until (phi, i, psi) ->
        sub_rec (_B1, _B2) _C init run_event n
          (possiblyF _B2 (Star (baseF _B2 phi)) i psi)
    | init, run_event, n, Neg v ->
        (if equal_nata n zero_nata then failwith "undefined"
          else VYDRA_Neg
                 (suba (_B1, _B2) _C init run_event (minus_nat n one_nat) v))
    | init, run_event, n, Bin (v, va, vb) ->
        (if equal_nata n zero_nata then failwith "undefined"
          else VYDRA_Bin
                 (v, suba (_B1, _B2) _C init run_event (minus_nat n one_nat) va,
                   suba (_B1, _B2) _C init run_event (minus_nat n one_nat) vb))
    | init, run_event, n, MatchP (v, va) ->
        (if equal_nata n zero_nata then failwith "undefined"
          else (let qf = state_cnt _B2 va in
                let transs =
                  IArray.of_list
                    (build_nfa_impl _C (_B1, _B2) va (zero_nata, (qf, [])))
                  in
                 VYDRA_MatchP
                   (v, transs, qf,
                     init_window
                       (init_args
                         (insert equal_nat zero_nata bot_set,
                           (delta transs qf, accept transs qf))
                         (run_t _B2 run_event, read_t _B2)
                         (run_subs
                            (run (_B1, _B2) _C run_event
                              (msize_fmla _B2 (MatchP (v, va)))),
                           read_subs (read _B2)))
                       (t0 _B2 init run_event)
                       (map (suba (_B1, _B2) _C init run_event
                              (minus_nat n one_nat))
                         (collect_subfmlas _C (_B1, _B2) va [])))))
    | init, run_event, n, MatchF (v, va) ->
        (if equal_nata n zero_nata then failwith "undefined"
          else (let qf = state_cnt _B2 va in
                let transs =
                  IArray.of_list
                    (build_nfa_impl _C (_B1, _B2) va (zero_nata, (qf, [])))
                  in
                 VYDRA_MatchF
                   (v, transs, qf,
                     init_window
                       (init_args
                         (insert equal_nat zero_nata bot_set,
                           (delta transs qf, accept transs qf))
                         (run_t _B2 run_event, read_t _B2)
                         (run_subs
                            (run (_B1, _B2) _C run_event
                              (msize_fmla _B2 (MatchP (v, va)))),
                           read_subs (read _B2)))
                       (t0 _B2 init run_event)
                       (map (suba (_B1, _B2) _C init run_event
                              (minus_nat n one_nat))
                         (collect_subfmlas _C (_B1, _B2) va [])))));;

let rec interval (_A1, _A2, _A3)
  l r = Abs_I (if less_eq _A3.preorder_order.ord_preorder (zero _A1) l &&
                    less_eq_ets (_A2, _A3) (Ets l) r
                then (l, r) else rep_I (_A1, _A3) (failwith "undefined"));;

let rec mk_event x = Set x;;

let rec run_vydra x = run (equal_nat, timestamp_nat) equal_literal x;;

let rec sub_vydra x = suba (equal_nat, timestamp_nat) equal_literal x;;

let rec nat_interval x = interval (zero_nat, equal_nat, order_nat) x;;

let rec nat_of_integer k = Nat (max ord_integer Z.zero k);;

let rec msize_fmla_vydra x = msize_fmla timestamp_nat x;;

end;; (*struct VYDRA*)
