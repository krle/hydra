#!/bin/bash

cd $(dirname $0)

make correctness
cd correctness

for SIZE in 1 2 3 4 8 16; do
    for MAXR in 0 1 2 4 8; do
        for SEED in {0..3}; do
            timeout "300.0s" ./test_single.sh $SIZE $MAXR $SEED
         done
     done
done
