#!/bin/bash

cd $(dirname $0)
cd packages

cat all.deps | xargs sudo dpkg -i

tar -xzf ae-opam-bundle.tar.gz
cd ae-opam-bundle
./bootstrap.sh
./configure.sh
echo "" | ./compile.sh
CWD=$(pwd)
export PATH="${CWD}/bootstrap/bin:$PATH"
eval $(opam env --root "${CWD}/opam" --set-root)
cd ..

cd aerial
make
cd ..
cp aerial/aerial.native ../aerial.native

cd monpoly
make
cd ..
cp monpoly/monpoly ../monpoly
