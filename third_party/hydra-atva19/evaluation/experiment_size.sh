#!/bin/bash

FMLAS=$1

PREFIX="exp_size"
RANGE=$(seq 2 2 50)
MAX_INT=16

for SIZE in ${RANGE}
do
    for((F=0; F<${FMLAS}; F++))
    do
        NAME=${PREFIX}_${SIZE}_${F}
        ./gen_fmla ${SIZE} ${MAX_INT} ${F} > fmlas/${NAME}.mtl
        cat fmlas/${NAME}.mtl | sed -E "s/(p[0-9]+)/\\1()/g" | sed -E "s/INFINITY\]/*)/g" > fmlas/${NAME}.mfotl
        ln -s shared.log logs/${NAME}.log
        ln -s shared.mlog logs/${NAME}.mlog
    done
done

./experiment.sh ${PREFIX} "${RANGE}" ${FMLAS}
