import re
import sys

n=int(sys.argv[1])
c=int(sys.argv[2])

reg="^\| *([0-9]+) *[^|]*"
for i in range(n):
    reg+="\|[^|]*"
reg+="\|"
for i in range(c+1):
    reg+=" *([0-9e+-.]+) ± ([0-9e+-.]+)"
r=re.compile(reg)

for l in sys.stdin.readlines():
    m=r.search(l)
    if m is None:
        continue
    print(m.group(1)+" "+m.group(2+2*c)+" "+m.group(3+2*c))
