#!/bin/bash

FMLAS=1
RANGE=$(seq 1 1 11)

for i in ${RANGE}
do
    ./gen_exp "exp_wc_${i}" ${i}
    cat "logs/exp_wc_${i}_0.log" | sed -E "s/([A-Za-z][A-Za-z0-9]*)/\\1()/g" > "logs/exp_wc_${i}_0.mlog"
done

./experiment.sh "exp_wc" "${RANGE}" ${FMLAS}
