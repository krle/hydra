import math
import sys

def mean(v):
    return sum(v)/len(v)

def std(v):
    m=mean(v)
    v=[abs(x-m)**2 for x in v]
    return math.sqrt(mean(v))

n=int(sys.argv[1])

v=[[] for i in range(n)]

numbers=input().split(",")
for tup in numbers:
    if tup=="":
        break
    comp=tup.split(" ")
    for i in range(n):
        v[i].append(float(comp[i]))

for i in range(n):
    if i==1: # scale the memory usage from KB down to MB
        v[i]=[t/1000.0 for t in v[i]]
    sys.stdout.write("{0:>20}".format(" {0:.3g}".format(mean(v[i]))+" ± {0:.3g}".format(std(v[i]))))
