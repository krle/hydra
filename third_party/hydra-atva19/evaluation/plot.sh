#!/bin/bash

DATAGG=$1
DATSINGLE=$2
OUT=$3
COL=$4
TITLE=$5
XLABEL=$6
XRANGE=$7
YLABEL=$8
YRANGE=$9
FONT=${10}
LEGEND=${11}
LW=${12}
SCALEX=${13}

python3 ./parse.py 0 ${COL} < ${DATSINGLE} > tmp-1S.dat
python3 ./parse.py 0 ${COL} < ${DATAGG} > tmp-1A.dat
python3 ./parse.py 1 ${COL} < ${DATSINGLE} > tmp-2S.dat
python3 ./parse.py 1 ${COL} < ${DATAGG} > tmp-2A.dat
python3 ./parse.py 2 ${COL} < ${DATSINGLE} > tmp-3S.dat
python3 ./parse.py 2 ${COL} < ${DATAGG} > tmp-3A.dat

echo "set terminal postscript eps enhanced color size 4.5,3 font 'Helvetica,${FONT}'; set output '${OUT}'" > tmp.gnuplot
echo "set size ratio 0.66" >> tmp.gnuplot
echo "set key ${LEGEND}" >> tmp.gnuplot
echo "set title '${TITLE}'" >> tmp.gnuplot
echo "set xlabel '${XLABEL}'" >> tmp.gnuplot
echo "set xrange ${XRANGE}" >> tmp.gnuplot
echo "set ylabel '${YLABEL}'" >> tmp.gnuplot
echo "set yrange ${YRANGE}" >> tmp.gnuplot
echo "plot 'tmp-2S.dat' using (\$1*${SCALEX}):2:3 notitle with points pointtype 4 ps 1.75 lc rgbcolor \"0xFF0000\", \\" >> tmp.gnuplot
echo "     'tmp-2A.dat' using (\$1*${SCALEX}):2 title 'HYDRA' with linespoints pt 5 ps 1.75 linetype 1 lw ${LW} lc rgbcolor \"0xFF0000\", \\" >> tmp.gnuplot
echo "     'tmp-1S.dat' using (\$1*${SCALEX}):2:3 notitle with points pointtype 6 ps 1.75 lc rgbcolor \"0x0000FF\", \\" >> tmp.gnuplot
echo "     'tmp-1A.dat' using (\$1*${SCALEX}):2 title 'AERIAL' with linespoints pt 7 ps 1.75 linetype 2 lw ${LW} lc rgbcolor \"0x0000FF\", \\" >> tmp.gnuplot
echo "     'tmp-3S.dat' using (\$1*${SCALEX}):2:3 notitle with points pointtype 8 ps 1.75 lc rgbcolor \"0x000000\", \\" >> tmp.gnuplot
echo "     'tmp-3A.dat' using (\$1*${SCALEX}):2 title 'MONPOLY' with linespoints pt 9 ps 1.75 linetype 3 lw ${LW} lc rgbcolor \"0x000000\"" >> tmp.gnuplot

gnuplot -c tmp.gnuplot

rm -f tmp*
