#!/bin/bash

EXPS=("trace" "size" "scaling" "wc")
RANGES=("$(seq 200 200 2000)" "$(seq 2 2 50)" "$(seq 1 1 10)" "$(seq 1 1 11)")
FMLASS=("$1" "$1" "$1" "1")

MODE_TIME="local"
MODE_SPACE="global"

for i in {0..3}
do
    NAME="${EXPS[${i}]}"
    RANGE="${RANGES[${i}]}"
    FMLAS="${FMLASS[${i}]}"
    ./plot_experiment_aggregate.sh "exp_${NAME}" "${RANGE}" "${MODE_TIME}" > data/exp_${NAME}_time_aggregate.dat
    ./plot_experiment_single.sh "exp_${NAME}" "${RANGE}" "${FMLAS}" "${MODE_TIME}" > data/exp_${NAME}_time_single.dat
    ./plot_experiment_aggregate.sh "exp_${NAME}" "${RANGE}" "${MODE_SPACE}" > data/exp_${NAME}_space_aggregate.dat
    ./plot_experiment_single.sh "exp_${NAME}" "${RANGE}" "${FMLAS}" "${MODE_SPACE}" > data/exp_${NAME}_space_single.dat
done
