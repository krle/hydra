#!/bin/bash

PREFIX=$1
RANGE=$2
FMLAS=$3

CELL="-expr"

# Specifies the number of repeated runs with identical inputs
REP=5

# Specifies the timeout for a single run
TIMEOUT="5.0s"

for N in ${RANGE}
do
    for((F=0; F<${FMLAS}; F++))
    do
        FMLA="fmlas/${PREFIX}_${N}_${F}.mtl"
        MFOTL="fmlas/${PREFIX}_${N}_${F}.mfotl"
        SIG="fmlas/mfotl.sig"
        LOG="logs/${PREFIX}_${N}_${F}.log"
        MLOG="logs/${PREFIX}_${N}_${F}.mlog"

        for MODE in "local" "global"
        do
            for((i=0; i<${REP}; i++))
            do
                bash -c "\time -f \"%S %U %M\" bash -c 'timeout ${TIMEOUT} ./../aerial.native ${CELL} -mode ${MODE} -mtl -fmla ${FMLA} -log ${LOG} > /dev/null 2>> error.log'" 2>&1 | egrep -o "[0-9.]+ [0-9.]+ [0-9.]+" | awk '{printf "%f %d,", $1+$2,$3}'
            done >> stats/stat_${PREFIX}_${N}_${F}_${MODE}.aerial
        done

        for((i=0; i<${REP}; i++))
        do
            bash -c "\time -f \"%S %U %M\" bash -c 'timeout ${TIMEOUT} ./../hydra ${FMLA} ${LOG} > /dev/null 2>> error.log'" 2>&1 | egrep -o "[0-9.]+ [0-9.]+ [0-9.]+" | awk '{printf "%f %d,", $1+$2,$3}'
        done >> stats/stat_${PREFIX}_${N}_${F}.hydra
        ./../hydra ${FMLA} ${LOG} | head -n 1 | egrep -o "[0-9]+ pass" | egrep -o "[0-9]+" | awk '{printf "%d,", $1}' >> stats/stat_${PREFIX}_${N}_${F}.pass

        for((i=0; i<${REP}; i++))
        do
            bash -c "\time -f \"%S %U %M\" bash -c 'timeout ${TIMEOUT} ./../monpoly -formula ${MFOTL} -sig ${SIG} -log ${MLOG} -negate > /dev/null 2>> error.log'" 2>&1 | egrep -o "[0-9.]+ [0-9.]+ [0-9.]+" | awk '{printf "%f %d,", $1+$2,$3}'
        done >> stats/stat_${PREFIX}_${N}_${F}.monpoly
    done
done
