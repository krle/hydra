#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <random>

std::mt19937 gen;

int main(int argc, char **argv) {
    if (argc < 5) {
        fprintf(stderr, "gen_log PREFIX ER DELTA SEED\n");
        exit(EXIT_FAILURE);
    }
	int er = atoi(argv[2]);
    int delta = atoi(argv[3]);
    gen.seed(atoi(argv[4]));

    char *log_name = (char *)malloc((strlen(argv[1]) + 5) * sizeof(char));
    strcpy(log_name, argv[1]);
    strcat(log_name, ".log");
	FILE *log=fopen(log_name, "w");

	int ts = 0;
	for (int i = 0; i < 100; i++) {
        for (int j = 0; j < er; j++) {
    		fprintf(log, "@%d", ts);
    		for (int j = 0; j < 16; j++) {
                if (j < 4 && gen() % (er * delta) == 0) continue;
                if (j >= 4 && gen() % 2) continue;
                fprintf(log, " p%d", j);
            }
    		fprintf(log, "\n");
		}
        ts += 1 + gen() % delta;
	}

    free(log_name);
	fclose(log);

	return 0;
}
