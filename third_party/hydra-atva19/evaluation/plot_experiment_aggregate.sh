#!/bin/bash

PREFIX=$1
RANGE=$2
MODE=$3

echo "+------------+-------------------------------------------------------------------------------------------------------+-----------------------------------------+"
echo "| PARAM      | AERIAL                                  | HYDRA                                                       | MONPOLY                                 |"
echo "|            |            Time [s]         Memory [MB] |            Time [s]         Memory [MB]              Passes |            Time [s]         Memory [MB] |"
echo "+------------+-------------------------------------------------------------------------------------------------------+-----------------------------------------|"
for N in ${RANGE}
do
    echo -e "${N}" | awk '{printf "| %10d |", $1}'
    cat stats/stat_${PREFIX}_${N}_*_${MODE}.aerial | python3 ./stat.py 2
    echo -n " |"
    cat stats/stat_${PREFIX}_${N}_*.hydra | python3 ./stat.py 2
    cat stats/stat_${PREFIX}_${N}_*.pass | python3 ./stat.py 1
    echo -n " |"
    cat stats/stat_${PREFIX}_${N}_*.monpoly | python3 ./stat.py 2
    echo " |"
    echo "+------------+-------------------------------------------------------------------------------------------------------+-----------------------------------------|"
done
