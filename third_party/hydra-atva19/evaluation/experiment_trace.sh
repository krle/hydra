#!/bin/bash

FMLAS=$1

PREFIX="exp_trace"
RANGE=$(seq 200 200 2000)
SIZE=25
MAX_INT=16
DELTA=4

for ER in ${RANGE}
do
        for((F=0; F<${FMLAS}; F++))
        do
            NAME=${PREFIX}_${ER}_${F}
            ./gen_fmla ${SIZE} ${MAX_INT} ${F} > fmlas/${NAME}.mtl
            cat fmlas/${NAME}.mtl | sed -E "s/(p[0-9]+)/\\1()/g" | sed -E "s/INFINITY\]/*)/g" > fmlas/${NAME}.mfotl
            ./gen_log logs/${NAME} ${ER} ${DELTA} ${F}
            cat "logs/${NAME}.log" | sed -E "s/([A-Za-z][A-Za-z0-9]*)/\\1()/g" > "logs/${NAME}.mlog"
        done
done

./experiment.sh ${PREFIX} "${RANGE}" ${FMLAS}
