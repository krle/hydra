#!/bin/bash

FMLAS=$1

PREFIX="exp_scaling"
RANGE=$(seq 1 1 10)
SIZE=25
MAX_INT=16

for N in ${RANGE}
do
    cp logs/shared.log logs/shared_${N}.log
    ./scale logs/shared_${N}.log ${N}
    cp logs/shared.mlog logs/shared_${N}.mlog
    ./scale logs/shared_${N}.mlog ${N}
    for((F=0; F<${FMLAS}; F++))
    do
        NAME=${PREFIX}_${N}_${F}
        ./gen_fmla ${SIZE} ${MAX_INT} ${F} > fmlas/${NAME}.mtl
        ./scale fmlas/${NAME}.mtl ${N}
        cat fmlas/${NAME}.mtl | sed -E "s/(p[0-9]+)/\\1()/g" | sed -E "s/INFINITY\]/*)/g" > fmlas/${NAME}.mfotl
        ln -s shared_${N}.log logs/${NAME}.log
        ln -s shared_${N}.mlog logs/${NAME}.mlog
    done
done

./experiment.sh ${PREFIX} "${RANGE}" ${FMLAS}
