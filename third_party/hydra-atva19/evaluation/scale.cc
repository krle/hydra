/* This tool scales all numbers in a file which are not immediately preceded by 'p' by a specified factor. */

#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>

FILE *f;
char *buf;

int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "scale FILE FACTOR\n");
        return EXIT_FAILURE;
    }
    size_t scale = atoi(argv[2]);

    f = fopen(argv[1], "rb");
    fseek(f, 0, SEEK_END);
    size_t sz = ftell(f);
    buf = (char *)malloc(sz);
    rewind(f);
    assert(fread(buf, 1, sz, f) == sz);
    fclose(f);

    f = fopen(argv[1], "w");
    size_t cur = 0;
    int pred = 0;
    for (size_t i = 0; i < sz; ) {
        if (!('0' <= buf[i] && buf[i] <= '9') || pred) {
            if (buf[i] == 'p') pred = 1;
            else if (!('0' <= buf[i] && buf[i] <= '9')) pred = 0;
            fprintf(f, "%c", buf[i++]);
            continue;
        } else {
            cur = 0;
            while (i < sz && ('0' <= buf[i] && buf[i] <= '9')) {
                cur = 10 * cur + (buf[i++] - '0');
            }
            fprintf(f, "%lu", scale * cur);
        }
    }
    fclose(f);
    free(buf);

    return 0;
}
