#!/bin/bash 

EXPS=("trace" "size" "scaling" "wc")
CASES=("Average-Case" "Average-Case" "Average-Case" "Worst-Case")
XS=("Trace Length [x1000]" "Formula Size" "Scaling Factor" "Formula Size Parameter (n)")
XRANGE=("[0:220]" "[0:52]" "[0:11]" "[0:12]")
SCALEX=("0.1" "1" "1" "1")

# Specifies the axes ranges
YRANGETIME=("[0:2.5]" "[0:2.5]" "[0:2.5]" "[0:2.5]")
YRANGESPACE=("[0:30]" "[0:30]" "[0:30]" "[0:20]")

for i in {0..3}
do
    NAME=${EXPS[${i}]}
    ./plot.sh data/exp_${NAME}_time_aggregate.dat data/exp_${NAME}_time_single.dat figs/exp_${NAME}_time.eps 0 "${CASES[${i}]} Time Complexity" "${XS[${i}]}" "${XRANGE[${i}]}" "Time [s]" "${YRANGETIME[${i}]}" "18" "outside" "3" "${SCALEX[${i}]}"
    ./plot.sh data/exp_${NAME}_space_aggregate.dat data/exp_${NAME}_space_single.dat figs/exp_${NAME}_space.eps 1 "${CASES[${i}]} Space Complexity" "${XS[${i}]}" "${XRANGE[${i}]}" "Space [MB]" "${YRANGESPACE[${i}]}" "18" "outside" "3" "${SCALEX[${i}]}"
done
