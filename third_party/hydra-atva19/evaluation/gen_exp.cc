#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void gen_log(FILE *log, int n) {
    fprintf(log, "@0\n");
	for(int m = 0; m < (1 << n); m++) {
		fprintf(log, "@1");
		for(int i = 0, k = m; k; i++, k >>= 1) {
			if(k&1) {
				fprintf(log, " p%d", i);
			}
		}
		fprintf(log, "\n");
	}
	fprintf(log, "@1 e p0\n");
    fprintf(log, "@2\n");
}

int main(int argc, char **argv) {
	if (argc < 3) {
        fprintf(stderr, "gen_exp PREFIX N\n");
        exit(EXIT_FAILURE);
	}

    char *fmla_name = (char *)malloc((strlen(argv[1]) + 13) * sizeof(char));
    strcpy(fmla_name, "fmlas/");
    strcat(fmla_name, argv[1]);
    strcat(fmla_name, "_0.mtl");
    char *mfotl_name = (char *)malloc((strlen(argv[1]) + 15) * sizeof(char));
    strcpy(mfotl_name, "fmlas/");
    strcat(mfotl_name, argv[1]);
    strcat(mfotl_name, "_0.mfotl");
    char *log_name = (char *)malloc((strlen(argv[1]) + 12) * sizeof(char));
    strcpy(log_name, "logs/");
    strcat(log_name, argv[1]);
    strcat(log_name, "_0.log");

	FILE *fmla = fopen(fmla_name, "w");
	FILE *mfotl = fopen(mfotl_name, "w");
	FILE *log = fopen(log_name, "w");

    int n = atoi(argv[2]);

    fprintf(fmla, "NEXT[1,1] (");
	fprintf(fmla, "(NOT e) UNTIL[0,0] (NOT e");
	for(int k = 0; k < n; k++) {
		fprintf(fmla, " AND (p%d => (ALWAYS[0,0] (e => p%d)))", k, k);
		fprintf(fmla, " AND (NOT p%d => (ALWAYS[0,0] (e => NOT p%d)))", k, k);
	}
	fprintf(fmla, "))\n");

    fprintf(mfotl, "NEXT[1,1] (");
	fprintf(mfotl, "(NOT e()) UNTIL[0,0] (NOT e()");
	for(int k = 0; k < n; k++) {
		fprintf(mfotl, " AND (p%d() IMPLIES (ALWAYS[0,0] (e() IMPLIES p%d())))", k, k);
		fprintf(mfotl, " AND (NOT p%d() IMPLIES (ALWAYS[0,0] (e() IMPLIES NOT p%d())))", k, k);
	}
	fprintf(mfotl, "))\n");

	gen_log(log, n);

    free(fmla_name);
    free(mfotl_name);
    free(log_name);

	fclose(fmla);
    fclose(mfotl);
	fclose(log);

	return 0;
}
