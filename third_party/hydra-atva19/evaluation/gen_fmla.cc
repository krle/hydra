#include <algorithm>
#include <random>

#include "constants.h"
#include "formula.h"

std::mt19937 gen;

std::pair<timestamp,timestamp> gen_int(timestamp maxr, bool inf) {
    if (maxr == 0) return std::make_pair(0, 0);
    int r = gen() % 4;
    if (r == 0) {
        return std::make_pair(0, 0);
    } else if (r == 1) {
        timestamp to = 1 + (gen() % (maxr + inf));
        if (to == maxr + 1) to = MAX_TIMESTAMP;
        return std::make_pair(0, to);
    } else {
        timestamp from = 1 + gen() % maxr;
        timestamp to = from + (gen() % (maxr - from + 1 + inf));
        if (to == maxr + 1) to = MAX_TIMESTAMP;
        return std::make_pair(from, to);
    }
}

Formula *gen_fmla(int size, int maxr) {
    assert(size > 0);
    if (size == 1) {
        return new PredFormula(gen() % 16);
    } else if (size == 2) {
        int op = gen() % 3;
        Formula *subf = gen_fmla(size - 1, maxr);
        std::pair<int,int> in = gen_int(maxr, op == 1);
        if (op == 0) return new NegFormula(subf);
        else if (op == 1) return new PrevFormula(subf, in.first, in.second);
        else return new NextFormula(subf, in.first, in.second);
    } else {
        int op = gen() % 2 ? 5 : gen() % 5;
        int sz = 1 + gen() % (size - 2);
        std::pair<int,int> in = gen_int(maxr, op == 2 || op == 4);
        if (op == 0) {
            Formula *subf = gen_fmla(size - 1, maxr);
            return new NegFormula(subf);
        } else if (op == 1) {
            Formula *subf1 = gen_fmla(sz, maxr);
            Formula *subf2 = gen_fmla(size - 1 - sz, maxr);
            return new OrFormula(subf1, subf2);
        } else if (op == 2) {
            Formula *subf = gen_fmla(size - 1, maxr);
            return new PrevFormula(subf, in.first, in.second);
        } else if (op == 3) {
            Formula *subf = gen_fmla(size - 1, maxr);
            return new NextFormula(subf, in.first, in.second);
        } else if (op == 4) {
            Formula *subf1 = gen_fmla(sz, maxr);
            Formula *subf2 = gen_fmla(size - 1 - sz, maxr);
            return new SinceFormula(subf1, subf2, in.first, in.second);
        } else {
            Formula *subf1 = gen_fmla(sz, maxr);
            Formula *subf2 = gen_fmla(size - 1 - sz, maxr);
            return new UntilFormula(subf1, subf2, in.first, in.second);
        }
    }
}

int main(int argc, char **argv) {
    if (argc < 4) {
        fprintf(stderr, "gen_fmla SIZE MAXR SEED\n");
        exit(EXIT_FAILURE);
    }

    int size = atoi(argv[1]);
    int maxr = atoi(argv[2]);
    int seed = atoi(argv[3]);

    gen.seed(seed);

    Formula *fmla = gen_fmla(size, maxr);

    fmla->print();
    printf("\n");

    delete fmla;

    return 0;
}
