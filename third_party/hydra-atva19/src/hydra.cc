#include <cstdio>
#include <cstdlib>
#include <exception>
#include <stdexcept>

#include "constants.h"
#include "formula.h"
#include "parser.h"
#include "lexer.h"

#include "monitor.h"
#include "trie.h"

Formula *getAST(const char *formula)
{
    Formula *fmla;
    yyscan_t scanner;
    YY_BUFFER_STATE state;
 
    if (yylex_init(&scanner)) {
        return NULL;
    }
 
    state = yy_scan_string(formula, scanner);
 
    if (yyparse(&fmla, scanner)) {
        return NULL;
    }
 
    yy_delete_buffer(state, scanner);
    yylex_destroy(scanner);
 
    return fmla;
}

void printUsage()
{
    fprintf(stderr, "hydra MTL LOG\n");
}

void printFmla(const Formula *fmla)
{
    printf("Monitoring ");
    fmla->print();
    printf(" with %d pass(es).\n", num_passes);
}

struct Timepoint {
    timestamp ts;
    int off;

    Timepoint(timestamp ts, int off) : ts(ts), off(off) {}
};
 
int main(int argc, char **argv)
{
    if (argc != 3) {
        printUsage();
        return EXIT_FAILURE;
    }

    FILE *mtl = fopen(argv[1], "r");
    if (mtl == NULL) {
        printUsage();
        return EXIT_FAILURE;
    }

    char *line = NULL;
    size_t length = 0;
    if (getline(&line, &length, mtl) == -1) {
        fprintf(stderr, "Error: formula\n");
        fclose(mtl);
        free(line);
        return EXIT_FAILURE;
    }
    fclose(mtl);
    Formula *fmla;
    try {
        fmla = getAST(line);
    } catch(const std::runtime_error &e) {
        fprintf(stderr, "Error: %s\n", e.what());
        free(line);
        if (fmla != NULL) delete fmla;
        return EXIT_FAILURE;
    }
    free(line);

    ap_lookup = new int[trie.cnt];
    for (int i = 0; i < trie.cnt; i++) ap_lookup[i] = 0;
    ap_list = new int[trie.cnt];

    Monitor *mon = createMonitor(fmla, argv[2]);
    printFmla(fmla);
    std::optional<Timepoint> tp;
    do {
        try {
            BooleanVerdict v = mon->step();
            if (tp) {
                if (tp->ts == v.ts) tp->off++;
                else tp = Timepoint(v.ts, 0);
            } else {
                tp = Timepoint(v.ts, 0);
            }
            if (v.b != UNRESOLVED) printf("%d:%d %s\n", tp->ts, tp->off, v.b == TRUE ? "true" : "false");
        } catch (const EOL &e) {
            printf("Bye.\n");
            break;
        }
    } while(true);

    delete fmla;
    delete [] ap_lookup;
    delete [] ap_list;
    delete mon;

    return 0;
}
