%{

#include "constants.h"
#include "formula.h"
#include "parser.h"
#include "util.h"

#include <exception>
#include <stdexcept>

%}

%option outfile="lexer.cc" header-file="lexer.h"
%option warn nodefault

%option reentrant noyywrap never-interactive nounistd
%option bison-bridge

LPAREN      \(
RPAREN      \)
 
INTLPAREN   \[
INTRPAREN   \]

NUMBER      [0-9]*
ALPHA       [a-zA-Z$]
ALPHANUMS   [a-zA-Z$0-9_'"]*
WS          [ \r\n\t]*
 
%%
 
{WS}                { continue; }
INFINITY            { return TOKEN_INFINITY; }
FALSE               { return TOKEN_FALSE; }
TRUE                { return TOKEN_TRUE; }
!|NOT               { return TOKEN_NEG; }
&|AND               { return TOKEN_CONJ; }
\||OR               { return TOKEN_DISJ; }
=\>|-\>             { return TOKEN_IMP; }
\<=\>|\<-\>         { return TOKEN_IFF; }
SINCE               { return TOKEN_SINCE; }
UNTIL               { return TOKEN_UNTIL; }
NEXT                { return TOKEN_NEXT; }
PREV                { return TOKEN_PREV; }
EVENTUALLY          { return TOKEN_EVENTUALLY; }
ONCE                { return TOKEN_ONCE; }
ALWAYS              { return TOKEN_ALWAYS; }
HISTORICALLY        { return TOKEN_HISTORICALLY; }
{LPAREN}            { return TOKEN_LPAREN; }
{RPAREN}            { return TOKEN_RPAREN; }

{ALPHA}{ALPHANUMS}  { if (strlen(yytext) > MAX_PRED_LEN) throw std::runtime_error("predicate name"); strcpy(yylval->name, yytext); return TOKEN_PRED; }
{NUMBER}            { if (parseNumber(yytext, NULL, &yylval->value)) throw std::runtime_error("interval bounds"); return TOKEN_NUMBER; }
{INTLPAREN}         { return TOKEN_INTLPAREN; }
,                   { return TOKEN_SEP; }
{INTRPAREN}         { return TOKEN_INTRPAREN; }

.                   { throw std::runtime_error("unexpected character"); }
 
%%
