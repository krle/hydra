#ifndef __TRIE_H__
#define __TRIE_H__

#include <cstdlib>

struct TrieNode {
    int value;
    TrieNode *next[128];

    TrieNode() {
        value = -1;
        for (int i = 0; i < 128; i++) next[i] = NULL;
    }
    ~TrieNode() {
        for (int i = 0; i < 128; i++) {
            if (next[i] != NULL) {
                delete next[i];
            }
        }
    }
};

struct Trie {
    TrieNode root;
    int cnt = 0;

    int getOrAdd(const char *s);
};

extern Trie trie;

#endif /* __TRIE_H__ */
