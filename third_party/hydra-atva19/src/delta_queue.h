#ifndef __DELTA_QUEUE_H__
#define __DELTA_QUEUE_H__

#include "circular_buffer.h"
#include "constants.h"

#include <cassert>
#include <cstdint>

class DeltaQueue {
    timestamp_delta sum = 0;
    circular_buffer<timestamp_delta> q;

    bool inv() {
        assert(q.empty() || q.peek() == 0);
    }
    void align() {
        while (!q.empty() && q.peek() != 0) sum -= q.pop_one();
    }

public:
    DeltaQueue(size_t n) : q(n) {}
    bool empty() const {
        return q.empty();
    }
    bool check(timestamp_delta from) const {
        if (empty()) return false;
        else return sum >= from;
    }
    timestamp_delta get_sum() const {
        return sum;
    }
    void add_delta(timestamp_delta delta) {
        if (empty() || delta == 0) return;
        q.push(delta);
        sum += delta;
    }
    void push() {
        q.push(0);
    }
    timestamp_delta pop_one() {
        assert(!empty());
        size_t prev_sum = sum;
        q.pop_one();
        align();
        return prev_sum;
    }
    void pop_until(timestamp_delta bound, bool strict, circular_buffer<Bunch<timestamp_delta> > *tmp = NULL) {
        while (!empty() && (sum > bound || (sum == bound && !strict))) {
            Bunch<timestamp_delta> bunch = q.pop_bunch();
            bunch.t = sum;
            if (tmp != NULL) tmp->push(bunch);
            align();
        }
    }
    void clear() {
        sum = 0;
        q.clear();
    }
};

#endif /* __DELTA_QUEUE_H__ */
