#include "monitor.h"

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <stdexcept>

#include "circular_buffer.h"
#include "delta_queue.h"
#include "formula.h"
#include "trie.h"
#include "util.h"

#define min(x, y) ((x) < (y) ? (x) : (y))
#define contains(x, f, t) ((f) <= (x) && (x) <= (t))

int *ap_lookup;
int *ap_list;

static int fsm(const char *line, int *pos) {
    TrieNode *t = &trie.root;
    int i = *pos;
    while (line[i] && line[i] != ' ' && line[i] != '\r' && line[i] != '\n') {
        if (line[i] & 0x80) throw std::runtime_error("log file format");
        if (t->next[line[i]] == NULL) {
            *pos = i;
            return -1;
        }
        t = t->next[line[i++]];
    }
    *pos = i;
    return t->value;
}

BooleanVerdict NonTempMonitor::step_impl() {
    static char line[256];
    int pos = 0;
    int ts;
    int ap_cnt = 0;

    if (fgets(line, 256, f) == NULL) {
        throw EOL();
    }
    if (line[pos++] != '@') throw std::runtime_error("log file format");
    if (parseNumber(line, &pos, &ts) || (t && ts < *t)) throw std::runtime_error("timestamp");
    t = ts;
    do {
        while (line[pos] && line[pos] != '\r' && line[pos] != '\n') {
            if (line[pos] == ' ') {
                pos++;
                continue;
            }
            int value = fsm(line, &pos);
            if (value == -1) {
                while (line[pos] && line[pos] != ' ' && line[pos] != '\r' && line[pos] != '\n') pos++;
            } else {
                if (!ap_lookup[value]) ap_list[ap_cnt++] = value;
                ap_lookup[value] = 1;
            }
        }
        if (!line[pos]) {
            if (fgets(line, 256, f) == NULL) {
                break;
            }
            pos = 0;
        } else {
            break;
        }
    } while(line[pos] != '\r' && line[pos] != '\n');

    bool b = fmla->eval(ap_lookup);
    for (int i = 0; i < ap_cnt; i++) ap_lookup[ap_list[i]] = 0;

    return BooleanVerdict(ts, b ? TRUE : FALSE);
}

BooleanVerdict PrevMonitor::step_impl() {
    BooleanVerdict subv = subf->step();

    if (v) {
        BooleanVerdict last_subv = *v;
        v = subv;
        if (contains(subv.ts - last_subv.ts, from, to)) return BooleanVerdict(subv.ts, last_subv.b);
        else return BooleanVerdict(subv.ts, FALSE);
    } else {
        v = subv;
        return BooleanVerdict(subv.ts, FALSE);
    }
}

BooleanVerdict NextMonitor::step_impl() {
    do {
        BooleanVerdict subv = subf->step();

        if (t) {
            timestamp last_ts = *t;
            t = subv.ts;
            if (contains(subv.ts - last_ts, from, to)) return BooleanVerdict(last_ts, subv.b);
            else return BooleanVerdict(last_ts, FALSE);
        } else {
            t = subv.ts;
        }
    } while(true);
}
BooleanVerdict NextMonitor::handle_eof() {
    eof = true;
    if (t) {
        timestamp ts = *t;
        t = {};
        return BooleanVerdict(ts, UNRESOLVED);
    } else {
        throw EOL();
    }
}

BooleanVerdict SinceMonitor::step_impl() {
    BooleanVerdict subfv = subf->step();
    BooleanVerdict subgv = subg->step();
    assert(subfv.ts == subgv.ts);

    if (v[0]) {
        timestamp_delta delta = subfv.ts - v[0]->ts;
        dq[0]->add_delta(delta);
        dq[1]->add_delta(delta);
    }
    if (!v[0]) v[0] = BoolVerdict(subfv.ts, false);
    else v[0]->ts = subfv.ts;
    if (!v[1]) v[1] = BoolVerdict(subfv.ts, false);
    else v[1]->ts = subfv.ts;

    if (to < MAX_TIMESTAMP) dq[0]->pop_until(to, true);
    if (to < MAX_TIMESTAMP) dq[1]->pop_until(to, true);

    if (subfv.b == FALSE) {
        dq[0]->clear();
        v[0]->b = false;
        dq[1]->clear();
        v[1]->b = false;
    } else if (subfv.b == UNRESOLVED) {
        dq[0]->clear();
        v[0]->b = false;
    }
    if (subgv.b == TRUE) {
        dq[0]->push();
        dq[1]->push();
    } else if (subgv.b == UNRESOLVED) {
        dq[1]->push();
    }

    if (to < MAX_TIMESTAMP) {
        v[0]->b = dq[0]->check(from);
        v[1]->b = dq[1]->check(from);
    } else {
        if (dq[0]->check(from)) v[0]->b = true;
        dq[0]->pop_until(from, false);
        if (dq[1]->check(from)) v[1]->b = true;
        dq[1]->pop_until(from, false);
    }

    return BooleanVerdict(subfv.ts, v[0]->b != v[1]->b ? UNRESOLVED : v[0]->b ? TRUE : FALSE);
}

BooleanVerdict UntilMonitor::step_impl() {
    do {
        std::optional<BooleanVerdict> v = pull_verdict();
        if (v) {
            return *v;
        }

        BooleanVerdict subfv = subf->step();
        BooleanVerdict subgv = subg->step();
        assert(subfv.ts == subgv.ts);

        if (t) {
            timestamp_delta delta = subfv.ts - *t;
            dq[0]->add_delta(delta);
            dq[1]->add_delta(delta);
        }
        t = subfv.ts;
    
        resolve_until(0, to, true, false);
        resolve_until(1, to, true, false);
        dq[0]->push();
        dq[1]->push();
    
        if (subgv.b == TRUE) {
            resolve_until(0, from, false, true);
            resolve_until(1, from, false, true);
        } else if (subgv.b == UNRESOLVED) {
            resolve_until(1, from, false, true);
        }
        if (subfv.b == FALSE) {
            resolve_all(0, false);
            resolve_all(1, false);
        } else if (subfv.b == UNRESOLVED) {
            resolve_all(0, false);
        }
    } while(true);
}
BooleanVerdict UntilMonitor::handle_eof() {
    assert(q[0]->empty() || q[1]->empty());
    eof = true;
    if (q[0]->empty()) {
        if (!dq[0]->empty()) {
            assert(t != std::nullopt);
            timestamp ts = *t - dq[0]->pop_one();
            return BooleanVerdict(ts, UNRESOLVED);
        } else {
            throw EOL();
        }
    } else {
        if (!dq[1]->empty()) {
            assert(t != std::nullopt);
            timestamp ts = *t - dq[1]->pop_one();
            return BooleanVerdict(ts, UNRESOLVED);
        } else {
            throw EOL();
        }
    }
}
void UntilMonitor::resolve_until(int id, timestamp bound, bool strict, bool b) {
    assert(t != std::nullopt);
    tmp->clear();
    dq[id]->pop_until(bound, strict, tmp);
    while (!tmp->empty()) {
        Bunch<timestamp_delta> bunch_ts_delta = tmp->pop_one();
        Bunch<BoolVerdict> bunch_bv(BoolVerdict(*t - bunch_ts_delta.t, b), bunch_ts_delta.cnt);
        q[id]->push(bunch_bv);
    }
}
std::optional<BooleanVerdict> UntilMonitor::pull_verdict() {
    if (!q[0]->empty() && !q[1]->empty()) {
        BoolVerdict bv0 = q[0]->pop_one();
        BoolVerdict bv1 = q[1]->pop_one();
        assert(bv0.ts == bv1.ts);
        timestamp ts = bv0.ts;
        Boolean b = bv0.b == bv1.b ? (bv0.b ? TRUE : FALSE) : UNRESOLVED;
        return BooleanVerdict(ts, b);
    } else {
        return {};
    }
}
