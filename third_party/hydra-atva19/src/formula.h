#ifndef __FORMULA_H__
#define __FORMULA_H__

#include "constants.h"

#include <cassert>
#include <cstdlib>

struct Monitor;

extern int num_passes;

struct Formula {
    virtual ~Formula() {}
    virtual Monitor *createTempMonitor(const char *fname) const = 0;
    virtual bool eval(const int *ap_lookup) const {
        assert(0);
    }
    virtual void print() const = 0;
};

struct BoolFormula : Formula {
    bool b;

    BoolFormula(bool b) : b(b) {}
    Monitor *createTempMonitor(const char *fname) const override;
    bool eval(const int *ap_lookup) const override;
    void print() const override;
};

struct PredFormula : Formula {
    int pred;

    PredFormula(int pred) : pred(pred) {}
    Monitor *createTempMonitor(const char *fname) const override;
    bool eval(const int *ap_lookup) const override;
    void print() const override;
};

struct NegFormula : Formula {
    Formula *f;

    NegFormula(Formula *f) : f(f) {}
    ~NegFormula() override {
        if (f != NULL) delete f;
    }
    Monitor *createTempMonitor(const char *fname) const override;
    bool eval(const int *ap_lookup) const override;
    void print() const override;
};

struct AndFormula : Formula {
    Formula *f, *g;

    AndFormula(Formula *f, Formula *g) : f(f), g(g) {}
    ~AndFormula() override {
        if (f != NULL) delete f;
        if (g != NULL) delete g;
    }
    Monitor *createTempMonitor(const char *fname) const override;
    bool eval(const int *ap_lookup) const override;
    void print() const override;
};

struct OrFormula : Formula {
    Formula *f, *g;

    OrFormula(Formula *f, Formula *g) : f(f), g(g) {}
    ~OrFormula() override {
        if (f != NULL) delete f;
        if (g != NULL) delete g;
    }
    Monitor *createTempMonitor(const char *fname) const override;
    bool eval(const int *ap_lookup) const override;
    void print() const override;
};

struct PrevFormula : Formula {
    timestamp from, to;
    Formula *f;

    PrevFormula(Formula *f, timestamp from, timestamp to) : from(from), to(to), f(f) {}
    ~PrevFormula() override {
        if (f != NULL) delete f;
    }
    Monitor *createTempMonitor(const char *fname) const override;
    void print() const override;
};

struct NextFormula : Formula {
    timestamp from, to;
    Formula *f;

    NextFormula(Formula *f, timestamp from, timestamp to) : from(from), to(to), f(f) {}
    ~NextFormula() override {
        if (f != NULL) delete f;
    }
    Monitor *createTempMonitor(const char *fname) const override;
    void print() const override;
};

struct SinceFormula : Formula {
    timestamp from, to;
    Formula *f, *g;

    SinceFormula(Formula *f, Formula *g, timestamp from, timestamp to) : from(from), to(to), f(f), g(g) {}
    ~SinceFormula() override {
        if (f != NULL) delete f;
        if (g != NULL) delete g;
    }
    Monitor *createTempMonitor(const char *fname) const override;
    void print() const override;
};

struct UntilFormula : Formula {
    timestamp from, to;
    Formula *f, *g;

    UntilFormula(Formula *f, Formula *g, timestamp from, timestamp to) : from(from), to(to), f(f), g(g) {}
    ~UntilFormula() override {
        if (f != NULL) delete f;
        if (g != NULL) delete g;
    }
    Monitor *createTempMonitor(const char *fname) const override;
    void print() const override;
};

Monitor *createMonitor(Formula *fmla, const char *fname);

#endif /* __FORMULA_H__ */
