#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#include <cstdint>

const int MAX_PRED_LEN = 40;

typedef uint32_t timestamp;
typedef uint32_t timestamp_delta;
const timestamp MAX_TIMESTAMP = 0xFFFFFFFF;

enum Boolean {
    FALSE, TRUE, UNRESOLVED
};

#endif /* __CONSTANTS_H__ */
