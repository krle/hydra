#include "formula.h"

#include <cstdio>

#include "monitor.h"

int num_passes = 0;

Monitor *BoolFormula::createTempMonitor(const char *fname) const {
    return NULL;
}
bool BoolFormula::eval(const int *ap_lookup) const {
    return b;
}
void BoolFormula::print() const {
    printf("%s", b ? "TRUE" : "FALSE");
}

Monitor *PredFormula::createTempMonitor(const char *fname) const {
    return NULL;
}
bool PredFormula::eval(const int *ap_lookup) const {
    return ap_lookup[pred];
}
void PredFormula::print() const {
    printf("p%d", pred);
}

Monitor *NegFormula::createTempMonitor(const char *fname) const {
    Monitor *sub = f->createTempMonitor(fname);
    if (sub == NULL) return NULL;
    else return new NegMonitor(sub);
}
bool NegFormula::eval(const int *ap_lookup) const {
    return !f->eval(ap_lookup);
}
void NegFormula::print() const {
    printf("NOT ");
    f->print();
}

Monitor *AndFormula::createTempMonitor(const char *fname) const {
    Monitor *subf = f->createTempMonitor(fname);
    Monitor *subg = g->createTempMonitor(fname);
    if (subf == NULL && subg == NULL) return NULL;
    else {
        if (subf == NULL) {
            subf = new NonTempMonitor(f, fname);
            num_passes++;
        }
        if (subg == NULL) {
            subg = new NonTempMonitor(g, fname);
            num_passes++;
        }
        return new AndMonitor(subf, subg);
    }
}
bool AndFormula::eval(const int *ap_lookup) const {
    return f->eval(ap_lookup) && g->eval(ap_lookup);
}
void AndFormula::print() const {
    printf("(");
    f->print();
    printf(" AND ");
    g->print();
    printf(")");
}

Monitor *OrFormula::createTempMonitor(const char *fname) const {
    Monitor *subf = f->createTempMonitor(fname);
    Monitor *subg = g->createTempMonitor(fname);
    if (subf == NULL && subg == NULL) return NULL;
    else {
        if (subf == NULL) {
            subf = new NonTempMonitor(f, fname);
            num_passes++;
        }
        if (subg == NULL) {
            subg = new NonTempMonitor(g, fname);
            num_passes++;
        }
        return new OrMonitor(subf, subg);
    }
}
bool OrFormula::eval(const int *ap_lookup) const {
    return f->eval(ap_lookup) || g->eval(ap_lookup);
}
void OrFormula::print() const {
    printf("(");
    f->print();
    printf(" OR ");
    g->print();
    printf(")");
}

Monitor *PrevFormula::createTempMonitor(const char *fname) const {
    Monitor *subf = createMonitor(f, fname);
    return new PrevMonitor(subf, from, to);
}
void PrevFormula::print() const {
    if (to < MAX_TIMESTAMP) printf("PREV[%d,%d] ", from, to);
    else printf("PREV[%d,%s] ", from, "INFINITY");
    f->print();
}

Monitor *NextFormula::createTempMonitor(const char *fname) const {
    Monitor *subf = createMonitor(f, fname);
    return new NextMonitor(subf, from, to);
}
void NextFormula::print() const {
    printf("NEXT[%d,%d] ", from, to);
    f->print();
}

Monitor *SinceFormula::createTempMonitor(const char *fname) const {
    Monitor *subf = createMonitor(f, fname);
    Monitor *subg = createMonitor(g, fname);
    return new SinceMonitor(subf, subg, from, to);
}
void SinceFormula::print() const {
    printf("(");
    f->print();
    if (to < MAX_TIMESTAMP) printf(" SINCE[%d,%d] ", from, to);
    else printf(" SINCE[%d,%s] ", from, "INFINITY");
    g->print();
    printf(")");
}

Monitor *UntilFormula::createTempMonitor(const char *fname) const {
    Monitor *subf = createMonitor(f, fname);
    Monitor *subg = createMonitor(g, fname);
    return new UntilMonitor(subf, subg, from, to);
}
void UntilFormula::print() const {
    printf("(");
    f->print();
    printf(" UNTIL[%d,%d] ", from, to);
    g->print();
    printf(")");
}

Monitor *createMonitor(Formula *fmla, const char *fname) {
    Monitor *mon = fmla->createTempMonitor(fname);
    if (mon == NULL) {
        mon = new NonTempMonitor(fmla, fname);
        num_passes++;
    }
    return mon;
}
