#ifndef __CIRCULAR_BUFFER_H__
#define __CIRCULAR_BUFFER_H__

#include <cassert>
#include <cstdint>
#include <optional>

template <typename T>
struct Bunch {
    T t;
    size_t cnt;

    Bunch(T t, size_t cnt = 1) : t(t), cnt(cnt) {}
    bool operator==(const Bunch<T> &bunch) const {
        return t == bunch.t && cnt == bunch.cnt;
    }
};

template<typename T>
class circular_buffer {
    const size_t sz;
    std::optional<Bunch<T> > *buf;
    size_t beg, end;

public:
    circular_buffer(size_t sz) : sz(sz + 1), buf(new std::optional<Bunch<T> >[sz + 1]), beg(0), end(0) {}
    ~circular_buffer() {
        if (buf != NULL) delete [] buf;
    }
    bool empty() const {
        return beg == end;
    }
    void clear() {
        beg = end = 0;
    }
    void push(Bunch<T> bunch) {
        size_t prev_end = end == 0 ? sz - 1 : end - 1;
        if (!empty() && buf[prev_end]->t == bunch.t) {
            buf[prev_end]->cnt += bunch.cnt;
            return;
        }
        buf[end++] = bunch;
        if (end == sz) end = 0;
        assert(!empty());
    }
    T peek() {
        assert(!empty());
        return buf[beg]->t;
    }
    T pop_one() {
        assert(!empty());
        T t = buf[beg]->t;
        buf[beg]->cnt--;
        if (buf[beg]->cnt == 0) {
            if (++beg == sz) beg = 0;
        }
        return t;
    }
    Bunch<T> pop_bunch() {
        assert(!empty());
        Bunch<T> bunch = *buf[beg++];
        if (beg == sz) beg = 0;
        return bunch;
    }
};

#endif /* __CIRCULAR_BUFFER_H__ */
