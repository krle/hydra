#include "trie.h"

#include <cstdlib>

Trie trie;

int Trie::getOrAdd(const char *s)
{
    TrieNode *cur = &root;

    while (*s != 0) {
        if (cur->next[*s] == NULL) {
            cur->next[*s] = new TrieNode();
        }
        cur = cur->next[*s++];
    }

    if (cur->value == -1) {
        cur->value = cnt++;
    }

    return cur->value;
}
