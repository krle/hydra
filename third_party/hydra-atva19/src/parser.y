%{

#include "constants.h"
#include "formula.h"
#include "parser.h"
#include "lexer.h"
#include "trie.h"

#include <exception>
#include <stdexcept>

int yyerror(Formula **fmla, yyscan_t scanner, const char *msg) {
    throw std::runtime_error("parsing");
}

%}

%code requires {
    typedef void *yyscan_t;
}

%output  "parser.cc"
%defines "parser.h"
 
%define api.pure
%lex-param   { yyscan_t scanner }
%parse-param { Formula **fmla }
%parse-param { yyscan_t scanner }

%union {
    char name[MAX_PRED_LEN];
    int value;
    Formula *fmla;
}

%token TOKEN_FALSE "false"
%token TOKEN_TRUE "true"
%token TOKEN_NEG "NOT"
%token TOKEN_CONJ "AND"
%token TOKEN_DISJ "OR"
%token TOKEN_IMP "IMP"
%token TOKEN_IFF "IFF"
%token TOKEN_SINCE "SINCE"
%token TOKEN_UNTIL "UNTIL"
%token TOKEN_NEXT "NEXT"
%token TOKEN_PREV "PREV"
%token TOKEN_EVENTUALLY "EVENTUALLY"
%token TOKEN_ONCE "ONCE"
%token TOKEN_ALWAYS "ALWAYS"
%token TOKEN_HISTORICALLY "HISTORICALLY"
%token TOKEN_LPAREN "("
%token TOKEN_RPAREN ")"

%token <name> TOKEN_PRED "pred"
%token <value> TOKEN_NUMBER "num"
%token TOKEN_INFINITY "inf"
%token TOKEN_INTLPAREN "["
%token TOKEN_SEP ","
%token TOKEN_INTRPAREN "]"

%type <value> number
%type <value> infnumber
%type <fmla> formula

%left "SINCE" "UNTIL" "PREV" "NEXT" "EVENTUALLY" "ONCE" "ALWAYS" "HISTORICALLY"
%left "IMP" "IFF"
%left "OR"
%left "AND"
%left "NOT"

%%
 
input
    : formula { *fmla = $1; }
    ;

number
    : "num" { $$ = $1; }
    ;

infnumber
    : "num" { $$ = $1; }
    | "inf" { $$ = MAX_TIMESTAMP; }
    ;

formula
    : "false" { $$ = new BoolFormula(false); }
    | "true" { $$ = new BoolFormula(true); }
    | "pred" { $$ = new PredFormula(trie.getOrAdd($1)); }
    | "NOT" formula[f] { $$ = new NegFormula($f); }
    | formula[f] "AND" formula[g] { $$ = new AndFormula($f, $g); }
    | formula[f] "OR" formula[g] { $$ = new OrFormula($f, $g); }
    | formula[f] "IMP" formula[g] { $$ = new OrFormula(new NegFormula($f), $g); }
    | formula[f] "IFF" formula[g] { $$ = new OrFormula(new AndFormula($f, $g), new AndFormula(new NegFormula($f), new NegFormula($g))); }
    | formula[f] "SINCE" "[" number[from] "," infnumber[to] "]" formula[g] { $$ = new SinceFormula($f, $g, $from, $to); }
    | formula[f] "UNTIL" "[" number[from] "," number[to] "]" formula[g] { $$ = new UntilFormula($f, $g, $from, $to); }
    | "NEXT" "[" number[from] "," number[to] "]" formula[f] { $$ = new NextFormula($f, $from, $to); }
    | "PREV" "[" number[from] "," infnumber[to] "]" formula[f] { $$ = new PrevFormula($f, $from, $to); }
    | "EVENTUALLY" "[" number[from] "," number[to] "]" formula[f] { $$ = new UntilFormula(new BoolFormula(true), $f, $from, $to); }
    | "ONCE" "[" number[from] "," infnumber[to] "]" formula[f] { $$ = new SinceFormula(new BoolFormula(true), $f, $from, $to); }
    | "ALWAYS" "[" number[from] "," number[to] "]" formula[f] { $$ = new NegFormula(new UntilFormula(new BoolFormula(true), new NegFormula($f), $from, $to)); }
    | "HISTORICALLY" "[" number[from] "," infnumber[to] "]" formula[f] { $$ = new NegFormula(new SinceFormula(new BoolFormula(true), new NegFormula($f), $from, $to));; }
    | "(" formula[f] ")" { $$ = $f; }
    ;
 
%%
