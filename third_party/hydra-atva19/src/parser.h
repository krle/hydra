/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 18 "parser.y" /* yacc.c:1909  */

    typedef void *yyscan_t;

#line 48 "parser.h" /* yacc.c:1909  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    TOKEN_FALSE = 258,
    TOKEN_TRUE = 259,
    TOKEN_NEG = 260,
    TOKEN_CONJ = 261,
    TOKEN_DISJ = 262,
    TOKEN_IMP = 263,
    TOKEN_IFF = 264,
    TOKEN_SINCE = 265,
    TOKEN_UNTIL = 266,
    TOKEN_NEXT = 267,
    TOKEN_PREV = 268,
    TOKEN_EVENTUALLY = 269,
    TOKEN_ONCE = 270,
    TOKEN_ALWAYS = 271,
    TOKEN_HISTORICALLY = 272,
    TOKEN_LPAREN = 273,
    TOKEN_RPAREN = 274,
    TOKEN_PRED = 275,
    TOKEN_NUMBER = 276,
    TOKEN_INFINITY = 277,
    TOKEN_INTLPAREN = 278,
    TOKEN_SEP = 279,
    TOKEN_INTRPAREN = 280
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 30 "parser.y" /* yacc.c:1909  */

    char name[MAX_PRED_LEN];
    int value;
    Formula *fmla;

#line 92 "parser.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int yyparse (Formula **fmla, yyscan_t scanner);

#endif /* !YY_YY_PARSER_H_INCLUDED  */
