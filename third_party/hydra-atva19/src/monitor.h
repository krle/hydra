#ifndef __MONITOR_H__
#define __MONITOR_H__

#include "constants.h"
#include "circular_buffer.h"
#include "delta_queue.h"

#include <cassert>
#include <exception>
#include <optional>
#include <stdexcept>

struct Formula;

struct EOL : std::exception {};
   
struct BoolVerdict {
    timestamp ts;
    bool b;

    BoolVerdict(timestamp ts, bool b) : ts(ts), b(b) {}
    bool operator==(const BoolVerdict &bv) const {
        return ts == bv.ts && b == bv.b;
    }
};

struct BooleanVerdict {
    timestamp ts;
    Boolean b;

    BooleanVerdict(timestamp ts, Boolean b) : ts(ts), b(b) {}
    BooleanVerdict operator!() const {
        BooleanVerdict v = *this;
        if (v.b == TRUE) v.b = FALSE;
        else if (v.b == FALSE) v.b = TRUE;
        return v;
    }
    BooleanVerdict operator&&(const BooleanVerdict &w) const {
        assert(this->ts == w.ts);
        BooleanVerdict v = *this;
        if (this->b == FALSE || w.b == FALSE) v.b = FALSE;
        else if (this->b == TRUE && w.b == TRUE) v.b = TRUE;
        else v.b = UNRESOLVED;
        return v;
    }
    BooleanVerdict operator||(const BooleanVerdict &w) const {
        assert(this->ts == w.ts);
        BooleanVerdict v = *this;
        if (this->b == TRUE || w.b == TRUE) v.b = TRUE;
        else if (this->b == FALSE && w.b == FALSE) v.b = FALSE;
        else v.b = UNRESOLVED;
        return v;
    }
};

struct Monitor {
    bool eof = false;

    virtual ~Monitor() {}
    virtual BooleanVerdict step() final {
        if (eof) return handle_eof();
        else {
            try {
                return step_impl();
            } catch(const EOL &e) {
                return handle_eof();
            }
        }
    }
    virtual BooleanVerdict step_impl() = 0;
    virtual BooleanVerdict handle_eof() {
        eof = true;
        throw EOL();
    }
};

struct NonTempMonitor : Monitor {
    Formula *fmla;
    FILE *f;
    std::optional<timestamp> t;

    NonTempMonitor(Formula *fmla, const char *fname) : fmla(fmla) {
        f = fopen(fname, "r");
        if (f == NULL) throw std::runtime_error("log file");
    }
    ~NonTempMonitor() override {
        fclose(f);
    }
    BooleanVerdict step_impl() override;
};

struct NegMonitor : Monitor {
    Monitor *subf;

    NegMonitor(Monitor *subf) : subf(subf) {}
    ~NegMonitor() override {
        delete subf;
    }
    BooleanVerdict step_impl() override {
        return !subf->step();
    }
};

struct AndMonitor : Monitor {
    Monitor *subf, *subg;

    AndMonitor(Monitor *subf, Monitor *subg) : subf(subf), subg(subg) {}
    ~AndMonitor() override {
        delete subf;
        delete subg;
    }
    BooleanVerdict step_impl() override {
        return subf->step() && subg->step();
    }
};

struct OrMonitor : Monitor {
    Monitor *subf, *subg;

    OrMonitor(Monitor *subf, Monitor *subg) : subf(subf), subg(subg) {}
    ~OrMonitor() override {
        delete subf;
        delete subg;
    }
    BooleanVerdict step_impl() override {
        return subf->step() || subg->step();
    }
};

struct PrevMonitor : Monitor {
    timestamp from, to;
    Monitor *subf;
    std::optional<BooleanVerdict> v;

    PrevMonitor(Monitor *subf, timestamp from, timestamp to) : from(from), to(to), subf(subf) {}
    ~PrevMonitor() override {
        delete subf;
    }
    BooleanVerdict step_impl() override;
};

struct NextMonitor : Monitor {
    timestamp from, to;
    Monitor *subf;
    std::optional<timestamp> t;

    NextMonitor(Monitor *subf, timestamp from, timestamp to) : from(from), to(to), subf(subf) {}
    ~NextMonitor() override {
        delete subf;
    }
    BooleanVerdict step_impl() override;
    BooleanVerdict handle_eof() override;
};

struct SinceMonitor : Monitor {
    timestamp from, to;
    Monitor *subf, *subg;

    std::optional<BoolVerdict> v[2];
    DeltaQueue *dq[2];

    SinceMonitor(Monitor *subf, Monitor *subg, timestamp from, timestamp to) : from(from), to(to), subf(subf), subg(subg) {
        if (to < MAX_TIMESTAMP) dq[0] = new DeltaQueue(2 * (to + 1));
        else dq[0] = new DeltaQueue(2 * (from + 1));
        if (to < MAX_TIMESTAMP) dq[1] = new DeltaQueue(2 * (to + 1));
        else dq[1] = new DeltaQueue(2 * (from + 1));
    }
    ~SinceMonitor() override {
        delete subf;
        delete subg;
        delete dq[0];
        delete dq[1];
    }
    BooleanVerdict step_impl() override;
};

struct UntilMonitor : Monitor {
    timestamp from, to;
    Monitor *subf, *subg;

    std::optional<timestamp> t;
    DeltaQueue *dq[2];
    circular_buffer<Bunch<timestamp_delta> > *tmp;
    circular_buffer<BoolVerdict> *q[2];

    UntilMonitor(Monitor *subf, Monitor *subg, timestamp from, timestamp to) : from(from), to(to), subf(subf), subg(subg) {
        dq[0] = new DeltaQueue(2 * (to + 1));
        dq[1] = new DeltaQueue(2 * (to + 1));
        tmp = new circular_buffer<Bunch<timestamp_delta> >(to + 1);
        q[0] = new circular_buffer<BoolVerdict>(to + 2);
        q[1] = new circular_buffer<BoolVerdict>(to + 2);
    }
    ~UntilMonitor() {
        delete subf;
        delete subg;
        delete dq[0];
        delete dq[1];
        delete tmp;
        delete q[0];
        delete q[1];
    }
    BooleanVerdict step_impl() override;
    BooleanVerdict handle_eof() override;
    void resolve_until(int id, timestamp bound, bool strict, bool b);
    void resolve_all(int id, bool b) {
        resolve_until(id, 0, false, b);
    }
    std::optional<BooleanVerdict> pull_verdict();
};

extern int *ap_lookup;
extern int *ap_list;

#endif /* __MONITOR_H__ */
