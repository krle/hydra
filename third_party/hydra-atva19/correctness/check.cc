/* This tool compares output produced by HYDRA and AERIAL. */

#include <cassert>
#include <cstdio>
#include <cstring>
#include <map>
#include <vector>
using namespace std;

FILE *f1, *f2;

map<pair<int,int>, int> v1, v2;
map<pair<int,int>, vector<pair<int,int> > > eq;

void propagate(int ts, int off, int b, map<pair<int,int>, int> &v) {
	v[make_pair(ts, off)]=b;
	for(auto &it : eq[make_pair(ts, off)]) {
		propagate(it.first, it.second, b, v);
	}
}

void read_verdicts(FILE *f, map<pair<int,int>, int> &v) {
	eq.clear();
	while(!feof(f)) {
		int ts, off;
		char c;
		fscanf(f, "%d:%d %c", &ts, &off, &c);
		if(c=='t') {
			fscanf(f, "%*s\n");
			propagate(ts, off, 1, v);
		} else if(c=='f') {
			fscanf(f, "%*s\n");
			propagate(ts, off, 0, v);
		} else {
			assert(c=='=');
			int old_ts, old_off;
			fscanf(f, " %d:%d\n", &old_ts, &old_off);
			eq[make_pair(old_ts, old_off)].push_back(make_pair(ts, off));
		}
	}
}

int main(int argc, char **argv) {
	if(argc<3) exit(EXIT_FAILURE);

    int m[2];
    m[0] = strstr(argv[1], "main") != NULL;
    m[1] = strstr(argv[2], "main") != NULL;

	f1=fopen(argv[1], "r");
	f2=fopen(argv[2], "r");

	read_verdicts(f1, v1);
	read_verdicts(f2, v2);

	if(v1==v2) {
        fprintf(stderr, "OK\n");
	} else {
        fprintf(stderr, "DIFF\n");
    }

	return v1!=v2;
}
