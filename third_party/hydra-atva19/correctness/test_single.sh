#!/bin/bash

SIZE=$1
MAXR=$2
SEED=$3

FID=${SIZE}_${MAXR}_${SEED}

../evaluation/gen_fmla ${SIZE} ${MAXR} ${SEED} > z_${FID}.mtl
echo "Testing $(cat z_${FID}.mtl)"

../hydra z_${FID}.mtl ../evaluation/logs/shared.log > z_${FID}.hydra
if [ $? -ne 0 ]; then
    cp z_${FID}.mtl bug_${FID}.mtl
fi
sed -i -r "/^[MB].*$/d" z_${FID}.hydra # delete first line of HYDRA's output

../aerial.native -expr -mode "local" -mtl -fmla z_${FID}.mtl -log ../evaluation/logs/shared.log | egrep -o "^[^MB].*$" > z_${FID}.aerial


./check z_${FID}.aerial z_${FID}.hydra
if [ $? -ne 0 ]; then
    cp z_${FID}.mtl bug_${FID}.mtl
fi

rm -f z_${FID}.mtl
rm -f z_${FID}.hydra
rm -f z_${FID}.aerial
