#!/bin/bash

FMLAS=$1
if [ -z $FMLAS ]
then
    FMLAS=10
fi

cd $(dirname $0)

echo "Compiling prerequisites"

make evaluation
cd evaluation

echo "Running experiments"

./experiment_trace.sh ${FMLAS}
./experiment_size.sh ${FMLAS}
./experiment_scaling.sh ${FMLAS}
./experiment_wc.sh

echo "Processing results"

./process_all.sh ${FMLAS}

echo "Plotting data"

./plot_all.sh

echo "Finished"
