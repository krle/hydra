FROM ubuntu:18.04

RUN apt-get update && apt-get install --no-install-recommends -y make gnuplot clang m4 libboost-dev libgmp3-dev python3-pip ocamlbuild ocaml-nox opam vim locales
RUN locale-gen en_US.UTF-8 \
    && echo "export LANG=en_US.UTF-8 LANGUAGE=en_US.en LC_ALL=en_US.UTF-8" >> /root/.bashrc
RUN pip3 install setuptools wheel
RUN pip3 install antlr4-python3-runtime pyyaml
RUN opam init --comp=4.05.0
RUN opam switch 4.05.0
RUN opam install ocamlfind safa menhir zarith

USER root
ENV WDIR /home/root/hydra
ENV TDIR /home/root/hydra/workdir
RUN mkdir -p ${WDIR}
RUN mkdir -p ${TDIR}
WORKDIR ${WDIR}
ADD . ${WDIR}
RUN make -C src && cp hydra /usr/local/bin/hydra

RUN cd third_party/aerial; eval $(opam config env); make
RUN cd third_party/hydra-atva19; make hydra
RUN cd third_party/monpoly; eval $(opam config env); make
RUN cd third_party/pcre2-10.34; ./configure && make
RUN cd third_party/reelay-codegen; python3 setup.py build; cp scripts/reelay reelay.py
RUN cd src; make hydra
RUN cd vydra; eval $(opam config env); make
RUN cd evaluation; make all
