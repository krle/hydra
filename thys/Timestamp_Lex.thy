theory Timestamp_Lex
  imports "HOL-Library.Product_Lexorder" Timestamp
begin

class timestamp_linorder = timestamp + linorder

instantiation prod :: (timestamp_linorder, timestamp_linorder) timestamp_linorder
begin

definition embed_nat_prod :: "nat \<Rightarrow> 'a \<times> 'b" where
  "embed_nat_prod n = (embed_nat n, embed_nat 0)"

definition zero_prod :: "'a \<times> 'b" where
  "zero_prod = (zero_class.zero, zero_class.zero)"

fun plus_prod :: "'a \<times> 'b \<Rightarrow> 'a \<times> 'b \<Rightarrow> 'a \<times> 'b" where
  "(a, b) + (c, d) = (a + c, b + d)"

instance
  apply standard
       apply (auto simp add: ab_semigroup_add_class.add_ac zero_prod_def leD add_mono_comm
      embed_nat_prod_def embed_nat_mono)
  using embed_nat_unbounded
    apply blast
   apply (metis add.commute add_mono)+
  done

end

end
