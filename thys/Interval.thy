(*<*)
theory Interval
  imports "HOL-Library.Product_Lexorder" Timestamp
begin
(*>*)

section \<open>Intervals\<close>

typedef (overloaded) ('a :: "{order, zero}") \<I> = "{(i :: 'a, j :: 'a ets). 0 \<le> i \<and> ets i \<le> j}"
  by (intro exI[of _ "(0, \<infinity>)"]) (auto simp add: less_eq_ets_def)

setup_lifting type_definition_\<I>

instantiation \<I> :: ("{order, zero}") equal begin
lift_definition equal_\<I> :: "'a \<I> \<Rightarrow> 'a \<I> \<Rightarrow> bool" is "(=)" .
instance by standard (transfer, auto)
end

instantiation \<I> :: ("{order, zero}") order begin
lift_definition less_eq_\<I> :: "'a \<I> \<Rightarrow> 'a \<I> \<Rightarrow> bool" is "(\<le>)" .
lift_definition less_\<I> :: "'a \<I> \<Rightarrow> 'a \<I> \<Rightarrow> bool" is "(<)" .
instance
  by standard (transfer, auto simp add: dual_order.order_iff_strict Interval.less_eq_\<I>.rep_eq Rep_\<I>_inject)
end


lift_definition left :: "'a :: {order, zero} \<I> \<Rightarrow> 'a" is fst .
lift_definition right :: "'a :: {order, zero} \<I> \<Rightarrow> 'a ets" is snd .
definition mem :: "'a :: {order, plus, zero} \<Rightarrow> 'a \<Rightarrow> 'a \<I> \<Rightarrow> bool" where
  "mem i j I \<equiv> (i + left I \<le> j \<and> ets j \<le> ets i + right I)"

lemma mem_eq_nat:
  fixes i :: nat
  assumes "i \<le> j"
  shows "mem i j I \<longleftrightarrow> (left I \<le> j - i \<and> ets (j - i) \<le> right I)"
  unfolding mem_def
  using assms
  by (cases "right I"; transfer) (auto simp add: less_eq_ets_def)

lemma mem_eq_real:
  fixes i :: real
  assumes "i \<le> j"
  shows "mem i j I \<longleftrightarrow> (left I \<le> j - i \<and> ets (j - i) \<le> right I)"
  unfolding mem_def
  using assms
  by (cases "right I"; transfer) (auto simp add: less_eq_ets_def)

lemma right_I_add_mono:
  fixes x :: "'a :: timestamp ets"
  shows "x \<le> x + right I"
  apply transfer
  apply auto
  subgoal for x a b
    apply (cases x; cases b)
       apply (auto simp add: less_imp_le less_eq_ets_def)
     apply (metis add.right_neutral add_mono le_less_trans)
    using order.not_eq_order_implies_strict add_mono' by fastforce
  done

lemma right_I_add_mono':
  fixes x :: "'a :: timestamp ets"
  shows "x \<le> right I + x"
  apply transfer
  apply auto
  subgoal for x a b
    apply (cases x; cases b)
       apply (auto simp add: less_imp_le less_eq_ets_def)
     apply (metis add.commute add.right_neutral le_less_trans add_mono)
    apply (metis add.commute add.right_neutral order.not_eq_order_implies_strict add_mono)
    done
  done

lemma zero_left:
  fixes I :: "'a :: timestamp \<I>"
  shows "mem i i I \<longleftrightarrow> left I = 0"
  unfolding mem_def
  apply transfer
  apply auto
  apply (metis add.right_neutral add_mono antisym dual_order.order_iff_strict less_irrefl)
  using ets_add_mono by fastforce

lemma right_I_pos: "right I = ets rI \<Longrightarrow> 0 \<le> rI"
  apply transfer
  by auto (metis ets.inject le_less_trans less_eq_ets_def less_ets.simps(1) less_imp_le)

definition interval :: "'a :: {order, zero} \<Rightarrow> 'a ets \<Rightarrow> 'a \<I>" where
  "interval l r = (if 0 \<le> l \<and> ets l \<le> r then Abs_\<I> (l, r) else undefined)"

lemma [code abstract]: "Rep_\<I> (interval l r) = (if 0 \<le> l \<and> ets l \<le> r then (l, r) else Rep_\<I> undefined)"
  unfolding interval_def using Abs_\<I>_inverse by auto

(*<*)
end
(*>*)
