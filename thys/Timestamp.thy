theory Timestamp
  imports "HOL-Library.Extended_Nat"
begin

class timestamp = comm_monoid_add + semilattice_sup +
  fixes embed_nat :: "nat \<Rightarrow> 'a"
  assumes embed_nat_mono: "\<And>i j. i < j \<Longrightarrow> embed_nat i < embed_nat j"
    and embed_nat_unbounded: "\<exists>i. x < embed_nat i"
    and add_mono: "c < d \<Longrightarrow> a + c < a + d"
begin

lemma add_mono': "c \<le> d \<Longrightarrow> a + c \<le> a + d"
  using add_mono by (auto simp add: local.dual_order.order_iff_strict)

lemma add_mono_comm:
  fixes a :: 'a
  shows "c \<le> d \<Longrightarrow> c + a \<le> d + a"
proof -
  assume assm: "c \<le> d"
  have "c + a = a + c"
    by (rule add.commute)
  moreover have "\<dots> \<le> a + d"
    by (rule add_mono'[OF assm])
  moreover have "\<dots> = d + a"
    by (rule add.commute)
  ultimately show ?thesis
    by auto
qed

end

lemma sup_fin_closed: "finite A \<Longrightarrow> A \<noteq> {} \<Longrightarrow>
  (\<And>x y. x \<in> A \<Longrightarrow> y \<in> A \<Longrightarrow> sup x y \<in> {x, y}) \<Longrightarrow> \<Squnion>\<^sub>f\<^sub>i\<^sub>n A \<in> A"
  apply (induct A rule: finite.induct)
  using Sup_fin.insert
  by auto fastforce

instantiation nat :: timestamp
begin

definition embed_nat_nat :: "nat \<Rightarrow> nat" where
  "embed_nat_nat n = n"

instance
  apply standard
       apply (auto simp add: embed_nat_nat_def cInf_lower)
  done

end

instantiation rat :: timestamp
begin

definition embed_nat_rat :: "nat \<Rightarrow> rat" where
  "embed_nat_rat n = rat_of_nat n"

instance
  apply standard
    apply (auto simp add: embed_nat_rat_def reals_Archimedean2)
  done

end

instantiation real :: timestamp
begin

definition embed_nat_real :: "nat \<Rightarrow> real" where
  "embed_nat_real n = real n"

instance
  apply standard
      apply (auto simp add: embed_nat_real_def reals_Archimedean2)
  done

end

datatype 'a ets = ets (the_ets: 'a) | Infty

instantiation ets :: (order) infinity
begin

definition "infinity_ets = Infty"
instance ..

end

instantiation ets :: (order) order
begin

fun less_ets :: "'a ets \<Rightarrow> 'a ets \<Rightarrow> bool" where
  "ets x < ets y \<longleftrightarrow> x < y"
| "ets x < Infty \<longleftrightarrow> True"
| "Infty < _ \<longleftrightarrow> False"

definition "x \<le> (y :: 'a ets) \<longleftrightarrow> x < y \<or> x = y"

lemma ets_less_eqI: "x \<le> y \<Longrightarrow> ets x \<le> ets y"
  by (auto simp add: less_eq_ets_def)

lemma ets_infty_less[simp]:
  fixes x :: "'a ets"
  shows "x < \<infinity> \<longleftrightarrow> (x \<noteq> \<infinity>)"
  by (cases x) (auto simp add: infinity_ets_def)

lemma ets_infty_less_eq[simp]:
  fixes x :: "'a ets"
  shows "\<infinity> \<le> x \<longleftrightarrow> x = \<infinity>"
  by (cases x) (auto simp add: infinity_ets_def less_eq_ets_def)

lemma ets_infty_less_eq2:
  "a \<le> b \<Longrightarrow> a = \<infinity> \<Longrightarrow> b = (\<infinity>::'a ets)"
  by simp_all

instance
  apply standard
  using less_ets.elims(3)
     apply (auto simp add: less_eq_ets_def elim!: less_ets.elims)
  apply (meson dual_order.strict_trans)
  done

end

instantiation ets :: (semilattice_sup) semilattice_sup
begin

fun sup_ets :: "'a ets \<Rightarrow> 'a ets \<Rightarrow> 'a ets" where
  "sup_ets Infty _ = Infty"
| "sup_ets _ Infty = Infty"
| "sup_ets (ets a) (ets b) = ets (sup a b)"

instance
  apply standard
  subgoal for x y
    by (cases x; cases y) (auto simp add: ets_less_eqI less_imp_le)
  subgoal for x y
    by (cases x; cases y) (auto simp add: ets_less_eqI less_imp_le)
  subgoal for y x z
    by (cases x; cases y; cases z)
       (auto simp add: less_imp_le less_eq_ets_def order.not_eq_order_implies_strict
        order.strict_implies_order)
  done

end

instantiation ets :: (zero) zero
begin

definition zero_ets :: "'a ets" where
  "zero_ets = ets 0"

instance ..

end

instantiation ets :: (plus) plus
begin

fun plus_ets :: "'a ets \<Rightarrow> 'a ets \<Rightarrow> 'a ets" where
  "plus_ets (ets x) (ets y) = ets (x + y)"
| "plus_ets Infty _ = Infty"
| "plus_ets _ Infty = Infty"

instance ..

end

lemma ets_add_mono:
  fixes a :: "'a :: timestamp"
  shows "c \<le> d \<Longrightarrow> ets a + c \<le> ets a + d"
  apply (cases c; cases d)
     apply (auto simp add: leI less_imp_le dual_order.order_iff_strict add_mono)
  done

lemma ets_add_mono':
  fixes c :: "'a :: timestamp"
  shows "c \<le> d \<Longrightarrow> a + ets c \<le> a + ets d"
  by (cases a) (auto simp add: add_mono' ets_less_eqI)

lemma ets_add_mono_comm:
  fixes a :: "'a :: timestamp"
  shows "c \<le> d \<Longrightarrow> c + ets a \<le> d + ets a"
proof -
  assume assm: "c \<le> d"
  have "c + ets a = ets a + c"
    by (metis add.commute ets.discI ets.sel plus_ets.elims)
  moreover have "\<dots> \<le> ets a + d"
    using ets_add_mono[OF assm] .
  moreover have "\<dots> = d + ets a"
    by (metis add.commute ets.discI ets.sel plus_ets.elims)
  ultimately show ?thesis
    by simp
qed

lemma ets_add_mono_comm':
  fixes c :: "'a :: timestamp"
  shows "c \<le> d \<Longrightarrow> ets c + a \<le> ets d + a"
  by (cases a) (auto simp add: add_mono_comm ets_less_eqI)

lemma ets_add_mono_pure:
  fixes a :: "'a :: timestamp ets"
  shows "c \<le> d \<Longrightarrow> a + c \<le> a + d"
  apply (cases a; cases c; cases d)
     apply (auto simp add: leI less_imp_le dual_order.order_iff_strict add_mono)
  done

lemma ets_add_mono'_pure:
  fixes c :: "'a :: timestamp ets"
  shows "c \<le> d \<Longrightarrow> c + a \<le> d + a"
proof -
  assume assm: "c \<le> d"
  have "c + a = a + c"
    by (metis add.commute ets.inject ets.simps(3) plus_ets.elims)
  moreover have "\<dots> \<le> a + d"
    by (rule ets_add_mono_pure[OF assm])
  moreover have "\<dots> = d + a"
    by (metis add.commute ets.inject ets.simps(3) plus_ets.elims)
  ultimately show ?thesis
    by auto
qed

lemma ets_add_mono_rev:
  fixes a :: "'a :: timestamp"
  shows "ets a \<le> ets b \<Longrightarrow> a \<le> b"
  by (auto simp add: less_eq_ets_def elim: less_ets.cases)

lemma ets_add_assoc:
  fixes a :: "'a :: timestamp ets"
  shows "a + (b + c) = a + b + c"
  by (cases a; cases b; cases c) (auto simp: add.assoc)

end