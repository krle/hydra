theory Monitor_Code
  imports "HOL-Library.Code_Target_Nat" Monitor
begin

code_printing
  code_module "IArray" \<rightharpoonup> (OCaml)
\<open>module IArray : sig
  val of_list : 'a list -> 'a array
  val length' : 'a array -> Z.t
  val sub' : 'a array * Z.t -> 'a
end = struct

let of_list = Array.of_list;;

let length' xs = Z.of_int (Array.length xs);;

let sub' (xs, i) = Array.get xs (Z.to_int i);;

end\<close> for type_constructor iarray constant IArray.length' IArray.sub'

code_reserved OCaml IArray

code_printing
  type_constructor iarray \<rightharpoonup> (OCaml) "_ array"
| constant IArray \<rightharpoonup> (OCaml) "IArray.of'_list"
| constant IArray.length' \<rightharpoonup> (OCaml) "IArray.length'"
| constant IArray.sub' \<rightharpoonup> (OCaml) "IArray.sub'"

definition mk_event :: "String.literal list \<Rightarrow> String.literal set" where "mk_event = set"

definition nat_interval :: "_ \<Rightarrow> _ \<Rightarrow> nat \<I>" where
  "nat_interval = interval"
definition sub_vydra :: "_ \<Rightarrow> _ \<Rightarrow> _ \<Rightarrow> _ \<Rightarrow> (String.literal, nat, 'e) vydra" where
  "sub_vydra = sub"
definition run_vydra :: " _ \<Rightarrow> _ \<Rightarrow> (String.literal, nat, 'e) vydra \<Rightarrow> _" where
  "run_vydra = run"
definition msize_fmla_vydra :: "(String.literal, nat) formula \<Rightarrow> nat" where
  "msize_fmla_vydra = msize_fmla"

export_code sub_vydra run_vydra msize_fmla_vydra
  Bool ets nat_interval nat_of_integer integer_of_nat mk_event
  in OCaml module_name VYDRA file_prefix "verified"

definition real_interval :: "_ \<Rightarrow> _ \<Rightarrow> real \<I>" where
  "real_interval = interval"
definition sub_real_vydra :: "_ \<Rightarrow> _ \<Rightarrow> _ \<Rightarrow> _ \<Rightarrow> (String.literal, real, 'e) vydra" where
  "sub_real_vydra = sub"
definition run_real_vydra :: " _ \<Rightarrow> _ \<Rightarrow> (String.literal, real, 'e) vydra \<Rightarrow> _" where
  "run_real_vydra = run"

end
