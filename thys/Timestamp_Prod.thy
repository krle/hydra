theory Timestamp_Prod
  imports Timestamp
begin

instantiation prod :: (timestamp, timestamp) timestamp
begin

definition embed_nat_prod :: "nat \<Rightarrow> 'a \<times> 'b" where
  "embed_nat_prod n = (embed_nat n, embed_nat n)"

definition zero_prod :: "'a \<times> 'b" where
  "zero_prod = (zero_class.zero, zero_class.zero)"

fun plus_prod :: "'a \<times> 'b \<Rightarrow> 'a \<times> 'b \<Rightarrow> 'a \<times> 'b" where
  "(a, b) + (c, d) = (a + c, b + d)"

fun sup_prod :: "'a \<times> 'b \<Rightarrow> 'a \<times> 'b \<Rightarrow> 'a \<times> 'b" where
  "sup_prod (x, y) (x', y') = (sup x x', sup y y')"

fun less_eq_prod :: "'a \<times> 'b \<Rightarrow> 'a \<times> 'b \<Rightarrow> bool" where
  "less_eq_prod (x, y) (x', y') \<longleftrightarrow> x \<le> x' \<and> y \<le> y'"

definition less_prod :: "'a \<times> 'b \<Rightarrow> 'a \<times> 'b \<Rightarrow> bool" where
  "less_prod x y \<longleftrightarrow> x \<le> y \<and> x \<noteq> y"

instance
  apply standard
              apply (auto simp add: add.assoc add.commute zero_prod_def less_prod_def
      embed_nat_prod_def embed_nat_mono less_imp_le)
           apply (metis add.assoc add.commute)
          apply (metis add.assoc add.commute)
  using embed_nat_mono less_le
         apply auto[1]
  subgoal
  proof -
    fix a :: 'a and b :: 'b
    obtain nn :: "'a \<Rightarrow> nat" where
      f1: "\<forall>a. a < embed_nat (nn a)"
      by (metis (no_types) embed_nat_unbounded)
    have f2: "\<forall>n na. (embed_nat n::'a) < embed_nat na \<or> na \<le> n"
      by (meson embed_nat_mono not_le)
    have f3: "\<forall>a. a \<le> embed_nat (nn a)"
      using f1 less_imp_le by blast
    obtain nna :: "'b \<Rightarrow> nat" where
      f4: "\<forall>b. b \<le> embed_nat (nna b)"
      by (meson embed_nat_unbounded less_imp_le)
    have f5: "\<forall>n na. (embed_nat n::'a) \<le> embed_nat na \<or> na \<le> n"
      using f2 by (meson less_imp_le)
    have f6: "\<forall>b n. b \<le> embed_nat n \<or> n \<le> nna b"
      using f4 by (meson embed_nat_mono less_imp_le not_le order_trans)
    have "\<forall>a n. a \<le> embed_nat n \<or> n \<le> nn a"
      using f5 f3 by (meson order_trans)
    then show "\<exists>n. a \<le> embed_nat n \<and> b \<le> embed_nat n \<and> (a = embed_nat n \<longrightarrow> b \<noteq> embed_nat n)"
      using f6 f4 f2 f1 by (metis dual_order.order_iff_strict order_less_asym)
  qed
       apply (auto simp add: add_mono_comm)
   apply (smt add.commute dual_order.strict_iff_order timestamp_class.add_mono)+
  done

end

end