theory Window
  imports "HOL-Library.AList" "HOL-Library.Mapping" "HOL-Library.While_Combinator" Timestamp
begin

type_synonym ('a, 'b) mmap = "('a \<times> 'b) list"

(* 'b is a polymorphic input symbol; 'c is a polymorphic DFA state;
   'd is a timestamp; 'e is a submonitor state *)

inductive chain_le :: "'d :: timestamp list \<Rightarrow> bool" where
  chain_le_Nil: "chain_le []"
| chain_le_singleton: "chain_le [x]"
| chain_le_cons: "chain_le (y # xs) \<Longrightarrow> x \<le> y \<Longrightarrow> chain_le (x # y # xs)"

lemma chain_le_app: "chain_le (zs @ [z]) \<Longrightarrow> z \<le> w \<Longrightarrow> chain_le ((zs @ [z]) @ [w])"
  apply (induction "zs @ [z]" arbitrary: zs rule: chain_le.induct)
    apply (auto intro: chain_le.intros)[2]
  subgoal for y xs x zs
    apply (cases zs)
     apply (auto)
    apply (metis append.assoc append_Cons append_Nil chain_le_cons)
    done
  done

inductive reach_sub :: "('e \<Rightarrow> ('e \<times> 'f) option) \<Rightarrow> 'e \<Rightarrow> 'f list \<Rightarrow> 'e \<Rightarrow> bool"
  for run :: "'e \<Rightarrow> ('e \<times> 'f) option" where
    "reach_sub run s [] s"
  | "run s = Some (s', v) \<Longrightarrow> reach_sub run s' vs s'' \<Longrightarrow> reach_sub run s (v # vs) s''"

lemma reach_sub_split: "reach_sub run s vs s' \<Longrightarrow> i < length vs \<Longrightarrow>
  \<exists>s'' s'''. reach_sub run s (take i vs) s'' \<and> run s'' = Some (s''', vs ! i)"
proof (induction s vs s' arbitrary: i rule: reach_sub.induct)
  case (2 s s' v vs s'')
  show ?case
    using 2(1)
  proof (cases i)
    case (Suc n)
    show ?thesis
      using 2
      by (fastforce simp: Suc intro: reach_sub.intros)
  qed (auto intro: reach_sub.intros)
qed auto

lemma reach_sub_split': "reach_sub run s vs s' \<Longrightarrow> i \<le> length vs \<Longrightarrow>
  \<exists>s'' . reach_sub run s (take i vs) s'' \<and> reach_sub run s'' (drop i vs) s'"
proof (induction s vs s' arbitrary: i rule: reach_sub.induct)
  case (2 s s' v vs s'')
  show ?case
    using 2(1,2)
  proof (cases i)
    case (Suc n)
    show ?thesis
      using 2
      by (fastforce simp: Suc intro: reach_sub.intros)
  qed (auto intro: reach_sub.intros)
qed (auto intro: reach_sub.intros)

lemma reach_sub_split_app: "reach_sub run s (vs @ vs') s' \<Longrightarrow>
  \<exists>s''. reach_sub run s vs s'' \<and> reach_sub run s'' vs' s'"
  using reach_sub_split'[where i="length vs", of run s "vs @ vs'" s']
  by auto

lemma reach_sub_inj: "reach_sub run s vs t \<Longrightarrow> reach_sub run s vs' t' \<Longrightarrow>
  length vs = length vs' \<Longrightarrow> vs = vs' \<and> t = t'"
  apply (induction s vs t arbitrary: vs' t' rule: reach_sub.induct)
   apply (auto elim: reach_sub.cases)[1]
  subgoal for s s' v vs s'' vs' t'
    apply (rule reach_sub.cases[of run s' vs s'']; rule reach_sub.cases[of run s vs' t'])
            apply assumption+
        apply auto[2]
      apply fastforce
     apply (metis length_0_conv list.discI)
    apply (metis Pair_inject length_Cons nat.inject option.inject)
    done
  done

lemma reach_sub_split_last: "reach_sub run s (xs @ [x]) s'' \<Longrightarrow>
  \<exists>s'. reach_sub run s xs s' \<and> run s' = Some (s'', x)"
  apply (induction s "xs @ [x]" s'' arbitrary: xs x rule: reach_sub.induct)
   apply simp
  subgoal for s s' v vs s'' xs x
    by (cases vs rule: rev_cases) (fastforce elim: reach_sub.cases intro: reach_sub.intros)+
  done

lemma reach_sub_rev_induct[consumes 1]: "reach_sub run s vs s' \<Longrightarrow>
  (\<And>s. P s [] s) \<Longrightarrow>
  (\<And>s s' v vs s''. reach_sub run s vs s' \<Longrightarrow> P s vs s' \<Longrightarrow> run s' = Some (s'', v) \<Longrightarrow>
    P s (vs @ [v]) s'') \<Longrightarrow>
  P s vs s'"
proof (induction vs arbitrary: s s' rule: rev_induct)
  case (snoc x xs)
  from snoc(2) obtain s'' where s''_def: "reach_sub run s xs s''" "run s'' = Some (s', x)"
    using reach_sub_split_last
    by fast
  show ?case
    using snoc(4)[OF s''_def(1) _ s''_def(2)] snoc(1)[OF s''_def(1) snoc(3,4)]
    by auto
qed (auto elim: reach_sub.cases)

lemma reach_sub_app: "reach_sub run s vs s' \<Longrightarrow> run s' = Some (s'', v) \<Longrightarrow>
  reach_sub run s (vs @ [v]) s''"
  by (induction s vs s' rule: reach_sub.induct) (auto intro: reach_sub.intros)

lemma reach_sub_trans: "reach_sub run s vs s' \<Longrightarrow> reach_sub run s' vs' s'' \<Longrightarrow>
  reach_sub run s (vs @ vs') s''"
  by (induction s vs s' rule: reach_sub.induct) (auto intro: reach_sub.intros)

lemma reach_subD: "reach_sub run s ((t, b) # vs) s' \<Longrightarrow>
  \<exists>s''. run s = Some (s'', (t, b)) \<and> reach_sub run s'' vs s'"
  by (auto elim: reach_sub.cases)

lemma reach_sub_setD: "reach_sub run s vs s' \<Longrightarrow> x \<in> set vs \<Longrightarrow>
  \<exists>vs' vs'' s''. reach_sub run s (vs' @ [x]) s'' \<and> reach_sub run s'' vs'' s' \<and> vs = vs' @ x # vs''"
proof (induction s vs s' rule: reach_sub_rev_induct)
  case (2 s s' v vs s'')
  show ?case
  proof (cases "x \<in> set vs")
    case True
    obtain vs' vs'' s''' where split_def: "reach_sub run s (vs' @ [x]) s'''"
      "reach_sub run s''' vs'' s'" "vs = vs' @ x # vs''"
      using 2(3)[OF True]
      by auto
    show ?thesis
      using split_def(1,3) reach_sub_app[OF split_def(2) 2(2)]
      by auto
  next
    case False
    have x_v: "x = v"
      using 2(4) False
      by auto
    show ?thesis
      unfolding x_v
      using reach_sub_app[OF 2(1,2)] reach_sub.intros(1)[of run s'']
      by auto
  qed
qed auto

lemma reach_sub_len: "\<exists>vs s'. reach_sub run s vs s' \<and> (length vs = n \<or> run s' = None)"
proof (induction n arbitrary: s)
  case (Suc n)
  show ?case
  proof (cases "run s")
    case (Some x)
    obtain s' v where x_def: "x = (s', v)"
      by (cases x) auto
    obtain vs s'' where s''_def: "reach_sub run s' vs s''" "length vs = n \<or> run s'' = None"
      using Suc[of s']
      by auto
    show ?thesis
      using reach_sub.intros(2)[OF Some[unfolded x_def] s''_def(1)] s''_def(2)
      by fastforce
  qed (auto intro: reach_sub.intros)
qed (auto intro: reach_sub.intros)

lemma reach_sub_NilD: "reach_sub run q [] q' \<Longrightarrow> q = q'"
  by (auto elim: reach_sub.cases)

inductive reaches :: "('e \<Rightarrow> ('e \<times> 'f) option) \<Rightarrow> 'e \<Rightarrow> nat \<Rightarrow> 'e \<Rightarrow> bool"
  for run :: "'e \<Rightarrow> ('e \<times> 'f) option" where
    "reaches run s 0 s"
  | "run s = Some (s', v) \<Longrightarrow> reaches run s' n s'' \<Longrightarrow> reaches run s (Suc n) s''"

lemma reach_sub_n: "reach_sub run s vs s' \<Longrightarrow> reaches run s (length vs) s'"
  by (induction s vs s' rule: reach_sub.induct) (auto intro: reaches.intros)

lemma reaches_sub: "reaches run s n s' \<Longrightarrow> \<exists>vs. reach_sub run s vs s' \<and> length vs = n"
  by (induction s n s' rule: reaches.induct) (auto intro: reach_sub.intros)

definition ts_at :: "('d \<times> 'b) list \<Rightarrow> nat \<Rightarrow> 'd" where
  "ts_at rho i = fst (rho ! i)"

definition bs_at :: "('d \<times> 'b) list \<Rightarrow> nat \<Rightarrow> 'b" where
  "bs_at rho i = snd (rho ! i)"

fun sub_bs :: "('d \<times> 'b) list \<Rightarrow> nat \<times> nat \<Rightarrow> 'b list" where
  "sub_bs rho (i, j) = map (bs_at rho) [i..<j]"

definition steps :: "('c \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> ('d \<times> 'b) list \<Rightarrow> 'c \<Rightarrow> nat \<times> nat \<Rightarrow> 'c" where
  "steps step rho q ij = foldl step q (sub_bs rho ij)"

definition acc :: "('c \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> ('c \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> ('d \<times> 'b) list \<Rightarrow>
  'c \<Rightarrow> nat \<times> nat \<Rightarrow> bool" where
  "acc step accept rho q ij = accept (steps step rho q ij) (bs_at rho (snd ij))"

definition sup_acc :: "('c \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> ('c \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> ('d \<times> 'b) list \<Rightarrow>
  'c \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> ('d \<times> nat) option" where
  "sup_acc step accept rho q i j =
    (let L' = {l \<in> {i..<j}. acc step accept rho q (i, l)}; m = Max L' in
    if L' = {} then None else Some (ts_at rho m, m))"

definition sup_leadsto :: "'c \<Rightarrow> ('c \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> ('d \<times> 'b) list \<Rightarrow>
  nat \<Rightarrow> nat \<Rightarrow> 'c \<Rightarrow> 'd option" where
  "sup_leadsto init step rho i j q =
    (let L' = {l. l < i \<and> steps step rho init (l, j) = q}; m = Max L' in
    if L' = {} then None else Some (ts_at rho m))"

definition mmap_keys :: "('a, 'b) mmap \<Rightarrow> 'a set" where
  "mmap_keys kvs = set (map fst kvs)"

definition mmap_lookup :: "('a, 'b) mmap \<Rightarrow> 'a \<Rightarrow> 'b option" where
  "mmap_lookup = map_of"

definition valid_s :: "'c \<Rightarrow> ('c \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> ('c \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> ('d \<times> 'b) list \<Rightarrow>
  nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> ('c, 'c \<times> ('d \<times> nat) option) mmap \<Rightarrow> bool" where
 "valid_s init step accept rho u i j s =
  (mmap_keys s = {q. (\<exists>l \<le> u. steps step rho init (l, i) = q)} \<and> distinct (map fst s) \<and>
  (\<forall>q. case mmap_lookup s q of None \<Rightarrow> True
  | Some (q', tstp) \<Rightarrow> steps step rho q (i, j) = q' \<and> tstp = sup_acc step accept rho q i j))"

record ('b, 'c, 'd, 't, 'e) args =
  w_init :: 'c
  w_step :: "'c \<Rightarrow> 'b \<Rightarrow> 'c"
  w_accept :: "'c \<Rightarrow> 'b \<Rightarrow> bool"
  w_run_t :: "'t \<Rightarrow> ('t \<times> 'd) option"
  w_read_t :: "'t \<Rightarrow> 'd option"
  w_run_sub :: "'e \<Rightarrow> ('e \<times> 'b) option"
  w_read_sub :: "'e \<Rightarrow> 'b option"

record ('c, 'd, 't, 'e) window =
  w_i :: nat
  w_ti :: 't
  w_si :: 'e
  w_j :: nat
  w_tj :: 't
  w_sj :: 'e
  w_s :: "('c, 'c \<times> ('d \<times> nat) option) mmap"
  w_e :: "('c, 'd) mmap"

copy_bnf (dead 'c, dead 'd, dead 't, 'e, dead 'ext) window_ext

fun reach_window :: "('b, 'c, 'd, 't, 'e) args \<Rightarrow> 't \<Rightarrow> 'e \<Rightarrow>
  ('d \<times> 'b) list \<Rightarrow> nat \<times> 't \<times> 'e \<times> nat \<times> 't \<times> 'e \<Rightarrow> bool" where
  "reach_window args t0 sub rho (i, ti, si, j, tj, sj) \<longleftrightarrow> i \<le> j \<and> length rho = j \<and>
    reach_sub (w_run_t args) t0 (take i (map fst rho)) ti \<and>
    reach_sub (w_run_t args) ti (drop i (map fst rho)) tj \<and>
    reach_sub (w_run_sub args) sub (take i (map snd rho)) si \<and>
    reach_sub (w_run_sub args) si (drop i (map snd rho)) sj"

lemma reach_windowI: "reach_sub (w_run_t args) t0 (take i (map fst rho)) ti \<Longrightarrow>
  reach_sub (w_run_sub args) sub (take i (map snd rho)) si \<Longrightarrow>
  reach_sub (w_run_t args) t0 (map fst rho) tj \<Longrightarrow>
  reach_sub (w_run_sub args) sub (map snd rho) sj \<Longrightarrow>
  i \<le> length rho \<Longrightarrow> length rho = j \<Longrightarrow>
  reach_window args t0 sub rho (i, ti, si, j, tj, sj)"
  by auto (metis reach_sub_split'[of _ _ _ _ i] length_map reach_sub_inj)+

lemma reach_window_shift:
  assumes "reach_window args t0 sub rho (i, ti, si, j, tj, sj)" "i < j"
    "w_run_t args ti = Some (ti', t)" "w_run_sub args si = Some (si', s)"
  shows "reach_window args t0 sub rho (Suc i, ti', si', j, tj, sj)"
  using reach_sub_app[of "w_run_t args" t0 "take i (map fst rho)" ti ti' t]
    reach_sub_app[of "w_run_sub args" sub "take i (map snd rho)" si si' s] assms
  apply (auto)
     apply (smt append_take_drop_id id_take_nth_drop length_map list.discI list.inject
      option.inject reach_sub.cases same_append_eq snd_conv take_Suc_conv_app_nth)
    apply (smt Cons_nth_drop_Suc fst_conv length_map list.discI list.inject option.inject
      reach_sub.cases)
   apply (smt append_take_drop_id id_take_nth_drop length_map list.discI list.inject
      option.inject reach_sub.cases same_append_eq snd_conv take_Suc_conv_app_nth)
  apply (smt Cons_nth_drop_Suc fst_conv length_map list.discI list.inject option.inject
      reach_sub.cases)
  done

lemma reach_window_run_ti: "reach_window args t0 sub rho (i, ti, si, j, tj, sj) \<Longrightarrow>
  i < j \<Longrightarrow> \<exists>ti'. reach_sub (w_run_t args) t0 (take i (map fst rho)) ti \<and>
  w_run_t args ti = Some (ti', ts_at rho i) \<and>
  reach_sub (w_run_t args) ti' (drop (Suc i) (map fst rho)) tj"
  apply (auto simp: ts_at_def elim!: reach_sub.cases[of "w_run_t args" ti "drop i (map fst rho)"])
  using nth_via_drop apply fastforce
  by (metis Cons_nth_drop_Suc length_map list.inject)

lemma reach_window_run_si: "reach_window args t0 sub rho (i, ti, si, j, tj, sj) \<Longrightarrow>
  i < j \<Longrightarrow> \<exists>si'. reach_sub (w_run_sub args) sub (take i (map snd rho)) si \<and>
  w_run_sub args si = Some (si', bs_at rho i) \<and>
  reach_sub (w_run_sub args) si' (drop (Suc i) (map snd rho)) sj"
  apply (auto simp: bs_at_def elim!: reach_sub.cases[of "w_run_sub args" si "drop i (map snd rho)"])
  using nth_via_drop apply fastforce
  by (metis Cons_nth_drop_Suc length_map list.inject)

lemma reach_window_run_tj: "reach_window args t0 sub rho (i, ti, si, j, tj, sj) \<Longrightarrow>
  reach_sub (w_run_t args) t0 (map fst rho) tj"
  using reach_sub_trans
  by fastforce

lemma reach_window_run_sj: "reach_window args t0 sub rho (i, ti, si, j, tj, sj) \<Longrightarrow>
  reach_sub (w_run_sub args) sub (map snd rho) sj"
  using reach_sub_trans
  by fastforce

lemma reach_window_shift_all: "reach_window args t0 sub rho (i, si, ti, j, sj, tj) \<Longrightarrow>
  reach_window args t0 sub rho (j, sj, tj, j, sj, tj)"
  using reach_window_run_tj[of args t0 sub rho] reach_window_run_sj[of args t0 sub rho]
  by (auto intro: reach_sub.intros)

lemma reach_window_app: "reach_window args t0 sub rho (i, si, ti, j, tj, sj) \<Longrightarrow>
  w_run_t args tj = Some (tj', x) \<Longrightarrow> w_run_sub args sj = Some (sj', y) \<Longrightarrow>
  reach_window args t0 sub (rho @ [(x, y)]) (i, si, ti, Suc j, tj', sj')"
  by (fastforce simp add: reach_sub_app)

fun init_args :: "('c \<times> ('c \<Rightarrow> 'b \<Rightarrow> 'c) \<times> ('c \<Rightarrow> 'b \<Rightarrow> bool)) \<Rightarrow>
  (('t \<Rightarrow> ('t \<times> 'd) option) \<times> ('t \<Rightarrow> 'd option)) \<Rightarrow>
  (('e \<Rightarrow> ('e \<times> 'b) option) \<times> ('e \<Rightarrow> 'b option)) \<Rightarrow> ('b, 'c, 'd, 't, 'e) args" where
  "init_args (init, step, accept) (run_t, read_t) (run_sub, read_sub) =
  \<lparr>w_init = init, w_step = step, w_accept = accept, w_run_t = run_t, w_read_t = read_t,
  w_run_sub = run_sub, w_read_sub = read_sub\<rparr>"

fun init_window :: "('b, 'c, 'd, 't, 'e) args \<Rightarrow> 't \<Rightarrow> 'e \<Rightarrow> ('c, 'd, 't, 'e) window" where
  "init_window args t0 sub = \<lparr>w_i = 0, w_ti = t0, w_si = sub, w_j = 0, w_tj = t0, w_sj = sub,
  w_s =[(w_init args, (w_init args, None))], w_e = []\<rparr>"

definition valid_window :: "('b, 'c, 'd :: timestamp, 't, 'e) args \<Rightarrow> 't \<Rightarrow> 'e \<Rightarrow> ('d \<times> 'b) list \<Rightarrow>
  ('c, 'd, 't, 'e) window \<Rightarrow> bool" where
  "valid_window args t0 sub rho w \<longleftrightarrow>
    (let init = w_init args; step = w_step args; accept = w_accept args;
    run_t = w_run_t args; run_sub = w_run_sub args;
    i = w_i w; ti = w_ti w; si = w_si w; j = w_j w; tj = w_tj w; sj = w_sj w;
    s = w_s w; e = w_e w in
    (reach_window args t0 sub rho (i, ti, si, j, tj, sj) \<and>
    (\<forall>i j. i \<le> j \<and> j < length rho \<longrightarrow> ts_at rho i \<le> ts_at rho j) \<and>
    (\<forall>q. mmap_lookup e q = sup_leadsto init step rho i j q) \<and> distinct (map fst e) \<and>
    valid_s init step accept rho i i j s))"

lemma valid_init_window: "valid_window args t0 sub [] (init_window args t0 sub)"
  by (auto simp: valid_window_def mmap_keys_def mmap_lookup_def sup_leadsto_def
      valid_s_def steps_def sup_acc_def intro: reach_sub.intros split: option.splits)

lemma steps_app_cong: "j \<le> length rho \<Longrightarrow> steps step (rho @ [x]) q (i, j) =
  steps step rho q (i, j)"
proof -
  assume "j \<le> length rho"
  then have map_cong: "map (bs_at (rho @ [x])) [i..<j] = map (bs_at rho) [i..<j]"
    by (auto simp: bs_at_def nth_append)
  show ?thesis
    by (auto simp: steps_def map_cong)
qed

lemma acc_app_cong: "j < length rho \<Longrightarrow> acc step accept (rho @ [x]) q (i, j) =
  acc step accept rho q (i, j)"
  by (auto simp: acc_def bs_at_def nth_append steps_app_cong)

lemma sup_acc_app_cong: "j \<le> length rho \<Longrightarrow> sup_acc step accept (rho @ [x]) q i j =
  sup_acc step accept rho q i j"
  by (auto simp: sup_acc_def Let_def ts_at_def nth_append acc_app_cong)
     (smt Collect_cong acc_app_cong less_le_trans)+

lemma sup_acc_concat_cong: "j \<le> length rho \<Longrightarrow> sup_acc step accept (rho @ rho') q i j =
  sup_acc step accept rho q i j"
  apply (induction rho' rule: rev_induct)
   apply auto
  apply (smt append.assoc le_add1 le_trans length_append sup_acc_app_cong)
  done

lemma sup_leadsto_app_cong: "i \<le> j \<Longrightarrow> j \<le> length rho \<Longrightarrow>
  sup_leadsto init step (rho @ [x]) i j q = sup_leadsto init step rho i j q"
proof -
  assume assms: "i \<le> j" "j \<le> length rho"
  define L' where "L' = {l. l < i \<and> steps step rho init (l, j) = q}"
  define L'' where "L'' = {l. l < i \<and> steps step (rho @ [x]) init (l, j) = q}"
  show ?thesis
    using assms
    by (cases "L' = {}")
       (auto simp: sup_leadsto_def L'_def L''_def ts_at_def nth_append steps_app_cong)
qed

lemma chain_le:
  fixes xs :: "'d :: timestamp list"
  shows "chain_le xs \<Longrightarrow> i \<le> j \<Longrightarrow> j < length xs \<Longrightarrow> xs ! i \<le> xs ! j"
proof (induction xs arbitrary: i j rule: chain_le.induct)
  case (chain_le_cons y xs x)
  then show ?case
  proof (cases i)
    case 0
    then show ?thesis
      using chain_le_cons
      apply (cases j)
      apply auto
      apply (metis (no_types, lifting) le_add1 le_add_same_cancel1 le_less less_le_trans nth_Cons_0)
      done
  qed auto
qed auto

lemma steps_refl[simp]: "steps step rho q (i, i) = q"
  unfolding steps_def by auto

lemma steps_split: "i < j \<Longrightarrow> steps step rho q (i, j) =
  steps step rho (step q (bs_at rho i)) (Suc i, j)"
  unfolding steps_def by (simp add: upt_rec)

lemma steps_app: "i \<le> j \<Longrightarrow> steps step rho q (i, j + 1) =
  step (steps step rho q (i, j)) (bs_at rho j)"
  unfolding steps_def by auto

lemma steps_appE: "i \<le> j \<Longrightarrow> steps step rho q (i, Suc j) = q' \<Longrightarrow>
  \<exists>q''. steps step rho q (i, j) = q'' \<and> q' = step q'' (bs_at rho j)"
  unfolding steps_def sub_bs.simps by auto

lemma steps_comp: "i \<le> l \<Longrightarrow> l \<le> j \<Longrightarrow> steps step rho q (i, l) = q' \<Longrightarrow>
  steps step rho q' (l, j) = q'' \<Longrightarrow> steps step rho q (i, j) = q''"
proof -
  assume assms: "i \<le> l" "l \<le> j" "steps step rho q (i, l) = q'" "steps step rho q' (l, j) = q''"
  have range_app: "[i..<l] @ [l..<j] = [i..<j]"
    using assms(1,2)
    by (metis le_Suc_ex upt_add_eq_append)
  have "q' = foldl step q (map (bs_at rho) [i..<l])"
    using assms(3) unfolding steps_def by auto
  moreover have "q'' = foldl step q' (map (bs_at rho) [l..<j])"
    using assms(4) unfolding steps_def by auto
  ultimately have "q'' = foldl step q (map (bs_at rho) ([i..<l] @ [l..<j]))"
    by auto
  then show ?thesis
    unfolding steps_def range_app by auto
qed

lemma sup_acc_SomeI: "acc step accept rho q (i, l) \<Longrightarrow> l \<in> {i..<j} \<Longrightarrow>
  \<exists>tp. sup_acc step accept rho q i j = Some (ts_at rho tp, tp) \<and> l \<le> tp \<and> tp < j"
proof -
  assume assms: "acc step accept rho q (i, l)" "l \<in> {i..<j}"
  define L where "L = {l \<in> {i..<j}. acc step accept rho q (i, l)}"
  have L_props: "finite L" "L \<noteq> {}" "l \<in> L"
    using assms unfolding L_def by auto
  then show "\<exists>tp. sup_acc step accept rho q i j = Some (ts_at rho tp, tp) \<and> l \<le> tp \<and> tp < j"
    using L_def L_props
    by (auto simp add: sup_acc_def)
       (smt L_props(1) L_props(2) Max_ge Max_in mem_Collect_eq)
qed

lemma sup_acc_Some_ts: "sup_acc step accept rho q i j = Some (ts, tp) \<Longrightarrow> ts = ts_at rho tp"
  by (auto simp add: sup_acc_def Let_def split: if_splits)

lemma sup_acc_SomeE: "sup_acc step accept rho q i j = Some (ts, tp) \<Longrightarrow>
  tp \<in> {i..<j} \<and> acc step accept rho q (i, tp)"
proof -
  assume assms: "sup_acc step accept rho q i j = Some (ts, tp)"
  define L where "L  = {l \<in> {i..<j}. acc step accept rho q (i, l)}"
  have L_props: "finite L" "L \<noteq> {}" "Max L = tp"
    unfolding L_def using assms
    by (auto simp add: sup_acc_def Let_def split: if_splits)
  show ?thesis
    using Max_in[OF L_props(1,2)] unfolding L_props(3) unfolding L_def by auto
qed

lemma sup_acc_NoneE: "l \<in> {i..<j} \<Longrightarrow> sup_acc step accept rho q i j = None \<Longrightarrow>
  \<not>acc step accept rho q (i, l)"
  by (auto simp add: sup_acc_def Let_def split: if_splits)

lemma sup_acc_same: "sup_acc step accept rho q i i = None"
  by (auto simp add: sup_acc_def)

lemma sup_acc_None_restrict: "i \<le> j \<Longrightarrow> sup_acc step accept rho q i j = None \<Longrightarrow>
  sup_acc step accept rho (step q (bs_at rho i)) (i + 1) j = None"
  using steps_split
  by (auto simp add: sup_acc_def Let_def acc_def split: if_splits)
     (smt Suc_leD Suc_le_lessD steps_split)

lemma sup_acc_ext_idle: "i \<le> j \<Longrightarrow> \<not>acc step accept rho q (i, j) \<Longrightarrow>
  sup_acc step accept rho q i (j + 1) = sup_acc step accept rho q i j"
proof -
  assume assms: "i \<le> j" "\<not>acc step accept rho q (i, j)"
  define L where "L = {l \<in> {i..<j}. acc step accept rho q (i, l)}"
  define L' where "L' = {l \<in> {i..<j + 1}. acc step accept rho q (i, l)}"
  have L_L': "L = L'"
    unfolding L_def L'_def using assms(2) less_antisym by fastforce
  show "sup_acc step accept rho q i (j + 1) = sup_acc step accept rho q i j"
    using L_def L'_def L_L' by (auto simp add: sup_acc_def)
qed

lemma sup_acc_comp_Some_ge: "i \<le> l \<Longrightarrow> l \<le> j \<Longrightarrow> tp \<ge> l \<Longrightarrow>
  sup_acc step accept rho (steps step rho q (i, l)) l j = Some (ts, tp) \<Longrightarrow>
  sup_acc step accept rho q i j = sup_acc step accept rho (steps step rho q (i, l)) l j"
proof -
  assume assms: "i \<le> l" "l \<le> j" "sup_acc step accept rho (steps step rho q (i, l)) l j =
    Some (ts, tp)" "tp \<ge> l"
  define L where "L = {l \<in> {i..<j}. acc step accept rho q (i, l)}"
  define L' where "L' = {l' \<in> {l..<j}. acc step accept rho (steps step rho q (i, l)) (l, l')}"
  have L'_props: "finite L'" "L' \<noteq> {}" "tp = Max L'" "ts = ts_at rho tp"
    using assms(3) unfolding L'_def
    by (auto simp add: sup_acc_def Let_def split: if_splits)
  have tp_in_L': "tp \<in> L'"
    using Max_in[OF L'_props(1,2)] unfolding L'_props(3) .
  then have tp_in_L: "tp \<in> L"
    unfolding L_def L'_def using assms(1) steps_comp[OF assms(1,2), of step rho]
    apply (auto simp add: acc_def)
    using steps_comp
    by metis
  have L_props: "finite L" "L \<noteq> {}"
    using L_def tp_in_L by auto
  have "\<And>l'. l' \<in> L \<Longrightarrow> l' \<le> tp"
  proof -
    fix l'
    assume assm: "l' \<in> L"
    show "l' \<le> tp"
    proof (cases "l' < l")
      case True
      then show ?thesis
        using assms(4) by auto
    next
      case False
      then have "l' \<in> L'"
        using assm
        unfolding L_def L'_def
        apply (auto simp add: acc_def)
        by (metis assms(1) not_le steps_comp)
      then show ?thesis
        using Max_eq_iff[OF L'_props(1,2)] L'_props(3) by auto
    qed
  qed
  then have "Max L = tp"
    using Max_eq_iff[OF L_props] tp_in_L by auto
  then have "sup_acc step accept rho q i j = Some (ts, tp)"
    using L_def L_props(2) unfolding L'_props(4)
    by (auto simp add: sup_acc_def)
  then show "sup_acc step accept rho q i j = sup_acc step accept rho (steps step rho q (i, l)) l j"
    using assms(3) by auto
qed

lemma sup_acc_comp_None: "i \<le> l \<Longrightarrow> l \<le> j \<Longrightarrow>
  sup_acc step accept rho (steps step rho q (i, l)) l j = None \<Longrightarrow>
  sup_acc step accept rho q i j = sup_acc step accept rho q i l"
proof (induction "j - l" arbitrary: l)
  case (Suc n)
  have i_lt_j: "i < j"
    using Suc by auto
  have l_lt_j: "l < j"
    using Suc by auto
  have "\<not>acc step accept rho q (i, l)"
    using sup_acc_NoneE[of l l j step accept rho "steps step rho q (i, l)"] Suc
    by (auto simp add: acc_def)
  then have "sup_acc step accept rho q i (l + 1) = sup_acc step accept rho q i l"
    using sup_acc_ext_idle[OF Suc(3)] by auto
  moreover have "sup_acc step accept rho (steps step rho q (i, l + 1)) (l + 1) j = None"
    using sup_acc_None_restrict[OF Suc(4,5)] steps_app[OF Suc(3), of step rho]
    by auto
  ultimately show ?case
    using Suc(1)[of "l + 1"] Suc(2,3,4,5)
    by auto
qed (auto simp add: sup_acc_same)

lemma sup_acc_ext: "i \<le> j \<Longrightarrow> acc step accept rho q (i, j) \<Longrightarrow>
  sup_acc step accept rho q i (j + 1) = Some (ts_at rho j, j)"
proof -
  assume assms: "i \<le> j" "acc step accept rho q (i, j)"
  define L' where "L' = {l \<in> {i..<j + 1}. acc step accept rho q (i, l)}"
  have j_in_L': "finite L'" "L' \<noteq> {}" "j \<in> L'"
    using assms unfolding L'_def by auto
  have j_is_Max: "Max L' = j"
    using Max_eq_iff[OF j_in_L'(1,2)] j_in_L'(3)
    by (auto simp add: L'_def)
  show "sup_acc step accept rho q i (j + 1) = Some (ts_at rho j, j)"
    using L'_def j_is_Max j_in_L'(2)
    by (auto simp add: sup_acc_def)
qed

lemma sup_acc_None: "i < j \<Longrightarrow> sup_acc step accept rho q i j = None \<Longrightarrow>
  sup_acc step accept rho (step q (bs_at rho i)) (i + 1) j = None"
  using steps_split[of _ _ step rho]
  by (auto simp add: sup_acc_def Let_def acc_def split: if_splits)

lemma sup_acc_i: "i < j \<Longrightarrow> sup_acc step accept rho q i j = Some (ts, i) \<Longrightarrow>
  sup_acc step accept rho (step q (bs_at rho i)) (i + 1) j = None"
proof (rule ccontr)
  assume assms: "i < j" "sup_acc step accept rho q i j = Some (ts, i)"
    "sup_acc step accept rho (step q (bs_at rho i)) (i + 1) j \<noteq> None"
  from assms(3) obtain l where l_def: "l \<in> {i + 1..<j}"
    "acc step accept rho (step q (bs_at rho i)) (i + 1, l)"
    by (auto simp add: sup_acc_def Let_def split: if_splits)
  define L' where "L' = {l \<in> {i..<j}. acc step accept rho q (i, l)}"
  from assms(2) have L'_props: "finite L'" "L' \<noteq> {}" "Max L' = i"
    by (auto simp add: sup_acc_def L'_def Let_def split: if_splits)
  have i_lt_l: "i < l"
    using l_def(1) by auto
  from l_def have "l \<in> L'"
    unfolding L'_def acc_def using steps_split[OF i_lt_l, of step rho] by auto
  then show "False"
    using l_def(1) L'_props Max_ge i_lt_l not_le by auto
qed

lemma sup_acc_l: "i < j \<Longrightarrow> i \<noteq> l \<Longrightarrow> sup_acc step accept rho q i j = Some (ts, l) \<Longrightarrow>
  sup_acc step accept rho q i j = sup_acc step accept rho (step q (bs_at rho i)) (i + 1) j"
proof -
  assume assms: "i < j" "i \<noteq> l" "sup_acc step accept rho q i j = Some (ts, l)"
  define L where "L = {l \<in> {i..<j}. acc step accept rho q (i, l)}"
  define L' where "L' = {l \<in> {Suc i..<j}. acc step accept rho (step q (bs_at rho i)) (i + 1, l)}"
  from assms(3) have L_props: "finite L" "L \<noteq> {}" "l = Max L"
    "sup_acc step accept rho q i j = Some (ts_at rho l, l)"
    by (auto simp add: sup_acc_def L_def Let_def split: if_splits)
  have l_in_L: "l \<in> L"
    using Max_in[OF L_props(1,2)] L_props(3) by auto
  then have i_lt_l: "i < l"
    unfolding L_def using assms(2) by auto
  have l_in_L': "finite L'" "L' \<noteq> {}" "l \<in> L'"
    using steps_split[OF i_lt_l, of step rho q] l_in_L assms(2)
    unfolding L_def L'_def acc_def by auto
  have "\<And>l'. l' \<in> L' \<Longrightarrow> l' \<le> l"
  proof -
    fix l'
    assume assms: "l' \<in> L'"
    have i_lt_l': "i < l'"
      using assms unfolding L'_def by auto
    have "l' \<in> L"
      using steps_split[OF i_lt_l', of step rho] assms unfolding L_def L'_def acc_def by auto
    then show "l' \<le> l"
      using L_props by simp
  qed
  then have l_sup_L': "Max L' = l"
    using Max_eq_iff[OF l_in_L'(1,2)] l_in_L'(3) by auto
  then show "sup_acc step accept rho q i j =
    sup_acc step accept rho (step q (bs_at rho i)) (i + 1) j"
    unfolding L_props(4)
    unfolding sup_acc_def Let_def
    using L'_def l_in_L'(2,3) L_props
    unfolding Suc_eq_plus1 by auto
qed

lemma sup_leadsto_idle: "i < j \<Longrightarrow> steps step rho init (i, j) \<noteq> q \<Longrightarrow>
  sup_leadsto init step rho i j q = sup_leadsto init step rho (i + 1) j q"
proof -
  assume assms: "i < j" "steps step rho init (i, j) \<noteq> q"
  define L where "L = {l. l < i \<and> steps step rho init (l, j) = q}"
  define L' where "L' = {l. l < i + 1 \<and> steps step rho init (l, j) = q}"
  have L_L': "L = L'"
    unfolding L_def L'_def using assms(2) less_antisym
    by fastforce
  show "sup_leadsto init step rho i j q = sup_leadsto init step rho (i + 1) j q"
    using L_def L'_def L_L'
    by (auto simp add: sup_leadsto_def)
qed

lemma sup_leadsto_SomeI: "l < i \<Longrightarrow> steps step rho init (l, j) = q \<Longrightarrow>
  \<exists>l'. sup_leadsto init step rho i j q = Some (ts_at rho l') \<and> l \<le> l' \<and> l' < i"
proof -
  assume assms: "l < i" "steps step rho init (l, j) = q"
  define L' where "L' = {l. l < i \<and> steps step rho init (l, j) = q}"
  have fin_L': "finite L'"
    unfolding L'_def by auto
  moreover have L_nonempty: "L' \<noteq> {}"
    using assms unfolding L'_def
    by (auto simp add: sup_leadsto_def split: if_splits)
  ultimately have "Max L' \<in> L'"
    using Max_in by auto
  then show "\<exists>l'. sup_leadsto init step rho i j q = Some (ts_at rho l') \<and> l \<le> l' \<and> l' < i"
    using L'_def L_nonempty assms
    by (fastforce simp add: sup_leadsto_def split: if_splits)
qed

lemma sup_leadsto_SomeE: "i \<le> j \<Longrightarrow> sup_leadsto init step rho i j q = Some ts \<Longrightarrow>
  \<exists>l < i. steps step rho init (l, j) = q \<and> ts_at rho l = ts"
proof -
  assume assms: "i \<le> j" "sup_leadsto init step rho i j q = Some ts"
  define L' where "L' = {l. l < i \<and> steps step rho init (l, j) = q}"
  have fin_L': "finite L'"
    unfolding L'_def by auto
  moreover have L_nonempty: "L' \<noteq> {}"
    using assms(2) unfolding L'_def
    by (auto simp add: sup_leadsto_def split: if_splits)
  ultimately have "Max L' \<in> L'"
    using Max_in by auto
  then show "\<exists>l < i. steps step rho init (l, j) = q \<and> ts_at rho l = ts"
    using assms(2) L'_def
    apply (auto simp add: sup_leadsto_def split: if_splits)
    using \<open>Max L' \<in> L'\<close> by blast
qed

lemma Mapping_keys_dest: "x \<in> mmap_keys f \<Longrightarrow> \<exists>y. mmap_lookup f x = Some y"
  by (auto simp add: mmap_keys_def mmap_lookup_def weak_map_of_SomeI)

lemma Mapping_keys_intro: "mmap_lookup f x \<noteq> None \<Longrightarrow> x \<in> mmap_keys f"
  by (auto simp add: mmap_keys_def mmap_lookup_def)
     (metis map_of_eq_None_iff option.distinct(1))

lemma Mapping_not_keys_intro: "mmap_lookup f x = None \<Longrightarrow> x \<notin> mmap_keys f"
  unfolding mmap_lookup_def mmap_keys_def
  using weak_map_of_SomeI by force

lemma Mapping_lookup_None_intro: "x \<notin> mmap_keys f \<Longrightarrow> mmap_lookup f x = None"
  unfolding mmap_lookup_def mmap_keys_def
  by (simp add: map_of_eq_None_iff)

primrec mmap_combine :: "'key \<Rightarrow> 'val \<Rightarrow> ('val \<Rightarrow> 'val \<Rightarrow> 'val) \<Rightarrow> ('key \<times> 'val) list \<Rightarrow>
  ('key \<times> 'val) list"
  where
  "mmap_combine k v c [] = [(k, v)]"
| "mmap_combine k v c (p # ps) = (case p of (k', v') \<Rightarrow>
    if k = k' then (k, c v' v) # ps else p # mmap_combine k v c ps)"

lemma mmap_combine_distinct_set: "distinct (map fst r) \<Longrightarrow>
  distinct (map fst (mmap_combine k v c r)) \<and>
  set (map fst (mmap_combine k v c r)) = set (map fst r) \<union> {k}"
  by (induction r) force+

lemma mmap_combine_lookup: "distinct (map fst r) \<Longrightarrow> mmap_lookup (mmap_combine k v c r) z =
  (if k = z then (case mmap_lookup r k of None \<Rightarrow> Some v | Some v' \<Rightarrow> Some (c v' v))
  else mmap_lookup r z)"
  using eq_key_imp_eq_value
  by (induction r) (fastforce simp: mmap_lookup_def split: option.splits)+

definition mmap_fold :: "('c, 'd) mmap \<Rightarrow> (('c \<times> 'd) \<Rightarrow> ('c \<times> 'd)) \<Rightarrow> ('d \<Rightarrow> 'd \<Rightarrow> 'd) \<Rightarrow>
  ('c, 'd) mmap" where
  "mmap_fold m f c = foldl (\<lambda>r p. case f p of (k, v) \<Rightarrow> mmap_combine k v c r) [] m"

lemma foldl_mmap_combine_distinct_set: "distinct (map fst r) \<Longrightarrow>
  distinct (map fst (foldl (\<lambda>r p. case f p of (k, v) \<Rightarrow> mmap_combine k v c r) r m)) \<and>
  set (map fst (foldl (\<lambda>r p. case f p of (k, v) \<Rightarrow> mmap_combine k v c r) r m)) =
    set (map fst r) \<union> set (map (fst \<circ> f) m)"
  apply (induction m arbitrary: r)
  using mmap_combine_distinct_set
   apply (auto split: prod.splits)
      apply force
     apply (smt Un_iff fst_conv imageI insert_iff)
  using mk_disjoint_insert
    apply fastforce+
  done

lemma foldl_mmap_combine_lookup: "distinct (map fst r) \<Longrightarrow>
  mmap_lookup (foldl (\<lambda>r p. case f p of (k, v) \<Rightarrow> mmap_combine k v c r) r m) z =
  (case map (snd \<circ> f) (filter (\<lambda>(k, v). fst (f (k, v)) = z) m) of [] \<Rightarrow> mmap_lookup r z
  | v # vs \<Rightarrow> (case mmap_lookup r z of None \<Rightarrow> Some (foldl c v vs)
    | Some w \<Rightarrow> Some (foldl c w (v # vs))))"
proof (induction m arbitrary: r)
  case (Cons p ps)
  obtain k v where kv_def: "f p = (k, v)"
    by fastforce
  have distinct: "distinct (map fst (mmap_combine k v c r))"
    using mmap_combine_distinct_set[OF Cons(2)]
    by auto
  show ?case
    using Cons(1)[OF distinct, unfolded mmap_combine_lookup[OF Cons(2)]]
    by (auto simp: mmap_lookup_def kv_def split: list.splits option.splits)
qed auto

lemma mmap_fold_distinct: "distinct (map fst m) \<Longrightarrow> distinct (map fst (mmap_fold m f c))"
  using foldl_mmap_combine_distinct_set[of "[]"]
  by (auto simp: mmap_fold_def)

lemma mmap_fold_set: "distinct (map fst m) \<Longrightarrow> set (map fst (mmap_fold m f c)) = (fst \<circ> f) ` set m"
  using foldl_mmap_combine_distinct_set[of "[]"]
  by (force simp: mmap_fold_def)

lemma mmap_lookup_empty: "mmap_lookup [] z = None"
  by (auto simp: mmap_lookup_def)

lemma mmap_fold_lookup: "distinct (map fst m) \<Longrightarrow> mmap_lookup (mmap_fold m f c) z =
  (case map (snd \<circ> f) (filter (\<lambda>(k, v). fst (f (k, v)) = z) m) of [] \<Rightarrow> None
  | v # vs \<Rightarrow> Some (foldl c v vs))"
  using foldl_mmap_combine_lookup[of "[]" c f]
  by (auto simp: mmap_fold_def mmap_lookup_empty split: list.splits)

definition fold_sup :: "('c, 'd :: timestamp) mmap \<Rightarrow> ('c \<Rightarrow> 'c) \<Rightarrow> ('c, 'd) mmap" where
  "fold_sup m f = mmap_fold m (\<lambda>(x, y). (f x, y)) sup"

lemma mmap_lookup_distinct: "distinct (map fst m) \<Longrightarrow> (k, v) \<in> set m \<Longrightarrow>
  mmap_lookup m k = Some v"
  by (auto simp: mmap_lookup_def)

lemma fold_sup_distinct: "distinct (map fst m) \<Longrightarrow> distinct (map fst (fold_sup m f))"
  using mmap_fold_distinct
  by (auto simp: fold_sup_def)

lemma fold_sup:
  fixes v :: "'d :: timestamp"
  shows "foldl sup v vs = fold sup vs v"
  by (induction vs arbitrary: v) (auto simp: sup.commute)

lemma lookup_fold_sup:
  assumes distinct: "distinct (map fst m)"
  shows "mmap_lookup (fold_sup m f) z =
    (let Z = {x \<in> mmap_keys m. f x = z} in
    if Z = {} then None else Some (Sup_fin ((the \<circ> mmap_lookup m) ` Z)))"
proof (cases "{x \<in> mmap_keys m. f x = z} = {}")
  case True
  have "z \<notin> mmap_keys (mmap_fold m (\<lambda>(x, y). (f x, y)) sup)"
    using True[unfolded mmap_keys_def] mmap_fold_set[OF distinct]
    by (auto simp: mmap_keys_def)
  then have "mmap_lookup (fold_sup m f) z = None"
    unfolding fold_sup_def
    by (meson Mapping_keys_intro)
  then show ?thesis
    unfolding True
    by auto
next
  case False
  have z_in_keys: "z \<in> mmap_keys (mmap_fold m (\<lambda>(x, y). (f x, y)) sup)"
    using False[unfolded mmap_keys_def] mmap_fold_set[OF distinct]
    by (force simp: mmap_keys_def)
  obtain v vs where vs_def: "mmap_lookup (fold_sup m f) z = Some (foldl sup v vs)"
    "v # vs = map snd (filter (\<lambda>(k, v). f k = z) m)"
    using mmap_fold_lookup[OF distinct, of "(\<lambda>(x, y). (f x, y))" sup z]
      Mapping_keys_dest[OF z_in_keys]
    by (force simp: fold_sup_def mmap_keys_def comp_def split: list.splits prod.splits)
  have "set (v # vs) = (the \<circ> mmap_lookup m) ` {x \<in> mmap_keys m. f x = z}"
  proof (rule set_eqI, rule iffI)
    fix w
    assume "w \<in> set (v # vs)"
    then obtain x where x_def: "x \<in> mmap_keys m" "f x = z" "(x, w) \<in> set m"
      using vs_def(2)
      by (auto simp add: mmap_keys_def rev_image_eqI)
    show "w \<in> (the \<circ> mmap_lookup m) ` {x \<in> mmap_keys m. f x = z}"
      using x_def(1,2) mmap_lookup_distinct[OF distinct x_def(3)]
      by force
  next
    fix w
    assume "w \<in> (the \<circ> mmap_lookup m) ` {x \<in> mmap_keys m. f x = z}"
    then obtain x where x_def: "x \<in> mmap_keys m" "f x = z" "(x, w) \<in> set m"
      using mmap_lookup_distinct[OF distinct]
      by (auto simp add: Mapping_keys_intro distinct mmap_lookup_def dest: Mapping_keys_dest)
    show "w \<in> set (v # vs)"
      using x_def
      by (force simp: vs_def(2))
  qed
  then have "foldl sup v vs = Sup_fin ((the \<circ> mmap_lookup m) ` {x \<in> mmap_keys m. f x = z})"
    unfolding fold_sup
    by (metis Sup_fin.set_eq_fold)
  then show ?thesis
    using False
    by (auto simp: vs_def(1))
qed

definition mmap_map :: "('a \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> ('a, 'b) mmap \<Rightarrow> ('a, 'c) mmap" where
  "mmap_map f m = map (\<lambda>(k, v). (k, f k v)) m"

lemma mmap_map_keys: "mmap_keys (mmap_map f m) = mmap_keys m"
  by (force simp: mmap_map_def mmap_keys_def)

lemma mmap_map_fst: "map fst (mmap_map f m) = map fst m"
  by (auto simp: mmap_map_def)

definition adv_end :: "('b, 'c, 'd :: timestamp, 't, 'e) args \<Rightarrow>
  ('c, 'd, 't, 'e) window \<Rightarrow> ('c, 'd, 't, 'e) window" where
  "adv_end args w = (let init = w_init args; step = w_step args; accept = w_accept args;
    run_t = w_run_t args; run_sub = w_run_sub args;
    i = w_i w; ti = w_ti w; si = w_si w; j = w_j w; tj = w_tj w; sj = w_sj w;
    s = w_s w; e = w_e w in
    (case run_t tj of Some (tj', t) \<Rightarrow> (case run_sub sj of Some (sj', bs) \<Rightarrow>
      let s' = mmap_map (\<lambda>q (q', tstp).
        (step q' bs, if accept q' bs then Some (t, j) else tstp)) s;
      e' = fold_sup e (\<lambda>q. step q bs) in
      w\<lparr>w_j := Suc j, w_tj := tj', w_sj := sj', w_s := s', w_e := e'\<rparr>)))"

lemma map_values_lookup: "mmap_lookup (mmap_map f m) z = Some v' \<Longrightarrow>
  \<exists>v. mmap_lookup m z = Some v \<and> v' = f z v"
  by (induction m) (auto simp: mmap_lookup_def mmap_map_def)

lemma acc_app:
  assumes "i \<le> j" "steps step rho q (i, j) = q'" "accept q' (bs_at rho j)"
  shows "sup_acc step accept rho q i (j + 1) = Some (ts_at rho j, j)"
proof -
  define L where "L = {l \<in> {i..<Suc j}. accept (steps step rho q (i, l)) (bs_at rho l)}"
  have nonempty: "finite L" "L \<noteq> {}"
    using assms unfolding L_def by auto
  moreover have "Max {l \<in> {i..<Suc j}. accept (steps step rho q (i, l)) (bs_at rho l)} = j"
    unfolding L_def[symmetric] Max_eq_iff[OF nonempty, of j]
    unfolding L_def using assms by auto
  ultimately show ?thesis
    by (auto simp add: sup_acc_def acc_def L_def)
qed

lemma acc_app_idle:
  assumes "i \<le> j" "steps step rho q (i, j) = q'" "\<not>accept q' (bs_at rho j)"
  shows "sup_acc step accept rho q i (j + 1) = sup_acc step accept rho q i j"
  using assms
  apply (auto simp add: sup_acc_def Let_def acc_def elim: less_SucE)
  by (metis less_Suc_eq)+

lemma valid_adv_end:
  assumes "valid_window args t0 sub rho w" "w_run_t args (w_tj w) = Some (tj', t)"
    "w_run_sub args (w_sj w) = Some (sj', bs)"
    "\<And>t'. t' \<in> set (map fst rho) \<Longrightarrow> t' \<le> t"
  shows "valid_window args t0 sub (rho @ [(t, bs)]) (adv_end args w)"
proof -
  define init where "init = w_init args"
  define step where "step = w_step args"
  define accept where "accept = w_accept args"
  define run_t where "run_t = w_run_t args"
  define run_sub where "run_sub = w_run_sub args"
  define i where "i = w_i w"
  define ti where "ti = w_ti w"
  define si where "si = w_si w"
  define j where "j = w_j w"
  define tj where "tj = w_tj w"
  define sj where "sj = w_sj w"
  define s where "s = w_s w"
  define e where "e = w_e w"
  have valid_before: "reach_window args t0 sub rho (i, ti, si, j, tj, sj)"
    "\<And>i j. i \<le> j \<Longrightarrow> j < length rho \<Longrightarrow> ts_at rho i \<le> ts_at rho j"
    "\<forall>q. mmap_lookup e q = sup_leadsto init step rho i j q" "distinct (map fst e)"
    "valid_s init step accept rho i i j s"
    using assms(1)
    by (auto simp: valid_window_def Let_def init_def step_def accept_def run_t_def
        run_sub_def i_def ti_def si_def j_def tj_def sj_def s_def e_def)
  have i_j: "i \<le> j"
    using valid_before(1)
    by auto
  have distinct_before: "distinct (map fst s)" "distinct (map fst e)"
    using valid_before
    by (auto simp: valid_s_def)
  note run_tj = assms(2)[folded run_t_def tj_def]
  note run_sj = assms(3)[folded run_sub_def sj_def]
  define rho' where "rho' = rho @ [(t, bs)]"
  have ts_at_mono: "\<And>i j. i \<le> j \<Longrightarrow> j < length rho' \<Longrightarrow> ts_at rho' i \<le> ts_at rho' j"
    using valid_before(2) assms(4)
    by (auto simp: rho'_def ts_at_def nth_append split: option.splits list.splits if_splits)
  define s' where "s' = mmap_map (\<lambda>q (q', tstp).
    (step q' bs, if accept q' bs then Some (t, j) else tstp)) s"
  define e' where "e' = fold_sup e (\<lambda>q. step q bs)"
  have adv_end: "adv_end args w = w\<lparr>w_j := Suc j, w_tj := tj', w_sj := sj',
    w_s := s', w_e := e'\<rparr>"
    using run_tj run_sj
    unfolding adv_end_def init_def step_def accept_def run_t_def run_sub_def
      i_def ti_def si_def j_def tj_def sj_def s_def e_def s'_def e'_def
    by (auto simp: Let_def)
  have keys_s': "mmap_keys s' = mmap_keys s"
    by (force simp: mmap_keys_def mmap_map_def s'_def)
  have lookup_s: "\<And>q q' tstp. mmap_lookup s q = Some (q', tstp) \<Longrightarrow>
  steps step rho' q (i, j) = q' \<and> tstp = sup_acc step accept rho' q i j"
    using valid_before Mapping_keys_intro
    by (force simp add: Let_def rho'_def valid_s_def steps_app_cong sup_acc_app_cong
        split: option.splits)
  have bs_at_rho'_j: "bs_at rho' j = bs"
    using valid_before
    by (auto simp: rho'_def bs_at_def nth_append)
  have ts_at_rho'_j: "ts_at rho' j = t"
    using valid_before
    by (auto simp: rho'_def ts_at_def nth_append)
  have lookup_s': "\<And>q q' tstp. mmap_lookup s' q = Some (q', tstp) \<Longrightarrow>
  steps step rho' q (i, j + 1) = q' \<and> tstp = sup_acc step accept rho' q i (j + 1)"
  proof -
    fix q q'' tstp'
    assume assm: "mmap_lookup s' q = Some (q'', tstp')"
    obtain q' tstp where "mmap_lookup s q = Some (q', tstp)" "q'' = step q' bs"
      "tstp' = (if accept q' bs then Some (t, j) else tstp)"
      using map_values_lookup[OF assm[unfolded s'_def]] by auto
    then show "steps step rho' q (i, j + 1) = q'' \<and> tstp' = sup_acc step accept rho' q i (j + 1)"
      using lookup_s
      apply (auto simp: bs_at_rho'_j ts_at_rho'_j)
         apply (metis Suc_eq_plus1 bs_at_rho'_j i_j steps_app)
        apply (metis Suc_eq_plus1 acc_app bs_at_rho'_j i_j ts_at_rho'_j)
       apply (metis Suc_eq_plus1 bs_at_rho'_j i_j steps_app)
      apply (metis Suc_eq_plus1 acc_app_idle bs_at_rho'_j i_j)
      done
  qed
  have lookup_e: "\<And>q. mmap_lookup e q = sup_leadsto init step rho' i j q"
    using valid_before sup_leadsto_app_cong[of _ _ rho init step]
    by (auto simp: rho'_def)
  have keys_e_alt: "mmap_keys e = {q. \<exists>l < i. steps step rho' init (l, j) = q}"
    using valid_before
    apply (auto simp add: sup_leadsto_def rho'_def)
     apply (metis (no_types, lifting) Mapping_keys_dest lookup_e rho'_def sup_leadsto_SomeE)
    apply (metis (no_types, lifting) Mapping_keys_intro option.simps(3) order_refl steps_app_cong)
    done
  have finite_keys_e: "finite (mmap_keys e)"
    unfolding keys_e_alt
    by (rule finite_surj[of "{l. l < i}"]) auto
  have "reach_sub run_sub sub (map snd rho) sj"
    using valid_before reach_sub_trans
    unfolding run_sub_def sub_def
    by fastforce
  then have reach_sub': "reach_sub run_sub sub (map snd rho @ [bs]) sj'"
    using reach_sub_app run_sj
    by fast
  have "reach_sub run_t t0 (map fst rho) tj"
    using valid_before reach_sub_trans
    unfolding run_t_def
    by fastforce
  then have reach_t': "reach_sub run_t t0 (map fst rho') tj'"
    using reach_sub_app run_tj
    unfolding rho'_def
    by fastforce
  have lookup_e': "\<And>q. mmap_lookup e' q = sup_leadsto init step rho' i (Suc j) q"
  proof -
    fix q
    define Z where "Z = {x \<in> mmap_keys e. step x bs = q}"
    show "mmap_lookup e' q = sup_leadsto init step rho' i (Suc j) q"
    proof (cases "Z = {}")
      case True
      then have "mmap_lookup e' q = None"
        using Z_def lookup_fold_sup[OF distinct_before(2)]
        unfolding e'_def
        by (auto simp: Let_def)
      moreover have "sup_leadsto init step rho' i (Suc j) q = None"
      proof (rule ccontr)
        assume assm: "sup_leadsto init step rho' i (Suc j) q \<noteq> None"
        obtain l where l_def: "l < i" "steps step rho' init (l, Suc j) = q"
          using i_j sup_leadsto_SomeE[of i "Suc j"] assm
          by force
        have l_j: "l \<le> j"
          using less_le_trans[OF l_def(1) i_j] by auto
        obtain q'' where q''_def: "steps step rho' init (l, j) = q''" "step q'' bs = q"
          using steps_appE[OF _ l_def(2)] l_j
          by (auto simp: bs_at_rho'_j)
        then have "q'' \<in> mmap_keys e"
          using keys_e_alt l_def(1)
          by auto
        then show "False"
          using Z_def q''_def(2) True
          by auto
      qed
      ultimately show ?thesis
        by auto
    next
      case False
      then have lookup_e': "mmap_lookup e' q = Some (Sup_fin ((the \<circ> mmap_lookup e) ` Z))"
        using Z_def lookup_fold_sup[OF distinct_before(2)]
        unfolding e'_def
        by (auto simp: Let_def)
      define L where "L = {l. l < i \<and> steps step rho' init (l, Suc j) = q}"
      have fin_L: "finite L"
        unfolding L_def by auto
      have Z_alt: "Z = {x. \<exists>l < i. steps step rho' init (l, j) = x \<and> step x bs = q}"
        using Z_def[unfolded keys_e_alt] by auto
      have fin_Z: "finite Z"
        unfolding Z_alt by auto
      have L_nonempty: "L \<noteq> {}"
        using L_def Z_alt False i_j steps_app[of _ _ step rho q]
        by (auto simp: bs_at_rho'_j)
          (smt Suc_eq_plus1 bs_at_rho'_j less_irrefl_nat less_le_trans nat_le_linear steps_app)
      have sup_leadsto: "sup_leadsto init step rho' i (Suc j) q = Some (ts_at rho' (Max L))"
        using L_nonempty L_def
        by (auto simp add: sup_leadsto_def)
      have j_lt_rho': "j < length rho'"
        using valid_before
        by (auto simp: rho'_def)
      have "Sup_fin ((the \<circ> mmap_lookup e) ` Z) = ts_at rho' (Max L)"
      proof (rule antisym)
        obtain z ts where zts_def: "z \<in> Z" "(the \<circ> mmap_lookup e) z = ts"
          "Sup_fin ((the \<circ> mmap_lookup e) ` Z) = ts"
        proof -
          assume lassm: "\<And>z ts. z \<in> Z \<Longrightarrow> (the \<circ> mmap_lookup e) z = ts \<Longrightarrow>
          \<Squnion>\<^sub>f\<^sub>i\<^sub>n ((the \<circ> mmap_lookup e) ` Z) = ts \<Longrightarrow> thesis"
          define T where "T = (the \<circ> mmap_lookup e) ` Z"
          have T_sub: "T \<subseteq> ts_at rho' ` {..j}"
            using lookup_e keys_e_alt i_j
            by (auto simp add: T_def Z_def sup_leadsto_def)
          have "finite T" "T \<noteq> {}"
            using fin_Z False
            by (auto simp add: T_def)
          then have sup_in: "\<Squnion>\<^sub>f\<^sub>i\<^sub>n T \<in> T"
          proof (rule sup_fin_closed)
            fix x y
            assume xy: "x \<in> T" "y \<in> T"
            then obtain a c where "x = ts_at rho' a" "y = ts_at rho' c" "a \<le> j" "c \<le> j"
              using T_sub
              by (meson atMost_iff imageE subsetD)
            then show "sup x y \<in> {x, y}"
              using ts_at_mono j_lt_rho'
              by (cases "a \<le> c") (auto simp add: sup.absorb1 sup.absorb2)
          qed
          then show ?thesis
            using lassm
            by (auto simp add: T_def)
        qed
        from zts_def(2) have lookup_e_z: "mmap_lookup e z = Some ts"
          using zts_def(1) Z_def by (auto dest: Mapping_keys_dest)
        have "sup_leadsto init step rho' i j z = Some ts"
          using lookup_e_z lookup_e
          by auto
        then obtain l where l_def: "l < i" "steps step rho' init (l, j) = z" "ts_at rho' l = ts"
          using sup_leadsto_SomeE[OF i_j]
          by (fastforce simp: rho'_def ts_at_def nth_append)
        have l_j: "l \<le> j"
          using less_le_trans[OF l_def(1) i_j] by auto
        have "l \<in> L"
          unfolding L_def using l_def zts_def(1) Z_alt
          by auto (metis (no_types, lifting) Suc_eq_plus1 bs_at_rho'_j l_j steps_app)
        then have "l \<le> Max L" "Max L < i"
          using L_nonempty fin_L
          by (auto simp add: L_def)
        then show "Sup_fin ((the \<circ> mmap_lookup e) ` Z) \<le> ts_at rho' (Max L)"
          unfolding zts_def(3) l_def(3)[symmetric]
          using ts_at_mono i_j j_lt_rho'
          by (auto simp: rho'_def)
      next
        obtain l where l_def: "Max L = l" "l < i" "steps step rho' init (l, Suc j) = q"
          using Max_in[OF fin_L L_nonempty] L_def by auto
        obtain z where z_def: "steps step rho' init (l, j) = z" "step z bs = q"
          using l_def(2,3) i_j bs_at_rho'_j
          by (metis less_imp_le_nat less_le_trans steps_appE)
        have z_in_Z: "z \<in> Z"
          unfolding Z_alt
          using l_def(2) z_def i_j
          by fastforce
        have lookup_e_z: "mmap_lookup e z = sup_leadsto init step rho' i j z"
          using lookup_e z_in_Z Z_alt
          by auto
        obtain l' where l'_def: "sup_leadsto init step rho' i j z = Some (ts_at rho' l')"
          "l \<le> l'" "l' < i"
          using sup_leadsto_SomeI[OF l_def(2) z_def(1)] by auto
        have "ts_at rho' l' \<in> (the \<circ> mmap_lookup e) ` Z"
          using lookup_e_z l'_def(1) z_in_Z
          by force
        then have "ts_at rho' l' \<le> Sup_fin ((the \<circ> mmap_lookup e) ` Z)"
          using Inf_fin_le_Sup_fin fin_Z z_in_Z
          by (simp add: Sup_fin.coboundedI)
        then show "ts_at rho' (Max L) \<le> Sup_fin ((the \<circ> mmap_lookup e) ` Z)"
        unfolding l_def(1)
        using ts_at_mono l'_def(2,3) i_j j_lt_rho'
        by (fastforce simp: rho'_def)
      qed
      then show ?thesis
        unfolding lookup_e' sup_leadsto by auto
    qed
  qed
  have "distinct (map fst s')" "distinct (map fst e')"
    using distinct_before mmap_fold_distinct
    unfolding s'_def mmap_map_fst e'_def fold_sup_def
    by auto
  moreover have "mmap_keys s' = {q. \<exists>l\<le>i. steps step rho' init (l, i) = q}"
    unfolding keys_s' rho'_def
    using valid_before(1,5) valid_s_def[of init step accept rho i i j s]
    by (auto simp: steps_app_cong[of _ rho step])
  moreover have "reach_sub run_t ti (drop i (map fst rho')) tj'"
    "reach_sub run_sub si (drop i (map snd rho')) sj'"
    using valid_before reach_sub_app run_tj run_sj
    by (auto simp: rho'_def run_t_def run_sub_def)
  ultimately show "valid_window args t0 sub (rho @ [(t, bs)]) (adv_end args w)"
    unfolding adv_end
    using valid_before lookup_e' lookup_s' ts_at_mono
    by (auto simp: valid_window_def Let_def init_def step_def accept_def run_t_def
        run_sub_def i_def ti_def si_def j_def tj_def sj_def s_def e_def
        rho'_def valid_s_def intro!: exI[of _ rho'] split: option.splits)
qed

lemma adv_end_bounds:
  assumes "w_run_t args (w_tj w) = Some (tj', t)"
    "w_run_sub args (w_sj w) = Some (sj', bs)"
  shows "w_i (adv_end args w) = w_i w" "w_ti (adv_end args w) = w_ti w"
    "w_si (adv_end args w) = w_si w" "w_j (adv_end args w) = Suc (w_j w)"
    "w_tj (adv_end args w) = tj'" "w_sj (adv_end args w) = sj'"
  using assms
  by (auto simp: adv_end_def Let_def)

definition drop_cur :: "nat \<Rightarrow> ('c \<times> ('d \<times> nat) option) \<Rightarrow> ('c \<times> ('d \<times> nat) option)" where
  "drop_cur i = (\<lambda>(q', tstp). (q', case tstp of Some (ts, tp) \<Rightarrow>
    if tp = i then None else tstp | None \<Rightarrow> tstp))"

definition adv_d :: "('c \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> nat \<Rightarrow> 'b \<Rightarrow> ('c, 'c \<times> ('d \<times> nat) option) mmap \<Rightarrow>
  ('c, 'c \<times> ('d \<times> nat) option) mmap" where
  "adv_d step i b s = mmap_fold s (\<lambda>(x, v). (step x b, drop_cur i v)) (\<lambda>x y. x)"

definition keys_idem :: "('c \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> nat \<Rightarrow> 'b \<Rightarrow>
  ('c, 'c \<times> ('d \<times> nat) option) mmap \<Rightarrow> bool" where
  "keys_idem step i b s = (\<forall>x \<in> mmap_keys s. \<forall>x' \<in> mmap_keys s.
    step x b = step x' b \<longrightarrow> drop_cur i (the (mmap_lookup s x)) =
    drop_cur i (the (mmap_lookup s x')))"

lemma adv_d_keys:
  assumes "distinct (map fst s)"
  shows "mmap_keys (adv_d step i b s) = (\<lambda>q. step q b) ` (mmap_keys s)"
  unfolding adv_d_def mmap_keys_def
  using mmap_fold_set[OF assms]
  by force

lemma lookup_adv_d_None:
  assumes distinct: "distinct (map fst s)"
    and Z_empty: "{x \<in> mmap_keys s. step x b = z} = {}"
  shows "mmap_lookup (adv_d step i b s) z = None"
proof -
  have "z \<notin> mmap_keys (mmap_fold s (\<lambda>(x, v). (step x b, drop_cur i v)) (\<lambda>x y. x))"
    using Z_empty[unfolded mmap_keys_def] mmap_fold_set[OF distinct]
    by (auto simp: mmap_keys_def)
  then have "mmap_lookup (adv_d step i b s) z = None"
    unfolding adv_d_def
    by (simp add: Mapping_lookup_None_intro)
  then show ?thesis
    unfolding Z_empty
    by auto
qed

lemma lookup_adv_d_Some:
  assumes distinct: "distinct (map fst s)" and idem: "keys_idem step i b s"
    and wit: "x \<in> mmap_keys s" "step x b = z"
  shows "mmap_lookup (adv_d step i b s) z = Some (drop_cur i (the (mmap_lookup s x)))"
proof -
  have z_in_keys: "z \<in> mmap_keys (mmap_fold s (\<lambda>(x, v). (step x b, drop_cur i v)) (\<lambda>x y. x))"
    using wit(1,2)[unfolded mmap_keys_def] mmap_fold_set[OF distinct]
    by (force simp: mmap_keys_def)
  obtain v vs where vs_def: "mmap_lookup (adv_d step i b s) z = Some (foldl (\<lambda>x y. x) v vs)"
    "v # vs = map (\<lambda>(x, v). drop_cur i v) (filter (\<lambda>(k, v). step k b = z) s)"
    unfolding adv_d_def
    using mmap_fold_lookup[OF distinct, of "(\<lambda>(x, v). (step x b, drop_cur i v))" "\<lambda>x y. x" z]
      Mapping_keys_dest[OF z_in_keys]
    by (force simp: adv_d_def mmap_keys_def split: list.splits)
  have "set (v # vs) = drop_cur i ` (the \<circ> mmap_lookup s) ` {x \<in> mmap_keys s. step x b = z}"
  proof (rule set_eqI, rule iffI)
    fix w
    assume "w \<in> set (v # vs)"
    then obtain x y where xy_def: "x \<in> mmap_keys s" "step x b = z" "(x, y) \<in> set s"
      "w = drop_cur i y"
      using vs_def(2)
      by (auto simp add: mmap_keys_def rev_image_eqI)
    show "w \<in> drop_cur i ` (the \<circ> mmap_lookup s) ` {x \<in> mmap_keys s. step x b = z}"
      using xy_def(1,2,4) mmap_lookup_distinct[OF distinct xy_def(3)]
      by force
  next
    fix w
    assume "w \<in> drop_cur i ` (the \<circ> mmap_lookup s) ` {x \<in> mmap_keys s. step x b = z}"
    then obtain x y where xy_def: "x \<in> mmap_keys s" "step x b = z" "(x, y) \<in> set s"
      "w = drop_cur i y"
      using mmap_lookup_distinct[OF distinct]
      by (auto simp add: Mapping_keys_intro distinct mmap_lookup_def dest: Mapping_keys_dest)
    show "w \<in> set (v # vs)"
      using xy_def
      by (force simp: vs_def(2))
  qed
  then have "foldl (\<lambda>x y. x) v vs = drop_cur i (the (mmap_lookup s x))"
    using wit
    apply (induction vs arbitrary: v)
     apply (auto)
     apply (smt empty_is_image idem imageE insert_not_empty keys_idem_def mem_Collect_eq
        the_elem_eq the_elem_image_unique)
    apply (smt Collect_cong idem imageE insert_compr keys_idem_def mem_Collect_eq)
    done
  then show ?thesis
    using wit
    by (auto simp: vs_def(1))
qed

definition "loop_cond j = (\<lambda>(i, ti, si, q, s, tstp). i < j \<and> q \<notin> mmap_keys s)"
definition "loop_body step accept run_t run_sub =
  (\<lambda>(i, ti, si, q, s, tstp). case run_t ti of Some (ti', t) \<Rightarrow> case run_sub si of Some (si', b) \<Rightarrow>
  (Suc i, ti', si', step q b, adv_d step i b s, if accept q b then Some (t, i) else tstp))"
definition "loop_inv init step accept args t0 sub rho u j tj sj =
  (\<lambda>(i, ti, si, q, s, tstp). u + 1 \<le> i \<and>
  reach_window args t0 sub rho (i, ti, si, j, tj, sj) \<and>
  steps step rho init (u + 1, i) = q \<and>
  valid_s init step accept rho u i j s \<and> tstp = sup_acc step accept rho init (u + 1) i)"

definition mmap_update :: "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b) mmap \<Rightarrow> ('a, 'b) mmap" where
  "mmap_update = AList.update"

lemma mmap_update_distinct: "distinct (map fst m) \<Longrightarrow> distinct (map fst (mmap_update k v m))"
  by (auto simp: mmap_update_def distinct_update)

definition adv_start :: "('b, 'c, 'd :: timestamp, 't, 'e) args \<Rightarrow>
  ('c, 'd, 't, 'e) window \<Rightarrow> ('c, 'd, 't, 'e) window" where
  "adv_start args w = (let init = w_init args; step = w_step args; accept = w_accept args;
    run_t = w_run_t args; run_sub = w_run_sub args;
    i = w_i w; ti = w_ti w; si = w_si w; j = w_j w; tj = w_tj w; sj = w_sj w;
    s = w_s w; e = w_e w in
    (case run_t ti of Some (ti', t) \<Rightarrow> (case run_sub si of Some (si', bs) \<Rightarrow>
    let s' = adv_d step i bs s;
    e' = mmap_update (fst (the (mmap_lookup s init))) t e;
    (i_cur, ti_cur, si_cur, q_cur, s_cur, tstp_cur) =
      while (loop_cond j) (loop_body step accept run_t run_sub)
        (Suc i, ti', si', init, s', None);
    s'' = mmap_update init (case mmap_lookup s_cur q_cur of Some (q', tstp') \<Rightarrow>
      (case tstp' of Some (ts, tp) \<Rightarrow> (q', tstp') | None \<Rightarrow> (q', tstp_cur))
      | None \<Rightarrow> (q_cur, tstp_cur)) s' in
    w\<lparr>w_i := Suc i, w_ti := ti', w_si := si', w_s := s'', w_e := e'\<rparr>)))"

lemma valid_adv_d:
  assumes valid_before: "valid_s init step accept rho u i j s"
    and u_le_i: "u \<le> i" and i_lt_j: "i < j" and b_def: "b = bs_at rho i"
  shows "valid_s init step accept rho u (i + 1) j (adv_d step i b s)"
proof -
  have keys_s: "mmap_keys s = {q. (\<exists>l \<le> u. steps step rho init (l, i) = q)}"
    using valid_before by (auto simp add: valid_s_def)
  have fin_keys_s: "finite (mmap_keys s)"
    using valid_before by (auto simp add: valid_s_def)
  have lookup_s: "\<And>q q' tstp. mmap_lookup s q = Some (q', tstp) \<Longrightarrow>
    steps step rho q (i, j) = q' \<and> tstp = sup_acc step accept rho q i j"
    using valid_before Mapping_keys_intro
    by (auto simp add: valid_s_def) (smt case_prodD option.simps(5))+
  have drop_cur_i: "\<And>x. x \<in> mmap_keys s \<Longrightarrow> drop_cur i (the (mmap_lookup s x)) =
    (steps step rho (step x (bs_at rho i)) (i + 1, j),
    sup_acc step accept rho (step x (bs_at rho i)) (i + 1) j)"
  proof -
    fix x
    assume assms: "x \<in> mmap_keys s"
    obtain q tstp where q_def: "mmap_lookup s x = Some (q, tstp)"
      using assms(1) by (auto dest: Mapping_keys_dest)
    have q_q': "q = steps step rho (step x (bs_at rho i)) (i + 1, j)"
      "tstp = sup_acc step accept rho x i j"
      using lookup_s[OF q_def] steps_split[OF i_lt_j] assms(1) by auto
    show "drop_cur i (the (mmap_lookup s x)) =
      (steps step rho (step x (bs_at rho i)) (i + 1, j),
      sup_acc step accept rho (step x (bs_at rho i)) (i + 1) j)"
      using q_def sup_acc_None[OF i_lt_j, of step accept rho]
        sup_acc_i[OF i_lt_j, of step accept rho] sup_acc_l[OF i_lt_j, of _ step accept rho]
      unfolding q_q'
      by (auto simp add: drop_cur_def split: option.splits)
  qed
  have valid_drop_cur: "\<And>x x'. x \<in> mmap_keys s \<Longrightarrow> x' \<in> mmap_keys s \<Longrightarrow>
    step x (bs_at rho i) = step x' (bs_at rho i) \<Longrightarrow> drop_cur i (the (mmap_lookup s x)) =
    drop_cur i (the (mmap_lookup s x'))"
    using drop_cur_i by auto
  then have keys_idem: "keys_idem step i b s"
    unfolding keys_idem_def b_def
    by blast
  have distinct: "distinct (map fst s)"
    using valid_before
    by (auto simp: valid_s_def)
  have "(\<lambda>q. step q (bs_at rho i)) ` {q. \<exists>l\<le>u. steps step rho init (l, i) = q} =
    {q. \<exists>l\<le>u. steps step rho init (l, i + 1) = q}"
    using steps_app[of _ i step rho init] u_le_i
    by auto
  then have keys_s': "mmap_keys (adv_d step i b s) =
    {q. \<exists>l\<le>u. steps step rho init (l, i + 1) = q}"
    using adv_d_keys[OF distinct, of step i b]
    unfolding keys_s b_def
    by auto
  have lookup_s': "\<And>q q' tstp. mmap_lookup (adv_d step i b s) q = Some (q', tstp) \<Longrightarrow>
    steps step rho q (i + 1, j) = q' \<and> tstp = sup_acc step accept rho q (i + 1) j"
  proof -
    fix q q' tstp
    assume assm: "mmap_lookup (adv_d step i b s) q = Some (q', tstp)"
    obtain x where wit: "x \<in> mmap_keys s" "step x (bs_at rho i) = q"
      using assm lookup_adv_d_None[OF distinct, of step b q i]
      by (auto simp: b_def)
    have lookup_s'_q: "mmap_lookup (adv_d step i b s) q =
      Some (drop_cur i (the (mmap_lookup s x)))"
      using lookup_adv_d_Some[OF distinct keys_idem wit[folded b_def]] .
    then show "steps step rho q (i + 1, j) = q' \<and> tstp = sup_acc step accept rho q (i + 1) j"
      using assm
      by (simp add: drop_cur_i wit)
  qed
  have "distinct (map fst (adv_d step i b s))"
    using mmap_fold_distinct[OF distinct]
    unfolding adv_d_def mmap_map_fst
    by auto
  then show "valid_s init step accept rho u (i + 1) j (adv_d step i b s)"
    unfolding valid_s_def
    using keys_s' lookup_s' u_le_i
    by (fastforce split: option.splits dest: Mapping_keys_dest)
qed

lemma mmap_lookup_update':
  "mmap_lookup (mmap_update k v kvs) z = (if k = z then Some v else mmap_lookup kvs z)"
  unfolding mmap_lookup_def mmap_update_def
  by (auto simp add: update_conv')

lemma mmap_keys_update: "mmap_keys (mmap_update k v kvs) = mmap_keys kvs \<union> {k}"
  by (induction kvs) (auto simp: mmap_keys_def mmap_update_def)

lemma valid_adv_start:
  assumes "valid_window args t0 sub rho w" "w_i w < w_j w"
  shows "valid_window args t0 sub rho (adv_start args w)"
proof -
  define init where "init = w_init args"
  define step where "step = w_step args"
  define accept where "accept = w_accept args"
  define run_t where "run_t = w_run_t args"
  define run_sub where "run_sub = w_run_sub args"
  define i where "i = w_i w"
  define ti where "ti = w_ti w"
  define si where "si = w_si w"
  define j where "j = w_j w"
  define tj where "tj = w_tj w"
  define sj where "sj = w_sj w"
  define s where "s = w_s w"
  define e where "e = w_e w"
  have valid_before: "reach_window args t0 sub rho (i, ti, si, j, tj, sj)"
    "\<And>i j. i \<le> j \<Longrightarrow> j < length rho \<Longrightarrow> ts_at rho i \<le> ts_at rho j"
    "\<forall>q. mmap_lookup e q = sup_leadsto init step rho i j q" "distinct (map fst e)"
    "valid_s init step accept rho i i j s"
    using assms(1)
    by (auto simp: valid_window_def Let_def init_def step_def accept_def run_t_def
        run_sub_def i_def ti_def si_def j_def tj_def sj_def s_def e_def)
  have distinct_before: "distinct (map fst s)" "distinct (map fst e)"
    using valid_before
    by (auto simp: valid_s_def)
  note i_lt_j = assms(2)[folded i_def j_def]
  obtain ti' si' t b where tb_def: "run_t ti = Some (ti', t)"
    "run_sub si = Some (si', b)"
    "reach_sub run_t ti' (drop (Suc i) (map fst rho)) tj"
    "reach_sub run_sub si' (drop (Suc i) (map snd rho)) sj"
    "t = ts_at rho i" "b = bs_at rho i"
    using valid_before i_lt_j
    apply (auto simp: ts_at_def bs_at_def run_t_def[symmetric] run_sub_def[symmetric]
        elim!: reach_sub.cases[of run_t ti "drop i (map fst rho)" tj]
        reach_sub.cases[of run_sub si "drop i (map snd rho)" sj])
    by (metis Cons_nth_drop_Suc length_map list.inject nth_map)
  have reach_sub_si': "reach_sub run_sub sub (take (Suc i) (map snd rho)) si'"
    using valid_before tb_def(2,3,4) i_lt_j reach_sub_app tb_def(1)
    by (auto simp: run_sub_def sub_def bs_at_def take_Suc_conv_app_nth reach_sub_app tb_def(6))
  have reach_sub_ti': "reach_sub run_t t0 (take (Suc i) (map fst rho)) ti'"
    using valid_before tb_def(2,3,4) i_lt_j reach_sub_app tb_def(1)
    by (auto simp: run_t_def ts_at_def take_Suc_conv_app_nth reach_sub_app tb_def(5))
  define e' where "e' = mmap_update (fst (the (mmap_lookup s init))) t e"
  define s' where "s' = adv_d step i b s"
  obtain i_cur ti_cur si_cur q_cur s_cur tstp_cur where loop_def:
    "(i_cur, ti_cur, si_cur, q_cur, s_cur, tstp_cur) =
      while (loop_cond j) (loop_body step accept run_t run_sub) (Suc i, ti', si', init, s', None)"
    by (cases "while (loop_cond j) (loop_body step accept run_t run_sub)
        (Suc i, ti', si', init, s', None)") auto
  define s'' where "s'' = mmap_update init (case mmap_lookup s_cur q_cur of
    Some (q', tstp') \<Rightarrow> (case tstp' of Some (ts, tp) \<Rightarrow> (q', tstp') | None \<Rightarrow> (q', tstp_cur))
    | None \<Rightarrow> (q_cur, tstp_cur)) s'"
  have i_le_j: "i \<le> j"
    using i_lt_j by auto
  have length_rho: "length rho = j"
    using valid_before by auto
  have lookup_s: "\<And>q q' tstp. mmap_lookup s q = Some (q', tstp) \<Longrightarrow>
    steps step rho q (i, j) = q' \<and> tstp = sup_acc step accept rho q i j"
    using valid_before Mapping_keys_intro
    by (auto simp: valid_s_def) (smt case_prodD option.simps(5))+
  have init_in_keys_s: "init \<in> mmap_keys s"
    using valid_before by (auto simp add: valid_s_def)
  then have run_init_i_j: "steps step rho init (i, j) = fst (the (mmap_lookup s init))"
    using lookup_s by (auto dest: Mapping_keys_dest)
  have lookup_e: "\<And>q. mmap_lookup e q = sup_leadsto init step rho i j q"
    using valid_before by auto
  have lookup_e': "\<And>q. mmap_lookup e' q = sup_leadsto init step rho (i + 1) j q"
  proof -
    fix q
    show "mmap_lookup e' q = sup_leadsto init step rho (i + 1) j q"
    proof (cases "steps step rho init (i, j) = q")
      case True
      have "Max {l. l < Suc i \<and> steps step rho init (l, j) = steps step rho init (i, j)} = i"
        by (rule iffD2[OF Max_eq_iff]) auto
      then have "sup_leadsto init step rho (i + 1) j q = Some (ts_at rho i)"
        by (auto simp add: sup_leadsto_def True)
      then show ?thesis
        unfolding e'_def using run_init_i_j tb_def
        by (auto simp add: mmap_lookup_update' True)
    next
      case False
      show ?thesis
        using run_init_i_j sup_leadsto_idle[OF i_lt_j False] lookup_e[of q] False
        by (auto simp add: e'_def mmap_lookup_update')
    qed
  qed
  have reach_split: "{q. \<exists>l\<le>i + 1. steps step rho init (l, i + 1) = q} =
    {q. \<exists>l\<le>i. steps step rho init (l, i + 1) = q} \<union> {init}"
    using le_Suc_eq by auto
  have valid_s_i: "valid_s init step accept rho i i j s"
    using valid_before by auto
  have valid_s'_Suc_i: "valid_s init step accept rho i (i + 1) j s'"
    using valid_adv_d[OF valid_s_i order.refl i_lt_j, OF tb_def(6)] unfolding s'_def .
  have loop: "loop_inv init step accept args t0 sub rho i j tj sj
    (i_cur, ti_cur, si_cur, q_cur, s_cur, tstp_cur) \<and>
    \<not>loop_cond j (i_cur, ti_cur, si_cur, q_cur, s_cur, tstp_cur)"
    unfolding loop_def
  proof (rule while_rule_lemma[of "loop_inv init step accept args t0 sub rho i j tj sj"
        "loop_cond j" "loop_body step accept run_t run_sub"
        "\<lambda>s. loop_inv init step accept args t0 sub rho i j tj sj s \<and> \<not> loop_cond j s"])
    show "loop_inv init step accept args t0 sub rho i j tj sj
      (Suc i, ti', si', init, s', None)"
      unfolding loop_inv_def
      using i_lt_j valid_s'_Suc_i sup_acc_same[of step accept rho]
        length_rho reach_sub_si' reach_sub_ti' tb_def(3,4)
      by (auto simp: run_t_def run_sub_def split: prod.splits)
  next
    have "{(t, s). loop_inv init step accept args t0 sub rho i j tj sj s \<and>
      loop_cond j s \<and> t = loop_body step accept run_t run_sub s} \<subseteq>
      measure (\<lambda>(i_cur, q, s, tstp). j - i_cur)"
      unfolding loop_inv_def loop_cond_def loop_body_def
      apply (auto simp: run_t_def run_sub_def split: option.splits)
      apply (metis drop_eq_Nil length_map not_less option.distinct(1) reach_sub.simps)
      apply (metis (no_types, lifting) drop_eq_Nil length_map not_less option.distinct(1)
          reach_sub.simps)
      by linarith
    then show "wf {(t, s). loop_inv init step accept args t0 sub rho i j tj sj s \<and>
      loop_cond j s \<and> t = loop_body step accept run_t run_sub s}"
      using wf_measure wf_subset by auto
  next
    fix state
    assume assms: "loop_inv init step accept args t0 sub rho i j tj sj state"
      "loop_cond j state"
    obtain i_cur ti_cur si_cur q_cur s_cur tstp_cur
      where state_def: "state = (i_cur, ti_cur, si_cur, q_cur, s_cur, tstp_cur)"
      by (cases state) auto
    obtain ti'_cur si'_cur t_cur b_cur where tb_cur_def: "run_t ti_cur = Some (ti'_cur, t_cur)"
      "run_sub si_cur = Some (si'_cur, b_cur)"
      "reach_sub run_t ti'_cur (drop (Suc i_cur) (map fst rho)) tj"
      "reach_sub run_sub si'_cur (drop (Suc i_cur) (map snd rho)) sj"
      "t_cur = ts_at rho i_cur" "b_cur = bs_at rho i_cur"
      using assms
      unfolding loop_inv_def loop_cond_def state_def
      apply (auto simp: ts_at_def bs_at_def run_t_def[symmetric] run_sub_def[symmetric]
          elim!: reach_sub.cases[of run_t ti_cur "drop i_cur (map fst rho)" tj]
          reach_sub.cases[of run_sub si_cur "drop i_cur (map snd rho)" sj])
      by (metis Cons_nth_drop_Suc length_map list.inject nth_map)
    have reach_sub_si': "reach_sub run_sub sub (take (Suc i_cur) (map snd rho)) si'_cur"
      using assms
      unfolding loop_inv_def loop_cond_def state_def
      by (auto simp: run_sub_def sub_def bs_at_def take_Suc_conv_app_nth reach_sub_app
          tb_cur_def(2,4,6))
         (metis bs_at_def reach_sub_app run_sub_def tb_cur_def(2) tb_cur_def(6))
    have reach_sub_ti': "reach_sub run_t t0 (take (Suc i_cur) (map fst rho)) ti'_cur"
      using assms
      unfolding loop_inv_def loop_cond_def state_def
      by (auto simp: run_t_def ts_at_def take_Suc_conv_app_nth reach_sub_app tb_cur_def(1,3,5))
         (metis reach_sub_app run_t_def tb_cur_def(1) tb_cur_def(5) ts_at_def)
    show "loop_inv init step accept args t0 sub rho i j tj sj
      (loop_body step accept run_t run_sub state)"
      using assms valid_adv_d[of init step accept rho] acc_app[of _ _ step rho _ _ accept]
        steps_app[of _ _ step rho] sup_acc_ext[of _ _ step accept rho]
        sup_acc_ext_idle[of _ _ step accept rho]
        tb_cur_def reach_sub_si' reach_sub_ti'
      unfolding loop_inv_def loop_cond_def loop_body_def state_def
      by (auto simp: run_t_def run_sub_def acc_def ts_at_def split: option.splits)
  qed auto
  have valid_s'': "valid_s init step accept rho (i + 1) (i + 1) j s''"
  proof (cases "mmap_lookup s_cur q_cur")
    case None
    then have added: "steps step rho init (i + 1, j) = q_cur"
      "tstp_cur = sup_acc step accept rho init (i + 1) j"
      using loop unfolding loop_inv_def loop_cond_def
      by (auto dest: Mapping_keys_dest)
    have s''_case: "s'' = mmap_update init (q_cur, tstp_cur) s'"
      unfolding s''_def using None by auto
    show ?thesis
      using valid_s'_Suc_i reach_split added mmap_update_distinct
      unfolding s''_case valid_s_def mmap_keys_update
      by (auto simp add: mmap_lookup_update' split: option.splits)
  next
    case (Some p)
    obtain q' tstp' where p_def: "p = (q', tstp')"
      by (cases p) auto
    note lookup_s_cur = Some[unfolded p_def]
    have i_cur_in: "i + 1 \<le> i_cur" "i_cur \<le> j"
      using loop unfolding loop_inv_def by auto
    have q_cur_def: "steps step rho init (i + 1, i_cur) = q_cur"
      using loop unfolding loop_inv_def by auto
    have valid_s_cur: "valid_s init step accept rho i i_cur j s_cur"
      using loop unfolding loop_inv_def by auto
    have q'_steps: "steps step rho q_cur (i_cur, j) = q'"
      using Some valid_s_cur unfolding valid_s_def p_def
      by (auto intro: Mapping_keys_intro) (smt case_prodD option.simps(5))
    have tstp_cur: "tstp_cur = sup_acc step accept rho init (i + 1) i_cur"
      using loop unfolding loop_inv_def by auto
    have tstp': "tstp' = sup_acc step accept rho q_cur i_cur j"
      using loop Some unfolding loop_inv_def p_def valid_s_def
      by (auto intro: Mapping_keys_intro) (smt case_prodD option.simps(5))
    have added: "steps step rho init (i + 1, j) = q'"
      using steps_comp[OF i_cur_in q_cur_def q'_steps] .
    show ?thesis
    proof (cases tstp')
      case None
      have s''_case: "s'' = mmap_update init (q', tstp_cur) s'"
        unfolding s''_def lookup_s_cur None by auto
      have tstp_cur_opt: "tstp_cur = sup_acc step accept rho init (i + 1) j"
        using sup_acc_comp_None[OF i_cur_in, of step accept rho init, unfolded q_cur_def,
            OF tstp'[unfolded None, symmetric]]
        unfolding tstp_cur by auto
      then show ?thesis
        using valid_s'_Suc_i reach_split added mmap_update_distinct
        unfolding s''_case valid_s_def mmap_keys_update
        by (auto simp add: mmap_lookup_update' split: option.splits)
    next
      case (Some p')
      obtain ts tp where p'_def: "p' = (ts, tp)"
        by (cases p') auto
      have True: "tp \<ge> i_cur"
        using sup_acc_SomeE[OF tstp'[unfolded Some p'_def, symmetric]] by auto
      have s''_case: "s'' = mmap_update init (q', tstp') s'"
        unfolding s''_def lookup_s_cur Some p'_def using True by auto
      have tstp'_opt: "tstp' = sup_acc step accept rho init (i + 1) j"
        using sup_acc_comp_Some_ge[OF i_cur_in True
            tstp'[unfolded Some p'_def q_cur_def[symmetric], symmetric]]
        unfolding tstp' by (auto simp: q_cur_def[symmetric])
      then show ?thesis
        using valid_s'_Suc_i reach_split added mmap_update_distinct
        unfolding s''_case valid_s_def mmap_keys_update
        by (auto simp add: mmap_lookup_update' split: option.splits)
    qed
  qed
  have "distinct (map fst e')"
    using mmap_update_distinct[OF distinct_before(2), unfolded e'_def]
    unfolding e'_def .
  then have "valid_window args t0 sub rho
    (w\<lparr>w_i := Suc i, w_ti := ti', w_si := si', w_s := s'', w_e := e'\<rparr>)"
    using i_lt_j lookup_e' valid_s'' length_rho tb_def(3,4) reach_sub_si' reach_sub_ti'
      valid_before(2,3)
    by (auto simp: valid_window_def Let_def init_def step_def accept_def run_t_def
        run_sub_def i_def ti_def si_def j_def tj_def sj_def s_def e_def)
  moreover have "adv_start args w = w\<lparr>w_i := Suc i, w_ti := ti', w_si := si', w_s := s'', w_e := e'\<rparr>"
    unfolding adv_start_def Let_def s''_def s'_def e'_def
    using tb_def(1,2) i_lt_j loop_def[unfolded s'_def]
    by (auto simp: valid_window_def Let_def init_def step_def accept_def run_t_def
        run_sub_def i_def ti_def si_def j_def tj_def sj_def s_def e_def
        split: prod.splits)
  ultimately show ?thesis
    by auto
qed

lemma valid_adv_start_bounds:
  assumes "valid_window args t0 sub rho w" "w_i w < w_j w"
  shows "w_i (adv_start args w) = Suc (w_i w)" "w_j (adv_start args w) = w_j w"
    "w_tj (adv_start args w) = w_tj w" "w_sj (adv_start args w) = w_sj w"
  using assms
  by (auto simp: adv_start_def Let_def valid_window_def split: option.splits prod.splits
      elim: reach_sub.cases)

lemma valid_adv_start_bounds':
  assumes "valid_window args t0 sub rho w" "w_run_t args (w_ti w) = Some (ti', t)"
    "w_run_sub args (w_si w) = Some (si', bs)"
  shows "w_ti (adv_start args w) = ti'" "w_si (adv_start args w) = si'"
  using assms
  by (auto simp: adv_start_def Let_def valid_window_def split: option.splits prod.splits)

end
